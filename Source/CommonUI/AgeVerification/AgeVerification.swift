//
//  AgeVerification.swift
//  Source
//
//  Created by Techwens on 06/09/23.
//

import UIKit

class AgeVerification: UIView {
    // Closure properties to handle button clicks
    var onCloseButtonTapped: (() -> Void)?
    var onOkeyClicked: (() -> Void)?
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet var okeyBtn: UIButton!
    
    override init(frame:CGRect){
        super.init(frame: frame)
        commitInt()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInt()
    }
    
    private func commitInt(){
        Bundle.main.loadNibNamed("AgeVerification", owner: self,options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.topCornersRadius = 40
//        contentView.layer.borderColor = UIColor.gray.cgColor
//        contentView.borderWidth = 1
        contentView.layer.shadowRadius = 40
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 1, height: 0)
        contentView.layer.shadowColor = UIColor.gray.cgColor
        
        
      //  closeBtn.allCornersRadius = 20
//        allowBtn.layer.cornerRadius = 10
        
//        onCick handler
   //     closeBtn.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
        okeyBtn.addTarget(self, action: #selector(okeyBtnTapped), for: .touchUpInside)
    }
    @objc private func okeyBtnTapped() {
        onOkeyClicked?()
       }
       
       

}

