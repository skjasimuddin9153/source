//
//  FellowshipDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class FellowshipDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with payload: [UploadPayloadData], parameters: [String:Any], completion:@escaping EmptyAPIResponseCompletion){
        makeMultipartAPICall(to: AuthEndpoints.register, withBody: parameters, payload: payload, progressHandler: nil, completion: completion)
    }
}
