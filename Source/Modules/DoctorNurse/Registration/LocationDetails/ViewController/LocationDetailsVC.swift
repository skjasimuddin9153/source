//
//  LocationDetailsVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
enum LocationDetailsDropdown {
     case country
     case state
}

class LocationDetailsVC: UIViewController {
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var btnLocation: UIButton!
    @IBOutlet weak var tfZipcode: UITextField!
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfDistrict: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var btnSend: AppSolidButton!
    // MARK: Instance variables
	lazy var viewModel = LocationDetailsViewModel()
    var acticeDropDown: LocationDetailsDropdown?
    var selectedCountryCode: String?{
        didSet {
            guard let selectedCountryCode = selectedCountryCode else {
                showSnackbar(withMessage: "Country Code Invalid")
                return
            }
            view.showLoader(withBlur: true)
            view.isUserInteractionEnabled = false
            viewModel.getAllState(byCountryId: selectedCountryCode)
            
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        view.showLoader(withBlur: true)
        view.isUserInteractionEnabled = false
        viewModel.getAllCountries()
        tfAddress.setupSourceTextField(withPlaceholder: "Address *")
        tfState.setupSourceTextField(withPlaceholder: "State*")
        tfDistrict.setupSourceTextField(withPlaceholder: "City*")
        tfZipcode.setupSourceTextField(withPlaceholder: "Zip Code*")
        tfCountry.setupSourceTextField(withPlaceholder: "Country")
        
        tfCountry.delegate = self
        tfCountry.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfCountry.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedCountryNameDropDown()
        }
        
        tfState.delegate = self
        tfState.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfState.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedStateNameDropDown()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        let stateCode = viewModel.stateList.filter({$0.name?.lowercased() == tfState.text!.lowercased()}).first!.state_code!
        let countryCode = viewModel.countryList.filter({$0.name?.lowercased() == tfCountry.text!.lowercased()}).first!.country_code!
        viewModel.register(prospect_id:"\(viewModel.dependency!.prospect_id!)", address_line: tfAddress.text!, state: stateCode, zip_code: tfZipcode.text!, latitude: "\(LocationManager.shared.location?.coordinate.latitude ?? 0)", longitude: "\(LocationManager.shared.location?.coordinate.latitude ?? 0)", district: tfDistrict.text!, country: countryCode)
    }
    
    func allFieldsValid() -> Bool {
        
        if LocationManager.shared.location?.coordinate.latitude  == nil || LocationManager.shared.location?.coordinate.longitude == nil {
            showSnackbar(withMessage: "Location Permission Required")
            LocationManager.shared.requestUserLocation()
            return false
        }
        
        if viewModel.dependency?.prospect_id == nil {
            showSnackbar(withMessage: "Prospect Not Found")
            return false
        }
        if (tfAddress.text?.isEmpty ?? true) || tfAddress.text == ""{
            showSnackbar(withMessage: "Address is Invalid")
            tfAddress.shake()
            return false
        }
        if  (tfZipcode.text?.isEmpty ?? true) || tfZipcode.text == "" {
            showSnackbar(withMessage: "Zipcode is Invalid")
            tfZipcode.shake()
            return false
        }
        if  (tfDistrict.text?.isEmpty ?? true) || tfDistrict.text == "" {
            showSnackbar(withMessage: "District No is Invalid")
            tfDistrict.shake()
            return false
        }
        if  (tfState.text?.isEmpty ?? true) || tfState.text == "" {
            showSnackbar(withMessage: "State is Invalid")
            tfState.shake()
            return false
        }
        if !viewModel.stateList.contains(where: {$0.name?.lowercased() == tfState.text?.lowercased() ?? ""}) {
            showSnackbar(withMessage: "State Code is Invalid")
            tfState.shake()
            return false
        }
        if  (tfCountry.text?.isEmpty ?? true) || tfCountry.text == "" {
            showSnackbar(withMessage: "Country is Invalid")
            tfCountry.shake()
            return false
        }
        if (searchCountryCodeFromName(key: tfCountry.text ?? "") == nil) {
            showSnackbar(withMessage: "Country Code is Invalid")
            tfCountry.shake()
            return false
        }
        return true
    }
    
    func setupStateAndOthers() {
        if let location = LocationManager.shared.location {
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
            location.placemark { [self] placemark, error in
                guard let placemark = placemark else {
                    print("Error:", error ?? "nil")
                    return
                }
                
                if viewModel.stateList.contains(where: {$0.state_code?.lowercased() == placemark.state?.lowercased()}) {
                    var address: String = ""
                    if let streetNumber = placemark.streetNumber {
                        address = address + streetNumber + ","
                    }
                    if let streetName = placemark.streetName {
                        address = address + streetName + ","
                    }
                    if let neighborhood = placemark.neighborhood {
                        address = address + neighborhood
                    }
                    tfAddress.text = address

                    if let stateName = getStateNameFromCode(code: placemark.state ?? "") {
                        tfState.text = stateName
                    }
                    tfZipcode.text = placemark.zipCode
                    tfDistrict.text = placemark.city
                }

               
            }
        }
    }
    
    func locationSetupCountry() {
        if let location = LocationManager.shared.location {
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
            location.placemark { [self] placemark, error in
                guard let placemark = placemark else {
                    print("Error:", error ?? "nil")
                    return
                }
                print("=====================================")
                print(placemark.streetNumber ?? "")
                print(placemark.streetName ?? "")
                print(placemark.neighborhood ?? "")
                print(placemark.city ?? "")
                print(placemark.county ?? "")
                print(placemark.state ?? "")
                print(placemark.zipCode ?? "")
                print(placemark.country ?? "")
                print(placemark.postalAddressFormatted ?? "")
                print("=====================================")

                tfCountry.text = placemark.country
                if let countryCode = searchCountryCodeFromName(key: placemark.country ?? "") {
                    selectedCountryCode = countryCode
                }
               
            }
        } else {
          selectedCountryCode =  getDefaultCountryCode()

        }
    }
    func tappedCountryNameDropDown() {
        acticeDropDown = .country
        let dependency = PickerViewDependency(title: "Select Country", dataList:viewModel.countryList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }

    
    func tappedStateNameDropDown() {
        if viewModel.stateList.isEmpty {
            showSnackbar(withMessage: "Please Select a country first")
            return
        }
        acticeDropDown = .state
        let dependency = PickerViewDependency(title: "Select State", dataList:viewModel.stateList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedGetLocation(_ sender: UIButton) {
        LocationManager.shared.requestUserLocation()
        locationSetupCountry()
    }
    
    func isSameCountrySelected(value: String) -> Bool {
        let currentValue = tfCountry.text ?? ""
        if currentValue == value   {
            return true
        } else {
            return false
        }
    }
    func isSameStateySelected(value: String) -> Bool {
        let currentValue = tfState.text ?? ""
        if currentValue == value   {
            return true
        } else {
            return false
        }
    }
    
    func searchCountryCodeFromName(key: String) -> String? {
      let matches = viewModel.countryList.filter({$0.name?.lowercased() == key.lowercased()})
        if matches.isEmpty {
            return nil
        } else {
            return matches.first?.country_code
        }
    }
    
    func getStateNameFromCode(code: String) -> String? {
        let matches = viewModel.stateList.filter({$0.state_code?.lowercased() == code.lowercased()})
          if matches.isEmpty {
              return nil
          } else {
              return matches.first?.name
          }
    }
    
    func getDefaultCountryCode() -> String {
        tfCountry.text = "India"
        
        if let code = searchCountryCodeFromName(key: "India") {
            return code
        } else {
            return "IN"
        }
    }
}

// MARK: - Load from storyboard with dependency
extension LocationDetailsVC {
    class func loadFromXIB(withDependency dependency: LocationDetailsDependency? = nil) -> LocationDetailsVC? {
        let storyboard = UIStoryboard(name: "LocationDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "LocationDetailsVC") as? LocationDetailsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - LocationDetailsAPIResponseDelegate
extension LocationDetailsVC: LocationDetailsAPIResponseDelegate {
    func handleCountryListResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        locationSetupCountry()
    }
    
    func handleStateListResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        setupStateAndOthers()
    }
    
    func handleRegisterResponse() {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        let dependency = RegistrationFinishDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = RegistrationFinishVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
    
}

extension LocationDetailsVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case tfCountry :
            tappedCountryNameDropDown()
            return false
        case tfState:
            tappedStateNameDropDown()
            return false
        default:
            return true
        }
    }
}

extension LocationDetailsVC: SelectInfoFromPickerDelegate {
    func didInfoSelected(value: String, index: Int) {
        switch acticeDropDown {
        case .country:
            if !isSameCountrySelected(value: value) {
                tfState.text = ""
                tfDistrict.text = ""
                tfZipcode.text = ""
                tfAddress.text = ""
            }
            tfCountry.text = value
            selectedCountryCode = viewModel.countryList[index].country_code ?? "IN"
        case .state:
            if !isSameStateySelected(value: value) {
                tfDistrict.text = ""
                tfZipcode.text = ""
                tfAddress.text = ""
            }
            tfState.text = value
        default:
            acticeDropDown = nil
        }
        acticeDropDown = nil
    }
    
    func noSelection() {
        acticeDropDown = nil
    }
}
