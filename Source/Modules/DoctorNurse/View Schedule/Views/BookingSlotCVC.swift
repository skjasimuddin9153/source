//
//  BookingSlotCVC.swift
//  Source
//
//  Created by Techwens on 08/09/22.
//

import UIKit

class BookingSlotCVC: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var lblTimeSlot: UILabel!
    
    
    override var isSelected: Bool {
            didSet {
                bgView.borderColor = isSelected ? .systemGreen : .appLight
                lblTimeSlot.textColor = isSelected ? .systemGreen : .appLight
            }
        }
}
