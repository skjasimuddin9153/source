//
//  LoginDependency.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

struct LoginDependency {
}

struct PhoneCode {
    let icon :UIImage
    let label:String
    let code:String
}
