//
//  TermConditionViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol TermConditionAPIResponseDelegate: class {
}

class TermConditionViewModel {
    weak var apiResponseDelegate: TermConditionAPIResponseDelegate?
    lazy var localDataManager = TermConditionLocalDataManager()
    lazy var apiDataManager = TermConditionAPIDataManager()
    
    var dependency: TermConditionDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
