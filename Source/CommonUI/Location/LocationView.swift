//
//  LocationView.swift
//  Source
//
//  Created by Techwens Software on 31/08/23.
//

import UIKit

class LocationView: UIView {
    // Closure properties to handle button clicks
    var onCloseButtonTapped: (() -> Void)?
    var onAllowButtonTapped: (() -> Void)?
    
    @IBOutlet var contentView: UIView!

    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var allowBtn: UIButton!
    override init(frame:CGRect){
        super.init(frame: frame)
        commitInt()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInt()
    }
    
    private func commitInt(){
        Bundle.main.loadNibNamed("LocationView", owner: self,options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.topCornersRadius = 40
        closeBtn.allCornersRadius = 20
//        allowBtn.layer.cornerRadius = 10
        
//        onCick handler
        closeBtn.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
              allowBtn.addTarget(self, action: #selector(allowButtonTapped), for: .touchUpInside)
    }
    @objc private func closeButtonTapped() {
           onCloseButtonTapped?()
       }
       
       @objc private func allowButtonTapped() {
           onAllowButtonTapped?()
       }

}
