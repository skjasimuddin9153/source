//
//  CodeVerificationAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias OtpSendResponseCompletion = (Result<APIResponse<EmptyAPIResponseModel>, Error>) -> Void
typealias OtpReSendResponseCompletion = (Result<APIResponse<EmptyAPIResponseModel>, Error>) -> Void

class CodeVerificationAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func sendOTPAPI(with parameter: [String: Any] ,prospect_id: Int, completion:@escaping OtpSendResponseCompletion){
//        makeAPICall(to: AuthEndpoints.verifyOtp(prospect_id: prospect_id), withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func sendOTPAPI(with parameter: [String: Any] , completion:@escaping OtpSendResponseCompletion){
        makeAPICall(to: AuthEndpoints.verifyOTPforgotPassword, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func resendOTPAPI(prospect_id: Int, completion:@escaping OtpReSendResponseCompletion){
        makeAPICall(to: AuthEndpoints.resendOTP(prospect_id: prospect_id), completion: completion)
    }
    
    func forgotPasswordAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.forgotPin, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
    
}
