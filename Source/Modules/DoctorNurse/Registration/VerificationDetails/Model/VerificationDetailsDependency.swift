//
//  VerificationDetailsDependency.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct VerificationDetailsDependency {
    let prospect_id : Int?
}

enum DocTypes: String {
    case adhar
    case passport
    case driving_license
    case electoral_id
    
    var name: String {
        switch self {
        case .adhar:
            return "Aadhaar Card"
        case .passport:
            return "Passport"
        case .driving_license:
            return "Driving License"
        case .electoral_id:
            return "Voter ID"
        }
    }
    
    var id: Int {
        switch self {
        case .adhar:
            return 1
        case .passport:
            return 2
        case .driving_license:
            return 3
        case .electoral_id:
            return 4
        }
    }
    
}
