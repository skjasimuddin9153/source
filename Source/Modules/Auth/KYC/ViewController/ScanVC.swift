//
//  ScanVC.swift
//  Source
//
//  Created by Techwens on 08/09/23.
//

import UIKit

class ScanVC: UIViewController {
    @IBOutlet var header: HeaderView!
    var  bottomSheetView = Verification()
    lazy var viewModel = ScanViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bottomSheetView.onAllowButtonTapped = { [weak self] in
            guard let vc = DetailsViewController.loadFromXIB() else { return }
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        header.navigationController = self.navigationController
        header.title.isHidden = true
    }
    
    @IBAction func btnContinue(_ sender: Any) {
        presentBottomSheet()
        print("btncontinue")
      
    }
    
    @IBAction func btnTryAgain(_ sender: Any) {
        
    }
    func presentBottomSheet() {
      
        
        view.addSubview(bottomSheetView)
        
        let height = view.frame.height * 0.4// Adjust this value as needed
        let startY = view.frame.height
        
        bottomSheetView.frame = CGRect(x: 0, y: startY, width: view.frame.width, height: height)
        
        UIView.animate(withDuration: 0.3) { [self] in
            self.bottomSheetView.frame.origin.y = self.view.frame.height - height
        }
    }
    
    func hideBottomSheet() {
          let height = view.frame.height * 0.5 // Adjust this value to match the original height of the bottom sheet view
          
          UIView.animate(withDuration: 0.3, animations: { [weak self] in
              self?.bottomSheetView.frame.origin.y += height
          }) { [weak self] _ in
              self?.bottomSheetView.removeFromSuperview()
          }
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ScanVC {
    class func loadFromXIB() -> DetailsViewController? {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return nil
        }
      
        return viewController
    }
}
