//
//  LoginPinDependency.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct LoginPinDependency {
    let emailPhone: String
    let userType: UserType
    let user : UserData
    
}
struct OtpVeryFyResponse: Decodable {
    let status: Bool
    let data: DataObject
}

struct OtpDataObject: Decodable {
    let message: String
    let status: Bool
    let response: ResponseObject
    let error: String?
}

struct OtpResponseObject: Decodable {
    let authKey: String

    private enum CodingKeys: String, CodingKey {
        case authKey = "auth_key"
    }
}
