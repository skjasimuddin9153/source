//
//  AddNewSlotsVC.swift
//  Source
//
//  Created by Techwens on 24/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class AddNewSlotsVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var imgNurse: UIImageView!
    @IBOutlet weak var imgEmg: UIImageView!
    @IBOutlet weak var imgIcu: UIImageView!
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnNurse: UIButton!
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var btnICU: UIButton!
    @IBOutlet weak var btnEmergency: UIButton!
    lazy var viewModel = AddNewSlotsViewModel()
    var userType: UserType?{
        didSet {
            switch userType {
            case .doctor:
                imgDoc.image = UIImage(named: "icRadioCheck")
                imgNurse.image = UIImage(named: "icRadio")
            case .nurse:
                imgNurse.image = UIImage(named: "icRadioCheck")
                imgDoc.image = UIImage(named: "icRadio")
            default :
                ()
                
            }
        }
    }
    var specailty: SlotType?{
        didSet {
            switch specailty {
            case .icu:
                imgIcu.image = UIImage(named: "icRadioCheck")
                imgEmg.image = UIImage(named: "icRadio")
            case .emergency:
                imgEmg.image = UIImage(named: "icRadioCheck")
                imgIcu.image = UIImage(named: "icRadio")
            default :
                ()
                
            }
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        setupTextField()
        userType = .doctor
        specailty = .icu
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedDoctn(_ sender: UIButton) {
        userType = .doctor
    }
    @IBAction func tappedNursetn(_ sender: UIButton) {
        userType = .nurse
        
    }
    @IBAction func tappedICUTypeBtn(_ sender: UIButton) {
        specailty = .icu
    }
    @IBAction func tappedEmgTypeBtn(_ sender: UIButton) {
        specailty = .emergency
    }
    
    func setupTextField() {
        tfStartTime.setupSourceTextField(withPlaceholder: "Start Time")
        tfStartTime.setInputViewTimePicker(target: self, selector: #selector(tapDonetfStart(sender:datePicker1:)))
        tfStartTime.setRightViewButton(with: UIImage(named: "icTime")!)
        tfEndTime.setupSourceTextField(withPlaceholder: "End Time")
        tfEndTime.setInputViewTimePicker(target: self, selector: #selector(tapDonetfEnd(sender:datePicker1:)))
        tfEndTime.setRightViewButton(with: UIImage(named: "icTime")!)
        tfPrice.setupSourceTextField(withPlaceholder: "XX,XXX /per hourr")
        tfPrice.setLeftViewButton(with: UIImage(named: "icRupee")!)
    }
    @objc func tapDonetfStart(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfStartTime.inputView as? UIDatePicker { // 2.1
            self.tfStartTime.text = datePicker.date.toString(format: .custom("hh: mm a"))
        }
        self.tfStartTime.resignFirstResponder() // 2.5
    }
    @objc func tapDonetfEnd(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfEndTime.inputView as? UIDatePicker { // 2.1
            self.tfEndTime.text = datePicker.date.toString(format: .custom("hh: mm a"))
        }
        self.tfEndTime.resignFirstResponder() // 2.5
    }
    func areAllFieldValid() -> Bool {
        if specailty == nil {
            showSnackbar(withMessage: "Select A Speciality")
            return false
        }
        if userType == nil {
            showSnackbar(withMessage: "Select A User Type")
            return false
        }
        if tfStartTime.text == nil || ( tfStartTime.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "Start time Invalid")
            return false
        }
        if tfEndTime.text == nil ||  (tfEndTime.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "End time Invalid")
            return false
        }
        if tfPrice.text == nil ||  (tfPrice.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "Price Invalid")
            return false
        }
        return true
    }
    @IBAction func tappedAddSlotBtn(_ sender: UIButton) {
        if !areAllFieldValid() {
            return
        }
        guard let _ = tfStartTime.text!.slotTimeTodate() else {
            showSnackbar(withMessage: "Error Time Format")
            return
        }
        guard let _ = tfEndTime.text!.slotTimeTodate() else {
            showSnackbar(withMessage: "Error Time Format")
            return
        }
        viewModel.createSlot(type: userType!, slotSpeciality: specailty!, startTime:tfStartTime.text!.slotFormatTime(), endTime: tfEndTime.text!.slotFormatTime(), price: tfPrice.text!)
        tfStartTime.text = ""
        tfEndTime.text = ""
        tfPrice.text = ""
    }
}

// MARK: - Load from storyboard with dependency
extension AddNewSlotsVC {
    class func loadFromXIB(withDependency dependency: AddNewSlotsDependency? = nil) -> AddNewSlotsVC? {
        let storyboard = UIStoryboard(name: "AddNewSlots", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AddNewSlotsVC") as? AddNewSlotsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - AddNewSlotsAPIResponseDelegate
extension AddNewSlotsVC: AddNewSlotsAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleSuceessAPI() {
        navigationController?.popViewController(animated: true)
    }
    
}
