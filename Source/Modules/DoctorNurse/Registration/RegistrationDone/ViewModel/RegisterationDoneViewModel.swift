//
//  RegisterationDoneViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol RegisterationDoneAPIResponseDelegate: class {
}

class RegisterationDoneViewModel {
    weak var apiResponseDelegate: RegisterationDoneAPIResponseDelegate?
    lazy var localDataManager = RegisterationDoneLocalDataManager()
    lazy var apiDataManager = RegisterationDoneAPIDataManager()
    
    var dependency: RegisterationDoneDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
