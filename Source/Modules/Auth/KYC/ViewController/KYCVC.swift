//
//  KYCVC.swift
//  Source
//
//  Created by Techwens Software on 29/08/23.
//

import UIKit
import AVFoundation
import Vision
class KYCVC: UIViewController {

    @IBOutlet var headerView: HeaderView!
    @IBOutlet var selfieBox: UIView!
    @IBOutlet var selFiePhotoview: UIImageView!
    @IBOutlet var btnContinue: BorderButton!
    lazy var viewModel = KycViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
       
        selFiePhotoview.isHidden = true
        selfieBox.layer.cornerRadius = 10
        selfieBox.layer.borderWidth = 1
        selfieBox.layer.borderColor = UIColor.gray.cgColor
        
        headerView.navigationController = self.navigationController
        headerView.title.isHidden = true
//        adding gesturehandler
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selfieBoxTapped))
            selfieBox.addGestureRecognizer(tapGesture)
            selfieBox.isUserInteractionEnabled = true
        // Do any additional setup after loading the view.
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func selfieBoxTapped() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self

        let alertController = UIAlertController(title: "Choose Photo", message: nil, preferredStyle: .actionSheet)

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            alertController.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .photo
                self.present(imagePicker, animated: true, completion: nil)
            }))
        }

        alertController.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        present(alertController, animated: true, completion: nil)
        guard let vc = KYCVC.loadFromScanXIB() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }

    


    
    @IBAction func btnContinue(_ sender: Any) {
        guard let vc = DetailsViewController.loadFromXIB() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension KYCVC {
    class func loadFromXIB(withDependency dependency: KYCDependency? = nil) -> KYCVC? {
        let storyboard = UIStoryboard(name: "KYC", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "KYCVC") as? KYCVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
    class func loadFromScanXIB(withDependency dependency: KYCDependency? = nil) -> ScanVC? {
        let storyboard = UIStoryboard(name: "KYC", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ScanVC") as? ScanVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

extension KYCVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVCaptureMetadataOutputObjectsDelegate {
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        // Get the captured image from the info dictionary
//        if let image = info[.originalImage] as? UIImage {
//            // Set the captured image to the selFiePhotoview
//            selFiePhotoview.image = image
//            btnContinue.isValid = true
//            selFiePhotoview.isHidden = false
//            selfieBox.isHidden = true
//            scanBarcode(in: image)
//        }
//
//        // Dismiss the image picker controller
//        picker.dismiss(animated: true, completion: nil)
//    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        // Dismiss the image picker controller if the user cancels
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Get the captured image from the info dictionary
        if let image = info[.originalImage] as? UIImage {
            // Print to check if the image is captured successfully
            print("Image captured")
            // Set the captured image to the selFiePhotoview
            selFiePhotoview.image = image
            btnContinue.isValid = true
            selFiePhotoview.isHidden = false
            selfieBox.isHidden = true
            if let rawQRData = processQRCodeImage(image) {
                // Handle the raw QR data here
                print("Raw QR Data: \(rawQRData)")
            } else {
                print("Image not captured")
            }
            
            // Dismiss the image picker controller
            picker.dismiss(animated: true, completion: nil)
        }
        
        
        func processQRCodeImage(_ image: UIImage) -> String? {
            guard let ciImage = CIImage(image: image) else {
                return nil
            }

            let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: nil)

            let features = detector?.features(in: ciImage)
            if let qrCodeFeature = features?.first as? CIQRCodeFeature,
                let messageString = qrCodeFeature.messageString {
                return messageString
            }

            return nil
        }

        
        
        
    }
}
