//
//  HomeFilterViewModel.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol HomeFilterAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleListAPI()
}

class HomeFilterViewModel {
    weak var apiResponseDelegate: HomeFilterAPIResponseDelegate?
    lazy var localDataManager = HomeFilterLocalDataManager()
    lazy var apiDataManager = HomeFilterAPIDataManager()
    
    var dependency: HomeFilterDependency?
    var slotList: [SlotListResponseDataModel] = []
    var fullSlotList: [SlotListResponseDataModel] = []
    
    init() {
    }
    // Data fetch service methods goes here
    func getSlotsList() {
        apiDataManager.getSlotListData{ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                guard let data = response.data else { return }
                weakSelf.slotList = data
                weakSelf.fullSlotList = data
                weakSelf.apiResponseDelegate?.handleListAPI()
            }
        }
    }
}
