//
//  LogoutDemoVC.swift
//  Source
//
//  Created by Techwens on 24/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class LogoutDemoVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = LogoutDemoViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedLogoutBtn(_ sender: AppSolidButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) { [self] in
            view.isUserInteractionEnabled = true
            DataManager.shared.reset()
            guard let vc = OnboardingVC.loadFromXIB() else { return }
            let navVC = UINavigationController (rootViewController: vc)
            navVC.isNavigationBarHidden = true
            UIWindow.key?.replaceRootViewControllerWith(navVC, animated: true, completion: nil)
        }
    }
}

// MARK: - Load from storyboard with dependency
extension LogoutDemoVC {
    class func loadFromXIB(withDependency dependency: LogoutDemoDependency? = nil) -> LogoutDemoVC? {
        let storyboard = UIStoryboard(name: "LogoutDemo", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "LogoutDemoVC") as? LogoutDemoVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - LogoutDemoAPIResponseDelegate
extension LogoutDemoVC: LogoutDemoAPIResponseDelegate {
}
