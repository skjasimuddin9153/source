//
//  PostgraduateDetailsDependency.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct PostgraduateDetailsDependency {
    let prospect_id : Int?
    let payloadData : [UploadPayloadData]
    let parameter : [String: String]
}
