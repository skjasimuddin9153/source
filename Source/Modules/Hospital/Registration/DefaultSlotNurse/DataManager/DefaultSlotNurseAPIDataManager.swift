//
//  DefaultSlotNurseAPIDataManager.swift
//  Source
//
//  Created by Techwens on 03/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class DefaultSlotNurseAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.hospitalRegister, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
