//
//  OnboardingVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import CoreLocation

class OnboardingVC: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    // MARK: Instance variables
	lazy var viewModel = OnboardingViewModel()
    var bottomSheetView = AgeVerification()
    var firstlogin : Bool = true {
        didSet {
            
            
            if (!firstlogin){
                guard let vc = ForgotPinVC.loadFromXIB() else { return }
//                guard let vc = KYCVC.loadFromXIB() else { return }
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        LocationManager.shared.requestUserLocation()
        print("latitude==\(LocationManager.shared.location?.coordinate.latitude ?? 0)", "longitude==\(LocationManager.shared.location?.coordinate.longitude ?? 0)")
      //  presentbottomSheet()
        bottomSheetView.onOkeyClicked = { [weak self] in
                  self?.hideBottomSheet()
                    self?.firstlogin = false
              }
//        bottomSheetView.onCloseButtonTapped = { [weak self] in
//                  self?.hideBottomSheet()
//              }
//
//        bottomSheetView.onAllowButtonTapped = {
//            self.handleAllowButtonTap()
//                   // Handle allow button click action here
//               }
       
    }
    
    private func presentbottomSheet(){
        view.addSubview(bottomSheetView)
        let height = view.frame.height  * 0.4
        let startY = view.frame.height
        bottomSheetView.frame = CGRect(x: 0, y: startY, width: view.frame.width, height: height)
        
        UIView.animate(withDuration: 0.3){[self] in
            self.bottomSheetView.frame.origin.y = self.view.frame.height - height
            
            
        }
    }
    
    
    private func hideBootomSheet(){
        let height = view.frame.height * 0.2
        
        UIView.animate(withDuration: 0.3, animations: {
            [weak self] in
            self?.bottomSheetView.frame.origin.y += height
        }){
            [weak self] _ in
            self?.bottomSheetView.removeFromSuperview()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func letsGoBtn(_ sender: UIButton) {
        
            presentBottomSheet()
      
//        }else if firstlogin == false {
//            guard let vc = ForgotPinVC.loadFromXIB() else { return }
//
//            navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    @IBAction func tappedCreateBtn(_ sender: UIButton) {
        guard let vc = UserTypeRegVC.loadFromXIB() else { return }
      
        navigationController?.pushViewController(vc, animated: true)
    }
    func presentBottomSheet() {
      
        
        view.addSubview(bottomSheetView)
        
        let height = view.frame.height * 0.4// Adjust this value as needed
        let startY = view.frame.height
        
        bottomSheetView.frame = CGRect(x: 0, y: startY, width: view.frame.width, height: height)
        
        UIView.animate(withDuration: 0.3) { [self] in
            self.bottomSheetView.frame.origin.y = self.view.frame.height - height
        }
    }
    
    func hideBottomSheet() {
          let height = view.frame.height * 0.5 // Adjust this value to match the original height of the bottom sheet view
          
          UIView.animate(withDuration: 0.3, animations: { [weak self] in
              self?.bottomSheetView.frame.origin.y += height
          }) { [weak self] _ in
              self?.bottomSheetView.removeFromSuperview()
          }
      }

    func handleAllowButtonTap() {
          let locationManager = CLLocationManager()
          locationManager.delegate = self

          if CLLocationManager.locationServicesEnabled() {
              switch CLLocationManager.authorizationStatus() {
              case .notDetermined:
                  locationManager.requestWhenInUseAuthorization() // Or .requestAlwaysAuthorization()
              case .authorizedWhenInUse, .authorizedAlways:
                  locationManager.startUpdatingLocation()
              case .denied, .restricted: break
            
                  // Handle the case when location access is denied or restricted
              @unknown default: break
                  // Handle future authorization status cases
              }
          }
        hideBottomSheet()
        
      }
}

// MARK: - Load from storyboard with dependency
extension OnboardingVC {
    class func loadFromXIB(withDependency dependency: OnboardingDependency? = nil) -> OnboardingVC? {
        let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "OnboardingVC") as? OnboardingVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - OnboardingAPIResponseDelegate
extension OnboardingVC: OnboardingAPIResponseDelegate {
}

extension OnboardingVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

extension OnboardingVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OnboardingCVC", for: indexPath) as? OnboardingCVC else { return UICollectionViewCell() }
        cell.imageHeight.constant = collectionView.frame.height * 0.6
        cell.pageControl.numberOfPages = 1
        cell.pageControl.currentPage = indexPath.row
        return cell
    }
    
    
}
