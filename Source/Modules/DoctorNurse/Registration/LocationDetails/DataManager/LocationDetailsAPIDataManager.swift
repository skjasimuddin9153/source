//
//  LocationDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class LocationDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.register, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
    func countriesAPI(completion:@escaping CountryResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.countryList,  ofType: .queryString, completion: completion)

    }
    
    func statesAPI(id: String ,completion:@escaping StateResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.stateList(countryId: id),  ofType: .queryString, completion: completion)

    }
}
