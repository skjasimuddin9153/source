
import UIKit
extension UITextField {
    func validateOTPInput(previousTextField: UITextField?, nextTextField: UITextField?, completionHandler: (() -> Void)?) {
        guard let text = text else { return }

        if text.count > 1 {
            self.text = String(text.prefix(1))
        }

        if text.count == 1, let nextField = nextTextField {
            nextField.becomeFirstResponder()
        }

        if text.isEmpty, let previousField = previousTextField {
            previousField.becomeFirstResponder()
        }

        if text.count == 1, self == nextTextField {
            completionHandler?()
        }
    }
}
extension UITextField {
    var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.nextField) as? UITextField
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.nextField, newValue, .OBJC_ASSOCIATION_ASSIGN)
        }
    }
}

struct AssociatedKeys {
    static var nextField: UInt8 = 0
}

