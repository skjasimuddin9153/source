//
//  AddNewSlotsViewModel.swift
//  Source
//
//  Created by Techwens on 24/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol AddNewSlotsAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleSuceessAPI()
}

class AddNewSlotsViewModel {
    weak var apiResponseDelegate: AddNewSlotsAPIResponseDelegate?
    lazy var localDataManager = AddNewSlotsLocalDataManager()
    lazy var apiDataManager = AddNewSlotsAPIDataManager()
    
    var dependency: AddNewSlotsDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func createSlot(type: UserType, slotSpeciality : SlotType , startTime: String, endTime: String, price: String) {
        var parameter: [String:Any] = [:]
        parameter["slot_type"] = type.rawValue
        parameter["slot_speciality"] = slotSpeciality.rawValue
        parameter["slot_time_start"] = startTime
        parameter["slot_time_end"] = endTime
        parameter["slot_price"] = price
        apiDataManager.addNewSlot(parameter: parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                guard let _ = response.data else { return }
                weakSelf.apiResponseDelegate?.handleSuceessAPI()
            }
        }
    }
}
