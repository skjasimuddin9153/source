//
//  ConfirmationCancellationVC.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class ConfirmationCancellationVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var bookingImage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    lazy var viewModel = ConfirmationCancellationViewModel()
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        guard let status = viewModel.dependency?.status else { return }
        bookingImage.image  = status.image
        lblTitle.text = status.title(name: "Random Hospital")
        lblSubTitle.text = status.subtitle
        lblHeader.isHidden = !status.headerRequired
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBacktoHome(_ sender: AppSolidButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - Load from storyboard with dependency
extension ConfirmationCancellationVC {
    class func loadFromXIB(withDependency dependency: ConfirmationCancellationDependency? = nil) -> ConfirmationCancellationVC? {
        let storyboard = UIStoryboard(name: "ConfirmationCancellation", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ConfirmationCancellationVC") as? ConfirmationCancellationVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ConfirmationCancellationAPIResponseDelegate
extension ConfirmationCancellationVC: ConfirmationCancellationAPIResponseDelegate {
}
