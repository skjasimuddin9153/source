//
//  MyCartViewController.swift
//  Source
//
//  Created by Techwens Software on 05/09/23.
//

struct Card{
    let cardImage:UIImage
    let cardNo:String
    let cardName:String
    let cardExp:String
}
import UIKit
import TTGSnackbar

class MyCardsViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet var headerView: HeaderView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var cardBoxheight: NSLayoutConstraint!
    @IBOutlet var cardAddBox: UIView!
    var cardIsOPen : Bool = false
    @IBOutlet var tfCardNo: UITextField!
    @IBOutlet var tfExp: UITextField!
    @IBOutlet var tfCvv: UITextField!
    @IBOutlet var cardBox: UIView!
    @IBOutlet var tableHEIGHT: NSLayoutConstraint!
    
    var isCardOpen :Bool = false
    
    var card :[Card] = [
        Card(cardImage: UIImage(named: "Mask group")!, cardNo: "*****89765", cardName: "Jasimuddin", cardExp: "10/25"),
        Card(cardImage: UIImage(named: "Mask group")!, cardNo: "*****89765", cardName: "Jasimuddin", cardExp: "10/25"),
        Card(cardImage:UIImage(named: "Mask group")!, cardNo: "*****89765", cardName: "Jasimuddin", cardExp: "10/25")
    ]{
        didSet{
            tableHEIGHT.constant = CGFloat(90 * card.count)
            UIView.animate(withDuration: 0.3) {
                  self.view.layoutIfNeeded()
              }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        style()
        headerView.navigationController = self.navigationController
     
        headerView.title.text = "Saved cards"
        // Do any additional setup after loading the view.
//        cardViewguester()
        tfCvv.delegate = self
        tfCardNo.delegate = self
        tfExp.delegate = self
    }
    
    private func style(){
       
        cardBox.borderColor = .black
        cardBox.borderWidth = 1
        cardBoxheight.constant = 60
        cardAddBox.isHidden = true
        
//        control table hight based on total card dynmically
        tableHEIGHT.constant = CGFloat(90 * card.count)
       
    }
    
    @IBAction func onAddcardClicked(_ sender: Any) {
        cardIsOPen = !cardIsOPen
        
        if cardIsOPen {
            cardBoxheight.constant = 235
            UIView.animate(withDuration: 0.3) {
                  self.view.layoutIfNeeded()
              }
            cardAddBox.isHidden = false
        } else{
            cardBoxheight.constant = 60
            UIView.animate(withDuration: 0.3) {
                  self.view.layoutIfNeeded()
              }
            cardAddBox.isHidden = true
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }

        if textField == tfCardNo {
            if currentText.count < 19 {
                textField.text = currentText.grouping(every: 4, with: " ")
                return true // Allow input if CVV is less than or equal to 3 digits
            }else{
                return false
            }
          
        }
        else if textField == tfExp {
            if currentText.count <= 3 {// Expiry Date Text Field
            textField.text = currentText.grouping(every: 2, with: "/")
         
                return true // Allow input if CVV is less than or equal to 3 digits
            }
            else{
                return false
            }
        }
        else if textField == tfCvv { // CVV Text Field
                if currentText.count <= 3 {
                    return true // Allow input if CVV is less than or equal to 3 digits
                } else {
                    return false // Don't allow input if CVV exceeds 3 digits
                }
            }
        return true
    }
    
    
    @IBAction func onSaveCardClicked(_ sender: Any) {
        guard let cardNo = tfCardNo.text,
               let expDate = tfExp.text,
               let cvv = tfCvv.text else {
             return
         }
        if isCardDetailsValid(cardNo: cardNo, expDate: expDate, cvv: cvv) {
            let hiddenDigits = String(repeating: "*", count: min(5, cardNo.count))
            let hiddenCardNo = hiddenDigits + cardNo.dropFirst(5)
            let newCard = Card(cardImage: UIImage(named: "Mask group")!, cardNo: hiddenCardNo, cardName: "Your Name", cardExp: expDate)
               card.append(newCard)

               // Update the table view to reflect the changes with animation
               tableView.beginUpdates()
               tableView.insertRows(at: [IndexPath(row: card.count - 1, section: 0)], with: .fade)
               tableView.endUpdates()

               // Reset the text fields and hide the card input box
               tfCardNo.text = ""
               tfExp.text = ""
               tfCvv.text = ""
               onAddcardClicked(self) // Close the card input box with animation
           } else {
               // Handle invalid card details, e.g., show an error message
           }
        
    }
    func isCardDetailsValid(cardNo: String, expDate: String, cvv: String) -> Bool {
        if(cardNo.isEmpty && expDate.isEmpty && cvv.isEmpty){
            let snackbar = TTGSnackbar(message: "Invalid Entry please provide all details", duration: .middle)
            snackbar.backgroundColor = .red
            snackbar.show()
            return false
            
          
        }
        // Add your validation logic here
        // For example, you can check if cardNo, expDate, and cvv are in the correct format and valid
        // Return true if the card details are valid, otherwise return false
        return true // Change this to your validation logic
    }

    
    //    private func cardViewguester(){
//        let cardGuester = UITapGestureRecognizer(target: self, action: #selector(onCardClicked))
//        cardViewEdit.addGestureRecognizer(cardGuester)
//
//    }
//    @objc func onCardClicked(){
//
//        isCardOpen = !isCardOpen
//
//        if isCardOpen {
//            addCardHeight.constant = 225
//            UIView.animate(withDuration: 0.3) {
//                self.view.layoutIfNeeded()
//            }
//        }
//        if !isCardOpen {
//            addCardHeight.constant = 40
//        }
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyCardsViewController {
    
    class func loadFromXIB() -> MyCardsViewController? {
        let storyboard = UIStoryboard(name: "MyCards", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MyCardsViewController") as? MyCardsViewController else {
            return nil
        }
//        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// handle TableView

extension MyCardsViewController : UITabBarDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        card.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MyCardTableViewCell  = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyCardTableViewCell
        cell.selectionStyle = .none
        cell.cardLogo.image = card[indexPath.row].cardImage
        cell.cardNo.text =  card[indexPath.row].cardNo
        cell.cardSubLabel.text =  "\(card[indexPath.row].cardName) , Exp: \(card[indexPath.row].cardExp)"
        cell.onDeleteCard = {
            if let indexPath = tableView.indexPath(for: cell) {
                   self.card.remove(at: indexPath.row)
                   tableView.beginUpdates()
                   tableView.deleteRows(at: [indexPath], with: .fade)
                   tableView.endUpdates()
               }
        }
        return cell
       
    }
    
    
    
}
extension String {
    func grouping(every groupSize: Int, with separator: Character) -> String {
        let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
        var result = ""
        var counter = 0
        
        for char in cleanedUpCopy {
            if counter == groupSize {
                result.append(separator)
                counter = 0
            }
            result.append(char)
            counter += 1
        }
        
        return result
    }
}

