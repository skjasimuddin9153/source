//
//  SearchVC.swift
//  Source
//
//  Created by Techwens on 27/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnEmergency: UIButton!
    @IBOutlet weak var btnICU: UIButton!
    @IBOutlet var specaialityBtnGroup: [UIButton]!
    @IBOutlet weak var tfSearchBar: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var selectedSlotType: SlotType = .all {
        didSet {
            deselectAllBtn()
            switch selectedSlotType {
            case .all:
                selectSpecialityBtn(btnAll)
            case .emergency:
                selectSpecialityBtn(btnEmergency)
            case .icu:
                selectSpecialityBtn(btnICU)
            }
        }
    }
    lazy var viewModel = SearchViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        selectedSlotType = .all
        viewModel.apiResponseDelegate = self
        tfSearchBar.setupSourceTextField(withPlaceholder: "Search Here for hospitals",radius: Int(tfSearchBar.bounds.height/2))
        tfSearchBar.setRightViewButton(with: UIImage(named: "icSearchButton")!)
        tfSearchBar.addTarget(self, action: #selector(textUpdate), for: .editingChanged)
        setupTableView()
    }
    override func viewDidLayoutSubviews() {
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func deselectAllBtn() {
        specaialityBtnGroup.forEach({$0.backgroundColor = .pickerGrey ?? .gray})
    }
    func selectSpecialityBtn(_ sender: UIButton){
        sender.backgroundColor = .accentColor ?? .yellow
    }
    @IBAction func tappedAllBtn(_ sender: UIButton) {
        selectedSlotType = .all
    }
    @IBAction func tappedEmergencyBtn(_ sender: UIButton) {
        selectedSlotType = .emergency
    }
    @IBAction func tappedICUBtn(_ sender: UIButton) {
        selectedSlotType = .icu
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension SearchVC {
    class func loadFromXIB(withDependency dependency: SearchDependency? = nil) -> SearchVC? {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - SearchAPIResponseDelegate
extension SearchVC: SearchAPIResponseDelegate {
    func handleGetHospitaListResponse() {
        tableView.reloadData()
    }
    
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
}

extension SearchVC {
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200.0
    }
    
    @objc func textUpdate(textfield: UITextField){
        // filter name
        
        if let searchText = textfield.text{
            if searchText != "" {
                viewModel.state = .searchResponse
                viewModel.getExploreHospitalData(keyword: searchText, speciality: selectedSlotType)
            } else {
                viewModel.state = .recentSearch
            }
            
        } else {
            viewModel.state = .recentSearch
        }
        tableView.reloadData()
    }
}

extension SearchVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
       return viewModel.numberOfSection()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewModel.viewForHeaderInSection(section: section)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellForRowAt(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.heightForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch viewModel.state {
        case .searchResponse:
            var searchHistory = viewModel.recentSearch
            searchHistory.append(viewModel.currentAreaHospital[indexPath.row].hospital_name ?? "Error")
            viewModel.recentSearch = searchHistory
            DataManager.shared.saveUserSearch(searchKeys: searchHistory)
            let id = viewModel.currentAreaHospital[indexPath.row].id
            let dependency = BookingTicketDependency(hospitalID: id)
            guard let vc = BookingTicketVC.loadFromXIB(withDependency: dependency) else { return }
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case .recentSearch:
            tfSearchBar.text = viewModel.recentSearch[indexPath.row]
            viewModel.state = .searchResponse
            viewModel.getExploreHospitalData(keyword: viewModel.recentSearch[indexPath.row], speciality: selectedSlotType)

        }
    }
}
