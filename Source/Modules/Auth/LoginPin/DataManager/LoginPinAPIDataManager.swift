//
//  LoginPinAPIDataManager.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias LoginResponseCompletion = (Result<APIResponse<LoginData>, Error>) -> Void
class LoginPinAPIDataManager: APIDataManager{
    init() {
    }
    // Data fetch service methods goes here
    func loginAPI(with parameter: [String: Any] , completion:@escaping LoginResponseCompletion){
        makeAPICall(to: AuthEndpoints.login, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
