//
//  RegistrationDocsVC.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum RegistrationDocsDropdown {
    case country
    case state
}
class RegistrationDocsVC: UIViewController {
    @IBOutlet weak var tfState: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfRegistrationNo: UITextField!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var frontBtnView: UIView!
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var btnSend: AppSolidButton!
    // MARK: Instance variables
	lazy var viewModel = RegistrationDocsViewModel()
    var acticeDropDown: RegistrationDocsDropdown?
    var activeImagePicker: DocumentPage?
    var payloadData : [UploadPayloadData] = []
    var parameter : [String: String] = [:]
    var imagePicker = UIImagePickerController()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        viewModel.getAllCountries()
        tfCountry.setupSourceTextField(withPlaceholder: "Registration Country*")
        tfState.setupSourceTextField(withPlaceholder: "Registration state*")
        tfRegistrationNo.setupSourceTextField(withPlaceholder: "Registration Number")
        tfRegistrationNo.autocapitalizationType = .allCharacters
        tfCountry.delegate = self
        tfCountry.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfCountry.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedCountryNameDropDown()
        }
        tfState.delegate = self
        tfState.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfState.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedStateNameDropDown()
        }
        frontBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .first
            welf.showPickerAlert()
        }
        backBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .second
            welf.showPickerAlert()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func tappedCountryNameDropDown() {
        print("Open drop down one")
        acticeDropDown = .country
        let dependency = PickerViewDependency(title: "Select Country", dataList: viewModel.countryList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    func tappedStateNameDropDown() {
        if viewModel.stateList.isEmpty {
            showSnackbar(withMessage: "Please Select a country first")
            return
        }
        acticeDropDown = .state
        let dependency = PickerViewDependency(title: "Select State", dataList:viewModel.stateList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    
    func allFieldsValid() -> Bool {
        
        if (tfCountry.text?.isEmpty ?? true) || tfCountry.text == ""{
            showSnackbar(withMessage: "Country is Invalid")
            tfCountry.shake()
            return false
        }
        
        if (tfState.text?.isEmpty ?? true) || tfState.text == ""{
            showSnackbar(withMessage: "State is Invalid")
            tfState.shake()
            return false
        }
        
        if  (tfRegistrationNo.text?.isEmpty ?? true) || tfRegistrationNo.text == "" {
            showSnackbar(withMessage: "Registration No is Invalid")
            tfRegistrationNo.shake()
            return false
        }
        
        guard let data1 = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Front Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data1) {
            showSnackbar(withMessage: "Image is too large")
            frontImageView.shake()
            return false
        }
        guard let data2 = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Back Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data2) {
            showSnackbar(withMessage: "Image is too large")
            backImageView.shake()
            return false
        }
        return true
    }
    
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedNextBtn(_ sender: AppSolidButton) {
        
        if !allFieldsValid() {
           return
        }
        guard let frontImage = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        guard let backImage = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        let gFrontpayloadData = UploadPayloadData(data: frontImage, name: "registration_document_front_file", filename: "hrFrontImage.jpeg", mimeType: .jpeg)
        let gBackpayloadData = UploadPayloadData(data: backImage, name: "registration_document_back_file", filename: "hrBackImage.jpeg", mimeType: .jpeg)
        payloadData.append(gFrontpayloadData)
        payloadData.append(gBackpayloadData)
        parameter["registration_country"] = viewModel.countryList.filter({$0.name == tfCountry.text!}).first?.country_code ?? ""
        parameter["registration_state"] = viewModel.stateList.filter({$0.name == tfState.text!}).first?.state_code ?? ""
        parameter["registration_number"] = tfRegistrationNo.text!
        parameter["prospect_id"] = "\(viewModel.dependency!.prospect_id!)"
        parameter["step"] = "3"
        print(parameter)
        print(payloadData)
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.hospitalRegisterStepThree(parameter: parameter, payload: payloadData)

       
    }
    @IBAction func tapepedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension RegistrationDocsVC {
    class func loadFromXIB(withDependency dependency: RegistrationDocsDependency? = nil) -> RegistrationDocsVC? {
        let storyboard = UIStoryboard(name: "RegistrationDocs", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "RegistrationDocsVC") as? RegistrationDocsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - RegistrationDocsAPIResponseDelegate
extension RegistrationDocsVC: RegistrationDocsAPIResponseDelegate {
    func handleRegisterationStep3Response(message: String) {
        btnSend.hideSpinner()
        view.dismissLoader()
        showSnackbar(withMessage: message)
        view.isUserInteractionEnabled = true
        let dependency = HospitalBankDetailsDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = HospitalBankDetailsVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func handleCountryListResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
    }
    
    func handleStateListResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
    }
}

extension RegistrationDocsVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case tfCountry :
            tappedCountryNameDropDown()
            return false
        case tfState :
            tappedStateNameDropDown()
            return false

        default:
            return true
        }
    }
}

extension RegistrationDocsVC: SelectInfoFromPickerDelegate {
    func didInfoSelected(value: String, index: Int) {
        switch acticeDropDown {
        case .country:
            tfCountry.text = value
            tfState.text = ""
            let code = viewModel.countryList[index].country_code ?? "IN"
            view.showLoader(withBlur: true)
            view.isUserInteractionEnabled = false
            viewModel.getAllState(byCountryId: code)
        case .state:
            tfState.text = value
        case .none:
            ()
        }
        acticeDropDown = nil
    }
    
    func noSelection() {
        acticeDropDown = nil
    }
}

extension RegistrationDocsVC {
    func openPicker(type: UIImagePickerController.SourceType) {
            imagePicker.sourceType = type
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .currentContext
            imagePicker.delegate = self
            present(imagePicker, animated: true)
        
        
    }
    
    func showPickerAlert(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ [self] _ in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                openPicker(type: .camera)
            }
            else{
                showSnackbar(withMessage: "No Camera Available On This Device")
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default , handler:{ [self] _ in
            openPicker(type: .photoLibrary)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ _ in
            
        }))
        
       present(alert, animated: true, completion: nil)
    }
}

extension RegistrationDocsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        switch activeImagePicker {
        case .first:
            frontImageView.image = tempImage
        case .second:
            backImageView.image = tempImage
        default:
            ()
        }
        activeImagePicker = nil
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activeImagePicker = nil
        dismiss(animated: true, completion: nil)
    }
}
