//
//  ConfirmationCancellationDependency.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum BookingStatus {
    case success
    case cancel
    
    var headerRequired: Bool {
        switch self {
        case .success:
            return true
        case .cancel:
            return false
        }
    }
    
    var image: UIImage {
        switch self {
        case .success:
            return UIImage(named: "ichandshake")!
        case .cancel:
            return UIImage(named: "iccancelled")!
        }
    }
    func title(name: String)-> String {
        switch self {
        case .success:
            return "Your booking has been confirmed for \(name)"
        case .cancel:
            return "Your booking has been cancelled for \(name) successfully."
        }
    }
    
    var subtitle: String {
        switch self {
        case .success:
            return "A confirmation mail has been sent your mail. "
        case .cancel:
            return "A cancellation mail has been sent your mail. "
        }
    }
    
}

struct ConfirmationCancellationDependency {
    let status : BookingStatus
}
