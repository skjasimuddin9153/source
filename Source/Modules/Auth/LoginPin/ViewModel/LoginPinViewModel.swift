//
//  LoginPinViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import Alamofire

protocol LoginPinAPIResponseDelegate: class {
    func handleLoginResponse(data: UserType)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

final class LoginPinViewModel {
  
    

    enum Event {
        case loading
        case stopLoading
        case dataLoaded
        case error(Error)
        case data(LoginResponse)
    }

    var eventHandler: ((Event) -> Void)?

    // Your existing login and other functions here...

    func verify(countryCode: String, mobilePhone: String,otp:String,authKey:String) {
        self.eventHandler?(.loading) // Notify the loading state
        
        let verifyData: [String: Any] = [
            "country_code": countryCode,
                "phone":mobilePhone,
                "otp":otp
        ]
        let header : HTTPHeaders = [
            "Content-Type":"application/json",
            "auth_key":authKey
            
        ]
        
        APIManager.shared.request(
            method: .post,
            endpoint:AuthEndpoints.verifyOtp,
            parameters: verifyData,
            headers: header
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: LoginResponse = try JSONDecoder().decode(LoginResponse.self, from: data)
                    print("Success: \(json)")
                    self.eventHandler?(.data(json)) // Notify success and pass data
                  
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
}
