import UIKit

struct AppTabBarViewModel {

    let tabItemData: [AppTabItemData] = [
        AppTabItemData(viewController: HomePageVC.loadFromXIB(),
                       image: UIImage(named: "homeDefault")!,
                       selectedImage: UIImage(named: "homeSelected")!,
                      title: "Home"),
        AppTabItemData(viewController: SearchVC.loadFromXIB(),
                       image: UIImage(named: "searchDefault")!,
                       selectedImage: UIImage(named: "searchSelected")!,
                      title: "Search"),
        AppTabItemData(viewController: ScheduledAppointmentVC.loadFromXIB(),
                       image: UIImage(named: "scheduleDefault")!,
                       selectedImage: UIImage(named: "scheduleSelected")!,
                      title: "Sechedule"),
        AppTabItemData(viewController: DemoVC.loadFromXIB(),
                       image: UIImage(named: "earningDefault")!,
                       selectedImage: UIImage(named: "earningSelected")!,
                      title: "Earning"),
        AppTabItemData(viewController: LogoutDemoVC.loadFromXIB(),
                       image: UIImage(named: "menuDefault")!,
                       selectedImage: UIImage(named: "menuSelected")!,
                      title: "Menu")
    ]

    init() {

    }
}
