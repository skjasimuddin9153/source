//
//  CurrentOrderViewController.swift
//  Source
//
//  Created by Techwens Software on 08/09/23.
//

import UIKit
import MapKit

//struct Product {
//    let image: UIImage
//    let title: String
//    let description: String
//    let price:String
//}

class CurrentOrderViewController: UIViewController {

    @IBOutlet var clouseBtn: UIButton!
    @IBOutlet var helpBtn: UIButton!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    
    var products: [Product] = [
        Product(
                id: 1,
                name: "Electronic Metal Pants",
                thcPercentage: "10.03%",
                marketPrice: 82.3,
                actualPrice: 11.22,
                rating: 3,
                imageUrl: "https://loremflickr.com/640/480?lock=5855526705430528",
                strain: Product.Strain(name: "HYBRID"),
                brand: Product.Brand(name: "Wiegand, Jacobson and Zboncak")
            ),
            Product(
                id: 2,
                name: "Practical Plastic Shirt",
                thcPercentage: "25.28%",
                marketPrice: 17.37,
                actualPrice: 23.32,
                rating: 3,
                imageUrl: "https://picsum.photos/seed/FU5j6/640/480",
                strain: Product.Strain(name: "INDICA"),
                brand: Product.Brand(name: "Klein and Sons")
            ),
    ]
    // Populate the array with sample products
//    let product1 = Product(image: UIImage(named: "product1Image")!, title: "Product 1", description: "This is the description of Product 1.")
//    let product2 = Product(image: UIImage(named: "product2Image")!, title: "Product 2", description: "This is the description of Product 2.")
//    let product3 = Product(image: UIImage(named: "product3Image")!, title: "Product 3", description: "This is the description of Product 3.")

   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mapViewfunc()
        style()
    }
    
    @IBAction func onClouseClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    private func mapViewfunc(){
        let latitude: CLLocationDegrees = 22.573177996807438
        let longitude: CLLocationDegrees = 88.4342746649035
        let latDelta: CLLocationDegrees = 0.01
        let longDelta: CLLocationDegrees = 0.01

        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let span = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: true)


    }
    private func style(){
        clouseBtn.allCornersRadius = 20
        helpBtn.allCornersRadius = 20
        tableHeight.constant = CGFloat(products.count * 70)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CurrentOrderViewController {
    class func loadFromXIB() -> CurrentOrderViewController? {
        let storyboard = UIStoryboard(name: "CurrentOrder", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CurrentOrderViewController") as? CurrentOrderViewController else {
            return nil
        }
      
        return viewController
    }
}

extension CurrentOrderViewController :UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CurrentOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CurrentOrderTableViewCell
        let product = products[indexPath.row]
            
            // Populate the cell with product data
            cell.productImage.image = UIImage(named: product.imageUrl)
        cell.productTitle.text = product.name
        cell.productDescription.text = "THC: \(product.thcPercentage)"
        cell.productPrice.text = "Price: $\(product.actualPrice)"
            
            cell.selectionStyle = .none
            return cell
    }
    
    
}
