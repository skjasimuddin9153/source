//
//  CartViewController.swift
//  Source
//
//  Created by Techwens Software on 01/09/23.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backbtnClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ondeleteAllClicked(_ sender: Any) {
        
        cartdataManager.shared.data.removeAll()
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
        tableView.reloadData()
    }
    @IBAction func checkoutBtnClicked(_ sender: Any) {
        guard let vc = CheckoutViewController.loadFromXIB() else {return}
                navigationController?.pushViewController(vc, animated: true)
    }
}

extension CartViewController:UITableViewDataSource,UITabBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartdataManager.shared.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CartTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! CartTableViewCell
        cell.itemCount.text = String(cartdataManager.shared.data[indexPath.row].itemCount)
        cell.productImage.image =  cartdataManager.shared.data[indexPath.row].productImage
        cell.productTitle.text =  cartdataManager.shared.data[indexPath.row].productTitle
        cell.productSubTitle.text =  cartdataManager.shared.data[indexPath.row].productSubTitle
        cell.addMorecart = {
            cartdataManager.shared.data[indexPath.row].itemCount += 1
            tableView.reloadData()
        }
        cell.removeMorecart = {
            if cartdataManager.shared.data[indexPath.row].itemCount == 1 {
                // Remove the item from the data source
             
                  
                    
//                   if let indexPath = tableView.indexPath(for: cell) {
                cartdataManager.shared.data.remove(at: indexPath.row)
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
            
            
            } else {
                cartdataManager.shared.data[indexPath.row].itemCount -= 1
             
            }
//            UIView.animate(withDuration: 0.3, animations: {
//                self.view.layoutIfNeeded()
//            })
            
            tableView.reloadData()
        }


        return cell
    }
    
    
}
extension CartViewController {
    class func loadFromXIB(withDependency dependency: CartProduct? = nil) -> CartViewController? {
        let storyboard = UIStoryboard(name: "Cart", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController else {
            return nil
        }
        return viewController
    }
}
