//
//  PersonalDetailsVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum Gender: String {
    case male = "M"
    case female = "F"
    case other = "O"
}

class PersonalDetailsVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var maleCheckBoxImage: UIImageView!
    @IBOutlet weak var femaleCheckBoxImage: UIImageView!
    @IBOutlet weak var maleBtnView: UIView!
    @IBOutlet weak var femaleBtnView: UIView!
    @IBOutlet weak var btnSend: AppSolidButton!
    lazy var viewModel = PersonalDetailsViewModel()
    var selecedGender: Gender? {
        didSet {
            switch selecedGender {
            case .female:
                femaleCheckBoxImage.image = #imageLiteral(resourceName: "icCheckedBox")
                maleCheckBoxImage.image = #imageLiteral(resourceName: "icUncheckedBox")
            case .male:
                maleCheckBoxImage.image = #imageLiteral(resourceName: "icCheckedBox")
                femaleCheckBoxImage.image = #imageLiteral(resourceName: "icUncheckedBox")
            default:
                ()
            }
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfFirstName.setupSourceTextField(withPlaceholder: "First Name")
        tfLastName.setupSourceTextField(withPlaceholder: "Last Name")
        tfEmail.setupSourceTextField(withPlaceholder: "Email")
        tfEmail.keyboardType = .emailAddress
        tfPhone.setupSourceTextField(withPlaceholder: "Phone No")
        tfPhone.keyboardType = .phonePad
        tfPhone.delegate = self
        maleBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selecedGender = .male
        }
        femaleBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selecedGender = .female
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        
        if !allFieldsValid() {
           return
        }
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.register(userType: viewModel.dependency!.userType, firstName: tfFirstName.text!, lastName: tfLastName.text!, email: tfEmail.text!, gender: selecedGender!, phoneNo: tfPhone.text!)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func allFieldsValid() -> Bool {
        if !tfFirstName.validateWith(regex: .name) {
            showSnackbar(withMessage: TextFieldValidationRegex.name.errorMessage)
            return false
        }
        if !tfLastName.validateWith(regex: .name) {
            showSnackbar(withMessage: TextFieldValidationRegex.name.errorMessage)
            return false
        }
        if !tfEmail.validateWith(regex: .email) {
            showSnackbar(withMessage: TextFieldValidationRegex.email.errorMessage)
            return false
        }
        if !tfPhone.validateWith(regex: .phoneNo) {
            showSnackbar(withMessage: TextFieldValidationRegex.phoneNo.errorMessage)
            return false
        }
        if selecedGender == nil {
            showSnackbar(withMessage: "Select a gender")
            return false
        }
        return true
    }
}

// MARK: - Load from storyboard with dependency
extension PersonalDetailsVC {
    class func loadFromXIB(withDependency dependency: PersonalDetailsDependency? = nil) -> PersonalDetailsVC? {
        let storyboard = UIStoryboard(name: "PersonalDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PersonalDetailsVC") as? PersonalDetailsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - PersonalDetailsAPIResponseDelegate
extension PersonalDetailsVC: PersonalDetailsAPIResponseDelegate {
    func handleRegisterResponse(data: RegisterStepOneData) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        let dependency = CodeVerificationDependency(user: viewModel.dependency?.userType ?? .doctor, flow: .registration, prospect_id: data.prospect_id, otp_valid_for: data.otp_valid_for)
        guard let vc = CodeVerificationVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
    }
    
}

extension PersonalDetailsVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)

        return newString.count <= maxLength
    }
}
