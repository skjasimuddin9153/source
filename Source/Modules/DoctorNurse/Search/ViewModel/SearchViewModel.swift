//
//  SearchViewModel.swift
//  Source
//
//  Created by Techwens on 27/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

protocol SearchAPIResponseDelegate: AnyObject {
    func handleGetHospitaListResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

enum SearchTableState {
    case recentSearch
    case searchResponse
}

class SearchViewModel {
    weak var apiResponseDelegate: SearchAPIResponseDelegate?
    lazy var localDataManager = SearchLocalDataManager()
    lazy var apiDataManager = SearchAPIDataManager()
    
    var dependency: SearchDependency?
    var state: SearchTableState = .recentSearch
    var recentSearch: [String] = DataManager.shared.getUserSearch()
    var currentAreaHospital: [HospitalsModel] = []
    
    init() {
    }
    // Data fetch service methods goes here
    
    func getExploreHospitalData(keyword: String, speciality: SlotType) {
        let parameter =  ["search": keyword ,
                          "speciality": speciality.rawValue ] as [String: AnyObject]
        apiDataManager.getHospitalList(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.currentAreaHospital = data.hospitals ?? []
                    weakSelf.apiResponseDelegate?.handleGetHospitaListResponse()
                  //  weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }

            }
        }
    }
    
    
    func cellForRowAt(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        switch state {
        case .recentSearch:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchHistoryTVC") as? SearchHistoryTVC else { return UITableViewCell() }
            cell.lblSearchTitle.text = recentSearch[indexPath.row]
            return cell
        case .searchResponse:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResponseTVC") as? SearchResponseTVC else { return UITableViewCell() }
            cell.hospitalData = currentAreaHospital[indexPath.row]
            return cell
        }

    }
    
    func heightForSection(section: Int) -> CGFloat {
        return 44.0
    }
    
    func numberOfRowInSection(section: Int) -> Int {
        switch state {
        case .recentSearch:
            return recentSearch.count
        case .searchResponse:
            return currentAreaHospital.count
        }
    }
    
    func numberOfSection() -> Int {
        return 1
    }
    
    func viewForHeaderInSection(section: Int) -> UIView? {
        let header = SearchTVHeader.instanceFromNib()
        switch state {
        case .recentSearch:
            header.lblHeader.text = "Recent Search"
        case .searchResponse:
            header.lblHeader.text = "Search Result"
        }
        return header
    }
}
