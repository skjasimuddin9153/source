//
//  OwnerInfoDependency.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct OwnerInfoDependency {
    let parameter : [String: String]
}
