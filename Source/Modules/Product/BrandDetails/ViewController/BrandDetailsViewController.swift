//
//  BrandDetailsViewController.swift
//  Source
//
//  Created by Techwens Software on 14/09/23.
//

import UIKit

class BrandDetailsViewController: UIViewController {

    @IBOutlet var clouseBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()

        // Do any additional setup after loading the view.
    }
    
    
    private func style(){
        clouseBtn.layer.cornerRadius = 20
    }

    @IBAction func onClouseBtnClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BrandDetailsViewController {
    class func loadFromXIB() -> BrandDetailsViewController? {
        let storyboard = UIStoryboard(name: "BrandDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "BrandDetailsViewController") as? BrandDetailsViewController else {
            return nil
        }
      
        return viewController
    }
}
