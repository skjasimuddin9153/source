//
//  SplashScreenViewModel.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol SplashScreenAPIResponseDelegate: class {
    func handleGetUserResponse(data: DetailsResponseModel)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class SplashScreenViewModel {
    weak var apiResponseDelegate: SplashScreenAPIResponseDelegate?
    lazy var localDataManager = SplashScreenLocalDataManager()
    lazy var apiDataManager = SplashScreenAPIDataManager()
    
    var dependency: SplashScreenDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func getUserData() {
        apiDataManager.getUseDetails {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.apiResponseDelegate?.handleGetUserResponse(data: data)
                    weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }

            }
        }
    }
}
