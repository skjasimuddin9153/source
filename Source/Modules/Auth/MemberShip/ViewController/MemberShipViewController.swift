//
//  MemberShipViewController.swift
//  Source
//
//  Created by Techwens Software on 30/08/23.
//

import UIKit

class MemberShipViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnletsStart: UIButton!
    lazy var viewModel = MemberShipViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onStartClick(_ sender: Any) {
        guard let vc = ProductDetailsViewController.loadFromXIB() else {return }
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension MemberShipViewController {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 600
    }
    
    class func loadFromXIB(withDependency dependency: MemberShipependency? = nil) -> MemberShipViewController? {
        let storyboard = UIStoryboard(name: "MemberShip", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MemberShipViewController") as? MemberShipViewController else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}


// add tableView

extension MemberShipViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell:MemberShipTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MemberShipTableViewCell
        return cell
    }
    

    
    
}
