//
//  BookingTicketAPIDataManager.swift
//  Source
//
//  Created by Techwens on 08/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias HospitalDetailsResponseCompletion = (Result<APIResponse<HospitalDetailsResponseModel>, Error>) -> Void
class BookingTicketAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func getHospitalDetails(parameter: [String: AnyObject] ,completion:@escaping HospitalDetailsResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.hospitalDetails, withParameters: parameter, ofType: .queryString, completion: completion)
    }
    
    func submitBookingData(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.bookHospital, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func addToFavourite(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.markFavourite, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func removeFromFavourite(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.removeFavourite, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
}
