//
//  HospitalHomeTVC.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//

import UIKit

class HospitalHomeTVC: UITableViewCell {

    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblSpeciality: UILabel!
    @IBOutlet weak var lblDocName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var data: Booking_list? {
        didSet {
            lblDocName.text = data?.booking_made_by?.first_name ?? ""
            lblSpeciality.text = data?.slot_speciality?.rawValue ?? ""
            lblTime.text = (data?.slot_start_time ?? "") + " - " + (data?.slot_end_time ?? "")
            
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let bookingDate = formatter.date(from: data?.booking_start_time ?? "")
            formatter.dateFormat = "dd-MMM-yyyy"
            let formattedData = formatter.string(from: bookingDate!)
            lblDate.text = formattedData
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
