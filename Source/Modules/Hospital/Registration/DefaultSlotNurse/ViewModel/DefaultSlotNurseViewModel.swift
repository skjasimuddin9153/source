//
//  DefaultSlotNurseViewModel.swift
//  Source
//
//  Created by Techwens on 03/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol DefaultSlotNurseAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class DefaultSlotNurseViewModel {
    weak var apiResponseDelegate: DefaultSlotNurseAPIResponseDelegate?
    lazy var localDataManager = DefaultSlotNurseLocalDataManager()
    lazy var apiDataManager = DefaultSlotNurseAPIDataManager()
    
    var dependency: DefaultSlotNurseDependency?
    var icuSlots: [SlotDataModel] = []
    var emergencySlots: [SlotDataModel] = []
    
    init() {
    }
    func getDataDictonaries() -> SlotNurseRequestData {
        var slotList : [SlotEncoData] = []
        
        for (_, slot) in icuSlots.enumerated() {
            let docSlot = SlotEncoData(speciality: SlotType.icu.rawValue, slot_time_start: slot.startTime.slotFormatTime(), slot_time_end: slot.endTime.slotFormatTime(), slot_price: slot.price)
            slotList.append(docSlot)
        }
        for (_, slot) in emergencySlots.enumerated() {
            let docSlot = SlotEncoData(speciality: SlotType.emergency.rawValue, slot_time_start: slot.startTime.slotFormatTime(), slot_time_end: slot.endTime.slotFormatTime(), slot_price: slot.price)
            slotList.append(docSlot)
        }
        let requestData = SlotNurseRequestData(prospect_id: "\(dependency!.prospect_id!)", step: "7", nurse_slots: slotList)
        return requestData
                
    }
    // Data fetch service methods goes here
    func register() {
        let parameters = getDataDictonaries().dictionary
         print(parameters)
        apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
   
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
