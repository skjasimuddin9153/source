//
//  HospitalRegisterDataModel.swift
//  Source
//
//  Created by Techwens on 01/07/22.
//


import Foundation
struct HospitalRegisterDataModel : Codable {
    let prospect_id : Int?
    let otp_valid_for : Int?
    let otp : Int?

    enum CodingKeys: String, CodingKey {

        case prospect_id = "prospect_id"
        case otp_valid_for = "otp_valid_for"
        case otp = "otp"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        prospect_id = try values.decodeIfPresent(Int.self, forKey: .prospect_id)
        otp_valid_for = try values.decodeIfPresent(Int.self, forKey: .otp_valid_for)
        otp = try values.decodeIfPresent(Int.self, forKey: .otp)
    }

}
