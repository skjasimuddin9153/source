//
//  HospitalBankDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol HospitalBankDetailsAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
    func handleBankResponse()
}

class HospitalBankDetailsViewModel {
    weak var apiResponseDelegate: HospitalBankDetailsAPIResponseDelegate?
    lazy var localDataManager = HospitalBankDetailsLocalDataManager()
    lazy var apiDataManager = HospitalBankDetailsAPIDataManager()
    
    var dependency: HospitalBankDetailsDependency?
    var bankList: [BankDataModel] = []
    init() {
    }
    // Data fetch service methods goes here
    func register(prospect_id: String, bankName: String, account_holder_name: String, account_number: String, ifsc_code: String) {
        let parameters = [
            "prospect_id": prospect_id,
            "step":"4",
            "bank_name":bankName,
            "account_holder_name":account_holder_name,
            "account_number":account_number,
            "ifsc_code":ifsc_code
        ]
        apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
   
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
    
    func getBankList() {
        apiDataManager.getBankAPI{ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                guard let data = response.data else { return }
                weakSelf.bankList = data
                weakSelf.apiResponseDelegate?.handleBankResponse()
            }
        }
    }
}
