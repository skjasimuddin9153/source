//
//  ChangeLocationVC.swift
//  Source
//
//  Created by Techwens on 24/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class ChangeLocationVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfSearchLocation: UITextField!
    lazy var viewModel = ChangeLocationViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfSearchLocation.setupSourceTextField(withPlaceholder: "Search for area street name...")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - Load from storyboard with dependency
extension ChangeLocationVC {
    class func loadFromXIB(withDependency dependency: ChangeLocationDependency? = nil) -> ChangeLocationVC? {
        let storyboard = UIStoryboard(name: "ChangeLocation", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ChangeLocationVC") as? ChangeLocationVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ChangeLocationAPIResponseDelegate
extension ChangeLocationVC: ChangeLocationAPIResponseDelegate {
}
