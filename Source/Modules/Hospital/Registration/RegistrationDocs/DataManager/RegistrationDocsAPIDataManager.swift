//
//  RegistrationDocsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class RegistrationDocsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func countriesAPI(completion:@escaping CountryResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.countryList,  ofType: .queryString, completion: completion)

    }
    
    func statesAPI(id: String ,completion:@escaping StateResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.stateList(countryId: id),  ofType: .queryString, completion: completion)

    }
    
    func registerAPI(with payload: [UploadPayloadData], parameters: [String:Any], completion:@escaping EmptyAPIResponseCompletion){
        makeMultipartAPICall(to: AuthEndpoints.hospitalRegister, withBody: parameters, payload: payload, progressHandler: nil, completion: completion)
    }
}
