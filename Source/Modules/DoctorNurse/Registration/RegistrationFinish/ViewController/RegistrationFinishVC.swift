//
//  RegistrationFinishVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class RegistrationFinishVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var btnTnCSelection: UIButton!
    @IBOutlet weak var lblTermConditionLnik: UILabel!
    lazy var viewModel = RegistrationFinishViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        lblTermConditionLnik.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            guard let vc = TermConditionVC.loadFromXIB() else {
                return
            }
            welf.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedTnCCheckTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func tappedNextBtn(_ sender: AppSolidButton) {
        if !btnTnCSelection.isSelected {
            showSnackbar(withMessage: "You must agree to the Term and Conditions")
            return
        }
        let dependency = PasswordSetupDependency(prospect_id: viewModel.dependency?.prospect_id, userType: viewModel.dependency?.userType)
        guard let vc = PasswordSetupVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension RegistrationFinishVC {
    class func loadFromXIB(withDependency dependency: RegistrationFinishDependency? = nil) -> RegistrationFinishVC? {
        let storyboard = UIStoryboard(name: "RegistrationFinish", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "RegistrationFinishVC") as? RegistrationFinishVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - RegistrationFinishAPIResponseDelegate
extension RegistrationFinishVC: RegistrationFinishAPIResponseDelegate {
}
