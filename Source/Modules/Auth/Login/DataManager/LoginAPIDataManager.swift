//
//  LoginAPIDataManager.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias FindUserResponseCompletion = (Result<APIResponse<FindUserData>, Error>) -> Void

class LoginAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func findUser(parameters: [String:Any], completion:@escaping FindUserResponseCompletion){
        makeAPICall(to: AuthEndpoints.findUser, withParameters: parameters, ofType: .queryString, completion: completion)
    }
}
