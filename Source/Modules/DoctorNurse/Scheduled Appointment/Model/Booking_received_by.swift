/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Booking_received_by : Codable {
	let hospital_name : String?
	let username : String?
	let email : String?
	let phone_code : Int?
	let phone_number : String?
	let user_type : String?
	let first_name : String?
	let last_name : String?
	let profile_image : String?
	let address_line_1 : String?
	let address_line_2 : String?
	let latitude : String?
	let longitude : String?
	let district : String?
	let state : String?
	let country_code : String?
	let zip_code : Int?

	enum CodingKeys: String, CodingKey {

		case hospital_name = "hospital_name"
		case username = "username"
		case email = "email"
		case phone_code = "phone_code"
		case phone_number = "phone_number"
		case user_type = "user_type"
		case first_name = "first_name"
		case last_name = "last_name"
		case profile_image = "profile_image"
		case address_line_1 = "address_line_1"
		case address_line_2 = "address_line_2"
		case latitude = "latitude"
		case longitude = "longitude"
		case district = "district"
		case state = "state"
		case country_code = "country_code"
		case zip_code = "zip_code"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		hospital_name = try values.decodeIfPresent(String.self, forKey: .hospital_name)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		phone_code = try values.decodeIfPresent(Int.self, forKey: .phone_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		address_line_1 = try values.decodeIfPresent(String.self, forKey: .address_line_1)
		address_line_2 = try values.decodeIfPresent(String.self, forKey: .address_line_2)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		zip_code = try values.decodeIfPresent(Int.self, forKey: .zip_code)
	}

}