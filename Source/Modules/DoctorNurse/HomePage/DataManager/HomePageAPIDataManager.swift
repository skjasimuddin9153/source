//
//  HomePageAPIDataManager.swift
//  Source
//
//  Created by Techwens on 22/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias HospitalListResponseCompletion = (Result<APIResponse<HospitalListResponse>, Error>) -> Void
typealias HospitalFavouritesResponseCompletion = (Result<APIListResponse<FavouriteModel>, Error>) -> Void
class HomePageAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    
    func getHospitalList(completion:@escaping HospitalListResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.hospitalList, completion: completion)
    }
    
    func getFavouriteList(completion:@escaping HospitalFavouritesResponseCompletion){
        makeAPICallForListResponse(to: DoctorNurseEndpoints.favouriteList, completion: completion)
    }
}
