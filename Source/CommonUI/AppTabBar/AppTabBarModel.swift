//
//  AppTabBarModel.swift
//  DigitalWallet
//
//  Created by Techwens on 25/11/21.
//

import UIKit

struct AppTabItemData {

    let viewController: UIViewController?
    let image: UIImage
    let selectedImage: UIImage
    let title: String
}
