//
//  SearchHistoryTVC.swift
//  Source
//
//  Created by Techwens on 28/07/22.
//

import UIKit

class SearchHistoryTVC: UITableViewCell {

    @IBOutlet weak var lblSearchTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
