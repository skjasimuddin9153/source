//
//  VerticalView.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//

import UIKit

class VerticalView: UIView {
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        let topRect = CGRect(x: 0, y: 0, width: rect.size.width, height: rect.size.height/5)
        UIColor.accentColor?.set()
        guard let topContext = UIGraphicsGetCurrentContext() else { return }
        topContext.fill(topRect)

        let bottomRect = CGRect(x: 0, y: rect.size.height/5, width: rect.size.width, height: 4*(rect.size.height/5))
        UIColor.lightBlue?.set()
        guard let bottomContext = UIGraphicsGetCurrentContext() else { return }
        bottomContext.fill(bottomRect)
    }
}
