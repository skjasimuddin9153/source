//
//  OnboardingCVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//

import UIKit

class OnboardingCVC: UICollectionViewCell {
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var onboardingImage: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    
}
