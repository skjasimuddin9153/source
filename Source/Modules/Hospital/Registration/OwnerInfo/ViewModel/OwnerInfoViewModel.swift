//
//  OwnerInfoViewModel.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol OwnerInfoAPIResponseDelegate: class {
    func handleRegisterResponse(data: HospitalRegisterDataModel)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class OwnerInfoViewModel {
    weak var apiResponseDelegate: OwnerInfoAPIResponseDelegate?
    lazy var localDataManager = OwnerInfoLocalDataManager()
    lazy var apiDataManager = OwnerInfoAPIDataManager()
    
    var dependency: OwnerInfoDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func hospitalRegisterStepOne(parameter :  [String: String]) {
        apiDataManager.registerAPI( with: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.apiResponseDelegate?.handleRegisterResponse(data: data)
                    weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }
            }
            
        })
    }
}
