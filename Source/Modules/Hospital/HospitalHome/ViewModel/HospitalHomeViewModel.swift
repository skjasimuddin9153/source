//
//  HospitalHomeViewModel.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol HospitalHomeAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleListAPI()
}

class HospitalHomeViewModel {
    weak var apiResponseDelegate: HospitalHomeAPIResponseDelegate?
    lazy var localDataManager = HospitalHomeLocalDataManager()
    lazy var apiDataManager = HospitalHomeAPIDataManager()
    
    var dependency: HospitalHomeDependency?
    var bookingList: [Booking_list] = []
    
    init() {
    }
    // Data fetch service methods goes here
    func getBookingList(parameters: [String: Any]) {
        
        apiDataManager.getBookingData(parameter: parameters){ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.bookingList = data.booking_list ?? []
                    weakSelf.apiResponseDelegate?.handleListAPI()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
               

            }
        }
    }
}
