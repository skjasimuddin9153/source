//
//  OwnerInfoAPIDataManager.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias HospitalRegStepOneResponseCompletion = (Result<APIResponse<HospitalRegisterDataModel>, Error>) -> Void

class OwnerInfoAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping HospitalRegStepOneResponseCompletion){
        makeAPICall(to: AuthEndpoints.hospitalRegister, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
