//
//  HospitalScheduleVC.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

class HospitalScheduleVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constTableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgNurse: UIImageView!
    @IBOutlet weak var imgEmg: UIImageView!
    @IBOutlet weak var imgIcu: UIImageView!
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnNurse: UIButton!
    @IBOutlet weak var imgDoc: UIImageView!
    @IBOutlet weak var btnICU: UIButton!
    @IBOutlet weak var btnEmergency: UIButton!
    
    var userType: UserType?{
        didSet {
            switch userType {
            case .doctor:
                imgDoc.image = UIImage(named: "icRadioCheck")
                imgNurse.image = UIImage(named: "icRadio")
                viewModel.slotList = viewModel.fullSlotList
                currentSpecialityUpdate(type: specailty)
                viewModel.slotList = viewModel.slotList.filter({$0.type == "doctor"})
                tableView.reloadData()
            case .nurse:
                imgNurse.image = UIImage(named: "icRadioCheck")
                imgDoc.image = UIImage(named: "icRadio")
                viewModel.slotList = viewModel.fullSlotList
                currentSpecialityUpdate(type: specailty)
                viewModel.slotList = viewModel.slotList.filter({$0.type == "nurse"})
                tableView.reloadData()
            default :
                ()
                
            }
        }
    }
    var specailty: SlotType?{
        didSet {
            switch specailty {
            case .icu:
                imgIcu.image = UIImage(named: "icRadioCheck")
                imgEmg.image = UIImage(named: "icRadio")
                viewModel.slotList = viewModel.fullSlotList
                currentUserUpdate(type: userType)
                viewModel.slotList = viewModel.slotList.filter({$0.speciality == "ICU"})
                tableView.reloadData()
            case .emergency:
                imgEmg.image = UIImage(named: "icRadioCheck")
                imgIcu.image = UIImage(named: "icRadio")
                viewModel.slotList = viewModel.fullSlotList
                currentUserUpdate(type: userType)
                viewModel.slotList = viewModel.slotList.filter({$0.speciality == "EMG"})
                tableView.reloadData()
            default :
                ()
                
            }
        }
    }
    
    lazy var viewModel = HospitalScheduleViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.rowHeight = 45
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: "SlotDefaultTVC", bundle: nil), forCellReuseIdentifier: "SlotDefaultTVC")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getBankList()

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedDoctn(_ sender: UIButton) {
        userType = .doctor
    }
    @IBAction func tappedNursetn(_ sender: UIButton) {
        userType = .nurse
        
    }
    @IBAction func tappedICUTypeBtn(_ sender: UIButton) {
        specailty = .icu
    }
    @IBAction func tappedEmgTypeBtn(_ sender: UIButton) {
        specailty = .emergency
    }
    @IBAction func tappedAddNewSlotBtn(_ sender: UIButton) {
        guard let vc = AddNewSlotsVC.loadFromXIB() else { return }
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    func currentUserUpdate(type: UserType? ) {
        switch type {
        case .doctor:
            viewModel.slotList = viewModel.fullSlotList.filter({$0.type == "doctor"})
        case .nurse :
            viewModel.slotList = viewModel.fullSlotList.filter({$0.type == "nurse"})
        default:
            viewModel.slotList = viewModel.fullSlotList
        }
    }
    
    func currentSpecialityUpdate(type: SlotType? ) {
        switch type {
        case .emergency:
            viewModel.slotList = viewModel.fullSlotList.filter({$0.speciality == "EMG"})
        case .icu :
            viewModel.slotList = viewModel.fullSlotList.filter({$0.speciality == "ICU"})
        default:
            viewModel.slotList = viewModel.fullSlotList
        }
    }
}

// MARK: - Load from storyboard with dependency
extension HospitalScheduleVC {
    class func loadFromXIB(withDependency dependency: HospitalScheduleDependency? = nil) -> HospitalScheduleVC? {
        let storyboard = UIStoryboard(name: "HospitalSchedule", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "HospitalScheduleVC") as? HospitalScheduleVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - HospitalScheduleAPIResponseDelegate
extension HospitalScheduleVC: HospitalScheduleAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleListAPI() {
        userType = .doctor
        specailty = .icu
        tableView.reloadData()
    }
}

extension HospitalScheduleVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SlotDefaultTVC") as? SlotDefaultTVC else { return UITableViewCell() }
        let item = viewModel.slotList[indexPath.row]
        cell.lblSlotTime.text = "\(item.start_time ?? "") - \(item.end_time ?? "")"
        cell.btnEdit.isHidden = true
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
        constTableHeight.constant = CGFloat(viewModel.slotList.count) * 45
        return viewModel.slotList.count
        }
        
}

extension HospitalScheduleVC: EmptyDataSetSource,EmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let mainTitle = NSAttributedString.init(string: "No Slot Created",attributes: [NSAttributedString.Key.font : UIFont.sfProTextSemibold(ofSize: 15),NSAttributedString.Key.foregroundColor: UIColor.black])
        return mainTitle
    }
}

extension HospitalScheduleVC: ModifySlotDelegate {
    func deleteSlotAt(index: Int) {
        if let id = viewModel.slotList[index].id {
            viewModel.deleteSlotWithID(id: id)
        }
    }
    
    
}
