//
//  ProductDependencies.swift
//  Source
//
//  Created by Techwens Software on 04/09/23.
//

import Foundation
struct ProductDetailsResponse: Codable {
    let status: Bool
    let data: ProductData
}

struct ProductData: Codable {
    let id: Int
    let name: String
    let about: String
    let brandId: Int
    let thcPercentage: String
    let marketPrice: Double
    let actualPrice: Double
    let rating: Int
    let imageUrl: String
    let detailImageUrl: [String]
    let strainId: Int
    let categoryId: Int
    let createdAt: String
    let updatedAt: String
    let Strain: StrainData
    let Brand: BrandData
}

struct StrainData: Codable {
    let name: String
}

struct BrandData: Codable {
    let name: String
}
