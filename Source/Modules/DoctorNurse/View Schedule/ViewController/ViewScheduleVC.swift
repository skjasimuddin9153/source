//
//  ViewScheduleVC.swift
//  Source
//
//  Created by Techwens on 07/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import JTAppleCalendar

class ViewScheduleVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblSpecaility: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var hospitalDetailsView: UIView!
    @IBOutlet var calendarView: JTACMonthView!
    @IBOutlet weak var bookingTimeCollectionView: UICollectionView!
    var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }
    var data: CalendarData = CalendarData(startDate:"", endDate:  "", availableDates: [], selectedCalendarData: [])


    var selectedCalendarData: [String] = []
    lazy var viewModel = ViewScheduleViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        let availableDate: [String] = Date.dates(from: Date(), to: Date().addMonth(n: 2)).map({formatter.string(from: $0)})
        data.availableDates = availableDate
        data.startDate = formatter.string(from: Date())
        data.endDate  = formatter.string(from: Date().addMonth(n: 2))
        if let bookingData = viewModel.dependency?.bookingData {
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let bookingDate = formatter.date(from: bookingData.booking_start_time ?? "")
            formatter.dateFormat = "dd-MMM-yyyy"
            let formattedData = formatter.string(from: bookingDate!)
            data.selectedCalendarData = [formattedData]
            if let hospitalAddress = bookingData.booking_received_by {
                let fullAddress = (hospitalAddress.district ?? "") + " " + (hospitalAddress.state ?? "") + " " + "\(hospitalAddress.zip_code ?? 0)"
                let mainAddress = (hospitalAddress.address_line_1 ?? "") + " " + (hospitalAddress.address_line_2 ?? "")
                lblAddress.text =  mainAddress + " " + fullAddress
                lblHospitalName.text = hospitalAddress.hospital_name ?? ""
                lblSpecaility.text = bookingData.slot_speciality?.rawValue ?? ""
            }
            
        }
        hospitalDetailsView.applyLightAllAroundShadow()
        calendarView.register(UINib(nibName: "DataCell", bundle: nil), forCellWithReuseIdentifier: "DataCell")
        
        calendarView.register(UINib(nibName: "DataHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:  "DataHeader")
        calendarView.scrollDirection = .horizontal
        calendarView.scrollingMode   = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = false
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        bookingTimeCollectionView.dataSource = self
        bookingTimeCollectionView.delegate = self
        calendarView.reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedCancelBooking(_ sender: AppSolidButton) {
        if let bookingID = viewModel.dependency?.bookingData.booking_id {
            viewModel.cancelBookingData(bookingId: bookingID)
        }
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedHeartBtn(_ sender: UIButton) {
        guard let id = viewModel.dependency?.bookingData.booking_received_by_user_id else {
            return
        }
        if sender.isSelected {
            viewModel.removeFromFavourite(hospitalId: id, button: sender)
        } else {
            viewModel.markAsFavourite(hospitalId: id, button: sender)
        }
    }

}

// MARK: - Load from storyboard with dependency
extension ViewScheduleVC {
    class func loadFromXIB(withDependency dependency: ViewScheduleDependency? = nil) -> ViewScheduleVC? {
        let storyboard = UIStoryboard(name: "ViewSchedule", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ViewScheduleVC") as? ViewScheduleVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ViewScheduleAPIResponseDelegate
extension ViewScheduleVC: ViewScheduleAPIResponseDelegate {
    func handleMarkAsFavResponse(button: UIButton, message: String) {
        showSnackbar(withMessage: message)
        button.isSelected = true
        button.tintColor = .systemPink
    }
    
    func handleRemoveFavResponse(button: UIButton, message: String) {
        showSnackbar(withMessage: message)
        button.isSelected = false
        button.tintColor = .black
    }
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
        let dependency = ConfirmationCancellationDependency(status: .cancel)
        guard let vc = ConfirmationCancellationVC.loadFromXIB(withDependency: dependency) else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ViewScheduleVC {
    
    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
        cell.dateLabel.text = cellState.text
        handleCellTextColor(cell: cell, cellState: cellState)
       
    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
        
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.isHidden = false
        } else {
            cell.dateLabel.isHidden = true
        }
        let dateString = formatter.string(from: cellState.date)
        if data.availableDates.contains(dateString) {
            cell.dateLabel.textColor = .green
        } else {
            cell.dateLabel.textColor = .lightGray
        }
        if data.selectedCalendarData.contains(dateString) {
            cell.dotView.isHidden = false
        } else {
            cell.dotView.isHidden = true
        }
    }
    
}

extension ViewScheduleVC: JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let startDate = formatter.date(from: data.startDate)!
        let endDate = formatter.date(from: data.endDate)!
        return ConfigurationParameters(startDate: startDate, endDate: endDate)
    }
}

extension ViewScheduleVC: JTACMonthViewDelegate {
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
       // handleCellEvents(cellState: cellState)
    }
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "DataCell", for: indexPath) as! DateCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let formatter = DateFormatter()  // Declare this outside, to avoid instancing this heavy class multiple times.
        formatter.dateFormat = "MMM"

        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "DataHeader", for: indexPath) as! DateHeader
        header.monthTitle.text = formatter.string(from: range.start)
        header.completionNext = {
            calendar.scrollToSegment(.next)
        }
        header.completionPrevious = {
            calendar.scrollToSegment(.previous)
        }
        return header
    }

    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return MonthSize(defaultSize: 110)
    }
}
extension ViewScheduleVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bookingTimeCollectionView {
            return 1
        } else {
            return 0
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = bookingTimeCollectionView.dequeueReusableCell(withReuseIdentifier: "BookingSlotCVC", for: indexPath) as? BookingSlotCVC
        else {
            return UICollectionViewCell()
        }
        if let bookingData = viewModel.dependency?.bookingData {
            cell.lblTimeSlot.text = (bookingData.slot_start_time ?? "") + " to " + (bookingData.slot_end_time ?? "")
        }
        cell.bgView.borderColor =  .systemGreen
        cell.lblTimeSlot.textColor =  .systemGreen
        return cell
    }
    
    
}

extension ViewScheduleVC: UICollectionViewDelegateFlowLayout {
    
}

