//
//  ForgotPinAPIDataManager.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import Alamofire

class ForgotPinAPIDataManager: APIDataManager{
    init() {
    }
    // Data fetch service methods goes here
    func forgotPasswordAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.forgotPin, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
