//
//  HospitalBasicOneVC.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum HospitalDropdowns {
    case country
}

class HospitalBasicOneVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfHospitalName: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var btnNext: AppSolidButton!
    lazy var viewModel = HospitalBasicOneViewModel()
    var acticeDropDown: HospitalDropdowns?
    var parameter : [String: String] = [:]
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        viewModel.getAllCountries()
        tfHospitalName.setupSourceTextField(withPlaceholder: "Hospital Name *")
        tfCountry.setupSourceTextField(withPlaceholder: "Select Country *")
        tfCountry.delegate = self
        tfCountry.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfCountry.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedCountryNameDropDown()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    func tappedCountryNameDropDown() {
        acticeDropDown = .country
        let dependency = PickerViewDependency(title: "Select Country", dataList: viewModel.countryList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedNextBtn(_ sender: AppSolidButton) {
        
        if !allFieldsValid() {
           return
        }
        parameter["hospital_name"] = tfHospitalName.text!
        // TODO: - CHANGE THIS TO COUNTRY PARAMTER WHEN READY?
        parameter["hospital_district"] = tfCountry.text!
        print(parameter)
        let dependency = OwnerInfoDependency(parameter: parameter)
        guard let vc = OwnerInfoVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    func allFieldsValid() -> Bool {
        
        if (tfHospitalName.text?.isEmpty ?? true) || tfHospitalName.text == ""{
            showSnackbar(withMessage: "Hospital Name is Invalid")
            tfHospitalName.shake()
            return false
        }
        
        if (tfCountry.text?.isEmpty ?? true) || tfCountry.text == ""{
            showSnackbar(withMessage: "Country is Invalid")
            tfCountry.shake()
            return false
        }
        return true
    }
    
}

// MARK: - Load from storyboard with dependency
extension HospitalBasicOneVC {
    class func loadFromXIB(withDependency dependency: HospitalBasicOneDependency? = nil) -> HospitalBasicOneVC? {
        let storyboard = UIStoryboard(name: "HospitalBasicOne", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "HospitalBasicOneVC") as? HospitalBasicOneVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - HospitalBasicOneAPIResponseDelegate
extension HospitalBasicOneVC: HospitalBasicOneAPIResponseDelegate {
    func handleCountryListResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
    }
    
    func handleAPIError(_ error: Error) {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
}

extension HospitalBasicOneVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case tfCountry :
            tappedCountryNameDropDown()
            return false
        default:
            return true
        }
    }
}

extension HospitalBasicOneVC: SelectInfoFromPickerDelegate {
    func didInfoSelected(value: String, index: Int) {
        switch acticeDropDown {
        case .country:
            tfCountry.text = value
        default:
            acticeDropDown = nil
        }
        acticeDropDown = nil
    }
    
    func noSelection() {
        acticeDropDown = nil
    }
}
