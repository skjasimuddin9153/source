//
//  HomeTVHeader.swift
//  Source
//
//  Created by Techwens on 23/07/22.
//

import UIKit

class HomeTVHeader: UIView {


    @IBOutlet weak var lblHeader: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> HomeTVHeader {
        return UINib(nibName: "HomeTVHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HomeTVHeader
    }

}
