//
//  BankDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias BankAPIResponseCompletion = (Result<APIListResponse<BankDataModel>, Error>) -> Void

class BankDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.register, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
    func getBankAPI( completion:@escaping BankAPIResponseCompletion){
        makeAPICallForListResponse(to: UtiltiyEndpoints.bankList, completion: completion)

    }
}
