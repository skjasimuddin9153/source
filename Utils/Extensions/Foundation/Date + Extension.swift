//
//  Date + Extension.swift
//  Source
//
//  Created by Techwens on 14/09/22.
//

import Foundation

extension Date {
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
    
      func addMonth(n: Int) -> Date {
      let calendar = Calendar.current
      return calendar.date(byAdding: .month, value: n, to: self)!
  }

}
