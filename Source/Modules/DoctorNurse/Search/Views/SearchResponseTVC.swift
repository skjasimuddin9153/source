//
//  SearchResponseTVC.swift
//  Source
//
//  Created by Techwens on 28/07/22.
//

import UIKit

class SearchResponseTVC: UITableViewCell {
    @IBOutlet weak var imaHospital: UIImageView!
    @IBOutlet weak var lblHospital: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var hospitalData: HospitalsModel? {
        didSet {
            guard let hospitalData = hospitalData else {
                return
            }
            lblHospital.text = hospitalData.hospital_name
            let imgURL = URL(string: hospitalData.hospital_image ?? "")
            imaHospital.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "icHospital"))
            let fullAddress = (hospitalData.district ?? "") + " " + (hospitalData.state ?? "") + " " + "\(hospitalData.zip_code ?? 0)"
            lblAddress.text = (hospitalData.address_line_1 ?? "") + " " + (hospitalData.address_line_2 ?? "") + " " + fullAddress
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
