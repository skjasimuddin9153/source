//
//  AppSnackbar.swift
//  DigitalWallet
//
//  Created by Techwens on 30/11/21.
//


import UIKit
import TTGSnackbar

enum SnackbarMessageType {
    
    case normal
    case alert
    case error
}

extension UIViewController {
    
    func showSnackbar(withMessage message: String,
                      ofType messageType: SnackbarMessageType = .normal,
                      actionTitle: String? = nil,
                      action: (() -> Void)? = nil
                      ) {
        
        var snackbar: TTGSnackbar?
        
        if let actionTitle = actionTitle {
            
            snackbar = TTGSnackbar(message: message,
                                       duration: .long,
                                       actionText: actionTitle) { (snackbar) in
                action?()
                snackbar.dismiss()
            }
        } else {
            snackbar = TTGSnackbar(message: message, duration: .middle)
        }
        
        //snackbar?.messageTextFont = .systemFont(ofSize: 15.0)
        snackbar?.messageTextFont = .poppinsRegular(ofSize: 15)
        
        snackbar?.onTapBlock = { snackbar in
            snackbar.dismiss()
        }
        
        snackbar?.dismissBlock = { snackbar in
            action?()
        }
        
        snackbar?.onSwipeBlock = { (snackbar, direction) in
            
            if direction == .right {
                snackbar.animationType = .slideFromLeftToRight
            } else if direction == .left {
                snackbar.animationType = .slideFromRightToLeft
            } else if direction == .up {
                snackbar.animationType = .slideFromTopBackToTop
            } else if direction == .down {
                snackbar.animationType = .slideFromTopBackToTop
            }
            snackbar.dismiss()
        }
        
        switch messageType {
        case .normal:
            snackbar?.messageTextColor = UIColor.white
        case .alert:
            snackbar?.messageTextColor = UIColor.white
        case .error:
            snackbar?.messageTextColor = UIColor.red
        }
        snackbar?.show()
    }
    
    func showSnackbarForever(withMessage message: String,
                      ofType messageType: SnackbarMessageType = .normal,
                      actionTitle: String? = nil,
                      action: (() -> Void)? = nil
        ) {
        
        var snackbar: TTGSnackbar?
        
        if let actionTitle = actionTitle {
            
            snackbar = TTGSnackbar(message: message,
                                   duration: .forever,
                                   actionText: actionTitle) { (snackbar) in
                                    action?()
                                    snackbar.dismiss()
            }
        } else {
            snackbar = TTGSnackbar(message: message, duration: .middle)
        }
        
        //snackbar?.messageTextFont = .systemFont(ofSize: 15.0)
        snackbar?.messageTextFont = .poppinsRegular(ofSize: 15)
        
        snackbar?.onTapBlock = { snackbar in
            snackbar.dismiss()
        }
        
        snackbar?.dismissBlock = { snackbar in
            action?()
        }
        
        snackbar?.onSwipeBlock = { (snackbar, direction) in
            
            if direction == .right {
                snackbar.animationType = .slideFromLeftToRight
            } else if direction == .left {
                snackbar.animationType = .slideFromRightToLeft
            } else if direction == .up {
                snackbar.animationType = .slideFromTopBackToTop
            } else if direction == .down {
                snackbar.animationType = .slideFromTopBackToTop
            }
            snackbar.dismiss()
        }
        
        switch messageType {
        case .normal:
            snackbar?.messageTextColor = UIColor.white
        case .alert:
            snackbar?.messageTextColor = UIColor.white
        case .error:
            snackbar?.messageTextColor = UIColor.red
        }
        snackbar?.show()
    }
}

class SnackBarMessage {
    
    static var snackbar = TTGSnackbar()
    static func showSnackBarWithMessage(message:String) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            DispatchQueue.main.async {
                let snackBar = TTGSnackbar.init(message: message, duration: .middle)
                snackBar.show()
            }
        }
        
    }
    
    static func showSnackBarWithMessageForEver(message:String) {
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            DispatchQueue.main.async {
                let snackBar = TTGSnackbar.init(message: message, duration: .forever)
                snackbar.messageTextFont = .poppinsRegular(ofSize: 15)
                snackBar.show()
                snackbar = snackBar
            }
        }
        
    }
    
    static func stopSnackBar() {
        
        DispatchQueue.global(qos: .userInitiated).async {
            snackbar.dismiss()
        }
    }
}
