
import Foundation
class cartdataManager {
    static let shared = cartdataManager()

    var data: [CartProduct] = [] {
        didSet {
            // Notify observers that the data has changed
            dataDidChangeCallback?()
        }
    }

    var dataDidChangeCallback: (() -> Void)?
}
