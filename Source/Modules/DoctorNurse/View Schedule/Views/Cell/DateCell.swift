import JTAppleCalendar
import UIKit

class DateCell: JTACDayCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var dotView: UIView!
    
    override func awakeFromNib() {
        dotView.layer.borderWidth = 0.5
        dotView.layer.cornerRadius = dotView.frame.height / 2
        dotView.layer.borderColor = UIColor.lightGray.cgColor

    }
    
}
