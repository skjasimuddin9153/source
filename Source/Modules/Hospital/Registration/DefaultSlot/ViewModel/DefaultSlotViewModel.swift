//
//  DefaultSlotViewModel.swift
//  Source
//
//  Created by Techwens on 02/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol DefaultSlotAPIResponseDelegate: AnyObject {
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class DefaultSlotViewModel {
    weak var apiResponseDelegate: DefaultSlotAPIResponseDelegate?
    lazy var localDataManager = DefaultSlotLocalDataManager()
    lazy var apiDataManager = DefaultSlotAPIDataManager()
    var icuSlots: [SlotDataModel] = []
    var emergencySlots: [SlotDataModel] = []
    var dependency: DefaultSlotDependency?
    
    init() {
    }
    func getDataDictonaries() -> SlotDocRequestData  {
        var slotList : [SlotEncoData] = []
        
        for (_, slot) in icuSlots.enumerated() {
            let docSlot = SlotEncoData(speciality: SlotType.icu.rawValue, slot_time_start: slot.startTime.slotFormatTime(), slot_time_end: slot.endTime.slotFormatTime(), slot_price: slot.price)
            slotList.append(docSlot)
        }
        for (_, slot) in emergencySlots.enumerated() {
            let docSlot = SlotEncoData(speciality: SlotType.emergency.rawValue, slot_time_start: slot.startTime.slotFormatTime(), slot_time_end: slot.endTime.slotFormatTime(), slot_price: slot.price)
            slotList.append(docSlot)
        }
        let requestData = SlotDocRequestData(prospect_id: "\(dependency!.prospect_id!)", step: "6", doctor_slots: slotList)
        return requestData
                
    }
    // Data fetch service methods goes here
    
    func register() {
       let parameters = getDataDictonaries().dictionary
        print(parameters)
        apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
   
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
