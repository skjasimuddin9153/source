//
//  PostgraduateDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol PostgraduateDetailsAPIResponseDelegate: class {
}

class PostgraduateDetailsViewModel {
    weak var apiResponseDelegate: PostgraduateDetailsAPIResponseDelegate?
    lazy var localDataManager = PostgraduateDetailsLocalDataManager()
    lazy var apiDataManager = PostgraduateDetailsAPIDataManager()
    
    var dependency: PostgraduateDetailsDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
