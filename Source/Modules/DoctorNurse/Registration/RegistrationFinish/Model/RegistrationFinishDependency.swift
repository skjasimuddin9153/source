//
//  RegistrationFinishDependency.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct RegistrationFinishDependency {
    let prospect_id : Int?
    var userType: UserType? = nil
}
