import UIKit
import NVActivityIndicatorView

protocol SpinnerButton where Self: UIButton {

    var activityIndicator: NVActivityIndicatorView? {get set}
    var spinnerColor: UIColor {get set}
    var cover: UIView? {get set}
}

extension SpinnerButton {





     func createActivityIndicator() -> NVActivityIndicatorView {

         let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: bounds.midX, y: bounds.midY, width: 30, height: 30), type: .ballRotateChase, color: spinnerColor, padding: 0)
        activityIndicator.isHidden = false
        return activityIndicator
    }

     func showSpinning() {

        positionActivityIndicatorInButtonCenter()
        isUserInteractionEnabled = false
        activityIndicator?.startAnimating()
    }

    private func positionActivityIndicatorInButtonCenter() {

        guard let activityIndicator = activityIndicator  else {
            return
        }

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false

        cover = UIView(frame: bounds)
        if backgroundColor == UIColor.clear {
            cover?.backgroundColor = superview?.backgroundColor
        } else {
            cover?.backgroundColor = backgroundColor
        }

        addSubview(cover!)
        addSubview(activityIndicator)
        cover?.addSubview(activityIndicator)
        cover?.layer.cornerRadius = layer.cornerRadius
        cover?.clipsToBounds = true

        let xCenterConstraint = NSLayoutConstraint(item: cover!,
                                                   attribute: .centerX,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerX,
                                                   multiplier: 1,
                                                   constant: 0)
        cover?.addConstraint(xCenterConstraint)

        let yCenterConstraint = NSLayoutConstraint(item: cover!,
                                                   attribute: .centerY,
                                                   relatedBy: .equal,
                                                   toItem: activityIndicator,
                                                   attribute: .centerY,
                                                   multiplier: 1,
                                                   constant: 0)
        cover?.addConstraint(yCenterConstraint)
    }
}
