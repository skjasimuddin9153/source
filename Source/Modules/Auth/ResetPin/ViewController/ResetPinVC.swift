//
//  ResetPinVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class ResetPinVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfPin: UITextField!
    @IBOutlet weak var tfConfirmPin: UITextField!
    lazy var viewModel = ResetPinViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfPin.setupSourceTextField(withPlaceholder: "Enter New Pin")
        tfPin.setLeftViewButton(with: UIImage(named: "icKey")!)
        tfConfirmPin.setupSourceTextField(withPlaceholder: "Confirm Pin")
        tfConfirmPin.setLeftViewButton(with: UIImage(named: "icKey")!)
        tfPin.delegate = self
        tfConfirmPin.delegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedSubmitBtn(_ sender: AppSolidButton) {
        guard let emailPhone = viewModel.dependency?.email else {
            showSnackbar(withMessage: "Invalid Email/Phone")
            return
        }
        if !allFieldsValid() {
           return
        }
        switch getInputType() {
        case .email:
            sender.showSpinner()
            view.isUserInteractionEnabled = false
            viewModel.resetPin(type: .email, withEmail: emailPhone, pin: tfPin.text! ,c_pin: tfConfirmPin.text!)
        case .phone:
            sender.showSpinner()
            view.isUserInteractionEnabled = false
            viewModel.resetPin(type: .phone, withEmail: emailPhone, pin: tfPin.text! ,c_pin: tfConfirmPin.text!)
        default:
           showSnackbar(withMessage: "Invalid Input Type")
        }
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func allFieldsValid() -> Bool {

        if (tfPin.text?.isEmpty ?? true) || tfPin.text == "" || tfPin.text?.count != 4 {
            showSnackbar(withMessage: "PIN Invalid")
            tfPin.shake()
            return false
        }
        if  (tfConfirmPin.text?.isEmpty ?? true) || tfConfirmPin.text == "" || tfConfirmPin.text?.count != 4 {
            showSnackbar(withMessage: "Confirm PIN is Invalid")
            tfConfirmPin.shake()
            return false
        }
        
        if tfPin.text != tfConfirmPin.text {
            showSnackbar(withMessage: "Both Pin must be same")
            tfPin.shake()
            tfConfirmPin.shake()
            return false
        }
       
        return true
    }
    
    func getInputType() -> FindType? {
        if viewModel.dependency!.email.validateWith(regex: .email) {
            return .email
        }
        if viewModel.dependency!.email.validateWith(regex: .phoneNo) {
            return .phone
        }
        return nil
    }
}

// MARK: - Load from storyboard with dependency
extension ResetPinVC {
    class func loadFromXIB(withDependency dependency: ResetPinDependency? = nil) -> ResetPinVC? {
        let storyboard = UIStoryboard(name: "ResetPin", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ResetPinVC") as? ResetPinVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ResetPinAPIResponseDelegate
extension ResetPinVC: ResetPinAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
        guard let vc = CongratulationPageVC.loadFromXIB(withDependency: nil) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension ResetPinVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)

        return newString.count <= maxLength
    }
}
