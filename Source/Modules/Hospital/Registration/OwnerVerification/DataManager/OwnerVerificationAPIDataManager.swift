//
//  OwnerVerificationAPIDataManager.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class OwnerVerificationAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with payload: [UploadPayloadData], parameters: [String:Any], completion:@escaping EmptyAPIResponseCompletion){
        makeMultipartAPICall(to: AuthEndpoints.hospitalRegister, withBody: parameters, payload: payload, progressHandler: nil, completion: completion)
    }
}
