//
//  RegistrationDoneVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class RegistrationDoneVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = RegistrationDoneViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension RegistrationDoneVC {
    class func loadFromXIB(withDependency dependency: RegistrationDoneDependency? = nil) -> RegistrationDoneVC? {
        let storyboard = UIStoryboard(name: "RegistrationDone", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "RegistrationDoneVC") as? RegistrationDoneVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - RegistrationDoneAPIResponseDelegate
extension RegistrationDoneVC: RegistrationDoneAPIResponseDelegate {
}
