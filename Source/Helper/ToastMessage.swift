//
//  ToastMessage.swift
//  Source
//
//  Created by Techwens Software on 03/10/23.
//

import Foundation
import TTGSnackbar


func showtoast (message:String,color:UIColor){
    let snackbar = TTGSnackbar(message: message, duration: .middle)
    snackbar.backgroundColor = color
    snackbar.show()
}
