//
//  ViewScheduleAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class ViewScheduleAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func cancelBookingData(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.bookingCancel, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func addToFavourite(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.markFavourite, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
    
    func removeFromFavourite(parameter: [String: AnyObject] ,completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.removeFavourite, withParameters: parameter, ofType: .httpBody, completion: completion)
    }
}
