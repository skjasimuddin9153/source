//
//  ProfileViewController.swift
//  Source
//
//  Created by Techwens Software on 05/09/23.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet var headerView: HeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        headerView.navigationController = self.navigationController
        headerView.title.text = "Profile"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileViewController {
    class func loadFromXIB() -> ProfileViewController? {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController else {
            return nil
        }
        
        return viewController
    }
}
