//
//  VerificationDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol VerificationDetailsAPIResponseDelegate: AnyObject {
    func handleRegisterResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class VerificationDetailsViewModel {
    weak var apiResponseDelegate: VerificationDetailsAPIResponseDelegate?
    lazy var localDataManager = VerificationDetailsLocalDataManager()
    lazy var apiDataManager = VerificationDetailsAPIDataManager()
    
    var dependency: VerificationDetailsDependency?
    let documents: [DocTypes] = [.adhar,.driving_license,.electoral_id,.passport]
    
    init() {
    }
    // Data fetch service methods goes here
    func registerStepTwo(with imageData: Data, prospect_id: Int, panNumber: String, docType: DocTypes, docNumber: String) {
        let payloadData = UploadPayloadData(data: imageData, name: "uploaded_document_file", filename: "image.jpeg", mimeType: .jpeg)
        let parameters = [
            "prospect_id":"\(prospect_id)",
            "step":"2",
            "pan_number": panNumber,
            "uploaded_document_type": docType.rawValue,
            "uploaded_document_number": docNumber
        ]
        apiDataManager.registerAPI(with: payloadData, parameters: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRegisterResponse()
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
