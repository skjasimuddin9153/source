//
//  CodeVerificationDependency.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct CodeVerificationDependency {
    let user: UserType
    let flow: FlowType
    let prospect_id : Int?
    let otp_valid_for : Int?
    var email: String? = nil
}

enum FlowType {
    case login
    case registration
}
