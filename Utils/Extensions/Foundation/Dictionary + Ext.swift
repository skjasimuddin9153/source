//
//  Dictionary + Ext.swift
//  Source
//
//  Created by Techwens on 27/06/22.
//

import Foundation

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
