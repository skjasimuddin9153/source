//
//  HospitalTabViewModel.swift
//  Source
//
//  Created by Techwens on 20/09/22.
//

import UIKit

struct HospitalTabViewModel {

    let tabItemData: [AppTabItemData] = [
        AppTabItemData(viewController: HospitalHomeVC.loadFromXIB(),
                       image: UIImage(named: "homeDefault")!,
                       selectedImage: UIImage(named: "homeSelected")!,
                      title: "Home"),
        AppTabItemData(viewController: HospitalScheduleVC.loadFromXIB(),
                       image: UIImage(named: "scheduleDefault")!,
                       selectedImage: UIImage(named: "scheduleSelected")!,
                      title: "Sechedule"),
        AppTabItemData(viewController: DemoVC.loadFromXIB(),
                       image: UIImage(named: "icStaffDefault")!,
                       selectedImage: UIImage(named: "icStaffSelected")!,
                      title: "Staffing"),
        AppTabItemData(viewController: DemoVC.loadFromXIB(),
                       image: UIImage(named: "earningDefault")!,
                       selectedImage: UIImage(named: "earningSelected")!,
                      title: "Payment"),
        AppTabItemData(viewController: LogoutDemoVC.loadFromXIB(),
                       image: UIImage(named: "menuDefault")!,
                       selectedImage: UIImage(named: "menuSelected")!,
                      title: "Menu")
    ]

    init() {

    }
}
