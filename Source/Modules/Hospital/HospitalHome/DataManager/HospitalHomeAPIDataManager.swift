//
//  HospitalHomeAPIDataManager.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class HospitalHomeAPIDataManager: APIDataManager{
    init() {
    }
    // Data fetch service methods goes here
    func getBookingData(parameter: [String: Any],completion:@escaping ScheduledAppointmentResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.userBookings,withParameters: parameter,ofType: .queryString, completion: completion)
    }
}
