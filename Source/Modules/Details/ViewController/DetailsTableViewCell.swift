//
//  DetailsTableViewCell.swift
//  Source
//
//  Created by Techwens Software on 30/08/23.
//

import UIKit
import AlamofireImage

class DetailsTableViewCell: UITableViewCell {
    @IBOutlet var productBg: UIView!
    @IBOutlet var productTitle: UILabel!
    @IBOutlet var productSubTitle: UILabel!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var skelitonBox: UIView!
    var product:Product?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                let layer0 = CAGradientLayer()
                layer0.colors = [
                UIColor(red: 0.063, green: 0.063, blue: 0.063, alpha: 0).cgColor,
                UIColor(red: 0.063, green: 0.063, blue: 0.063, alpha: 1).cgColor
                ]
                layer0.locations = [0, 1]
                layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
                layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
                layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -0.33, d: 0, tx: 0.66, ty: 0))
                layer0.bounds = productBg.bounds.insetBy(dx: -0.5*productBg.bounds.size.width, dy: productBg.bounds.size.height)
                layer0.position = productBg.center
        
  
                productBg.layer.addSublayer(layer0)
                productBg.layer.cornerRadius = 8
                skelitonBox.startAnimation()
       
        print("product\(product)")
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
