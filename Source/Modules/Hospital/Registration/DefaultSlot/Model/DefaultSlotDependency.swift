//
//  DefaultSlotDependency.swift
//  Source
//
//  Created by Techwens on 02/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

struct DefaultSlotDependency {
    let prospect_id : Int?
}

struct SlotDataModel {
    var startTime: String
    var endTime: String
    var price: String
    var isDayChanger: Bool
}

struct SlotEncoData: Codable {
    var speciality: String
    var slot_time_start: String
    var slot_time_end: String
    var slot_price: String

}

struct SlotDocRequestData: Codable {
    var prospect_id: String
    var step: String
    var doctor_slots: [SlotEncoData]
}

