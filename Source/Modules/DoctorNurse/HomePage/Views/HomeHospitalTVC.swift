//
//  HomeHospitalTVC.swift
//  Source
//
//  Created by Techwens on 23/07/22.
//

import UIKit
import SDWebImage

class HomeHospitalTVC: UITableViewCell {
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var imgHospital: UIImageView!
    @IBOutlet weak var lblSpeciality: UILabel!
    
    var hospitalData: HospitalsModel? {
        didSet {
            guard let hospitalData = hospitalData else {
                return
            }
            lblHospitalName.text = hospitalData.hospital_name
            let imgURL = URL(string: hospitalData.hospital_image ?? "")
            imgHospital.sd_setImage(with: imgURL, placeholderImage:  UIImage(named: "icHospital"))
            lblSpeciality.text = (hospitalData.speciality ?? .all).rawValue
            let fullAddress = (hospitalData.district ?? "") + " " + (hospitalData.state ?? "") + " " + "\(hospitalData.zip_code ?? 0)"
            lblAddress.text = (hospitalData.address_line_1 ?? "") + " " + (hospitalData.address_line_2 ?? "") + " " + fullAddress
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
