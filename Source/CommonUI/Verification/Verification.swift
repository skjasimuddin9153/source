//
//  Verification.swift
//  Source
//
//  Created by Techwens on 05/09/23.
//

import UIKit

class Verification: UIView {
    // Closure properties to handle button clicks
    var onCloseButtonTapped: (() -> Void)?
    var onAllowButtonTapped: (() -> Void)?
    
    @IBOutlet var contentView: UIView!

   
    @IBOutlet var allowBtn: UIButton!
    override init(frame:CGRect){
        super.init(frame: frame)
        commitInt()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInt()
    }
    
    private func commitInt(){
        Bundle.main.loadNibNamed("Verification", owner: self,options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        contentView.topCornersRadius = 40
       
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 1, height: 0)
        contentView.layer.shadowColor = UIColor.gray.cgColor
//        allowBtn.layer.cornerRadius = 10
        
//        onCick handler
       
              allowBtn.addTarget(self, action: #selector(allowButtonTapped), for: .touchUpInside)
    }
    @objc private func closeButtonTapped() {
           onCloseButtonTapped?()
       }
       
       @objc private func allowButtonTapped() {
           onAllowButtonTapped?()
       }

}
