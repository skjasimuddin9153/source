//
//  DefaultSlotNurseDependency.swift
//  Source
//
//  Created by Techwens on 03/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct DefaultSlotNurseDependency {
    let prospect_id : Int?
}

struct SlotNurseRequestData: Codable {
    var prospect_id: String
    var step: String
    var nurse_slots: [SlotEncoData]
}
