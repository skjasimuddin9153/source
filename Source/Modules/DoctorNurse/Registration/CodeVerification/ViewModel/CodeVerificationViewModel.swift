//
//  CodeVerificationViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol CodeVerificationAPIResponseDelegate: class {
    func handleSendOTPForgotPasswordResponse(message:String)
    func handleSendOTPResponse(message:String)
    func handleReSendOTPResponse(message:String)
    func handleAPIError(_ error: Error)
}

class CodeVerificationViewModel {
    weak var apiResponseDelegate: CodeVerificationAPIResponseDelegate?
    lazy var localDataManager = CodeVerificationLocalDataManager()
    lazy var apiDataManager = CodeVerificationAPIDataManager()
    
    var dependency: CodeVerificationDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func submit( otp: String, id: Int) {
        let parameters = [
            "otp" : otp
        ]
        apiDataManager.sendOTPAPI(with: parameters,prospect_id: id, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleSendOTPResponse(message: response.message)
            }
            
        })
    }
    
    func resendForgotPassword(type: FindType,withEmail email: String) {
        var parameter : [String: String] = [:]
        switch type {
        case .email:
            parameter["email"] = email
        case .phone:
            parameter["phone"] = email
        }
        apiDataManager.forgotPasswordAPI(with: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleReSendOTPResponse(message: response.message)
            }
            
        })
    }
    
    func submitForgotPassword( otp: String, emailId: String) {
        let parameters = [
            "otp" : otp,
            "email": emailId
        ]
        apiDataManager.sendOTPAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleSendOTPForgotPasswordResponse(message: response.message)
            }
            
        })
    }
    
    func resubmit(id: Int) {
        apiDataManager.resendOTPAPI(prospect_id: id, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleReSendOTPResponse(message: response.message)
            }
            
        })
    }
}
