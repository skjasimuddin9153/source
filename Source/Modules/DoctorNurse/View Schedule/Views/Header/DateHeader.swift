import UIKit
import JTAppleCalendar

class DateHeader: JTACMonthReusableView  {
    @IBOutlet var monthTitle: UILabel!
    var completionPrevious: (() -> Void ) = {}
    var completionNext: (() -> Void ) = {}
    
    @IBAction func tappedPreviousMonth(_ sender: UIButton) {
        completionPrevious()
    }
    
    @IBAction func tappedNextMonth(_ sender: UIButton) {
        completionNext()
    }
}
