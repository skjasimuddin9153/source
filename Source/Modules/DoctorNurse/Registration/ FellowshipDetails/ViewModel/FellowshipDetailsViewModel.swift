//
//  FellowshipDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol FellowshipDetailsAPIResponseDelegate: class {
    func handleRegisterResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class FellowshipDetailsViewModel {
    weak var apiResponseDelegate: FellowshipDetailsAPIResponseDelegate?
    lazy var localDataManager = FellowshipDetailsLocalDataManager()
    lazy var apiDataManager = FellowshipDetailsAPIDataManager()
    
    var dependency: FellowshipDetailsDependency?
    
    init() {
    }
    // Data fetch service methods goes here

    func registerStepThree(with payloadData: [UploadPayloadData], parameter : inout [String: String], prospect_id: Int) {
        parameter["prospect_id"] = "\(prospect_id)"
        parameter["step"] = "3"
        apiDataManager.registerAPI(with: payloadData, parameters: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRegisterResponse()
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
