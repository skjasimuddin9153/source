//

//
//  LoginPinViewModel.swift
//  Source
//
//  Created by jasim0021 on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import Alamofire



final class ProductDetailsViewModel {
  
    

    enum Event {
        case loading
        case stopLoading
        case dataLoaded
        case error(Error)
        case data(ProductDetailsResponse)
    }

    var eventHandler: ((Event) -> Void)?

    // Your existing login and other functions here...

    func getProductDetails(productId id:String) {
        self.eventHandler?(.loading) // Notify the loading state
        
    
        
        APIManager.shared.request(
            method: .get,
            endpoint:AuthEndpoints.productDetails(product_id: id)
          
          
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: ProductDetailsResponse = try JSONDecoder().decode(ProductDetailsResponse.self, from: data)
                    print("Success: \(json)")
                    self.eventHandler?(.data(json)) // Notify success and pass data
                  
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
}
