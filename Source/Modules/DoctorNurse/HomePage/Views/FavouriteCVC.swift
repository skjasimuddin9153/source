//
//  FavouriteCVC.swift
//  Source
//
//  Created by Techwens on 23/07/22.
//

import UIKit

class FavouriteCVC: UICollectionViewCell {
    @IBOutlet weak var hospitalName: UILabel!
    @IBOutlet weak var hospitalImage: UIImageView!
    
    var data: FavouriteModel? {
        didSet {
            hospitalName.text = data?.hospital_name ?? ""
        }
    }
    
}
