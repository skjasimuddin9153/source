//
//  GraduationDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias CountryResponseCompletion = (Result<APIResponse<CountriesDataResponse>, Error>) -> Void
typealias StateResponseCompletion = (Result<APIResponse<StateDataResponse>, Error>) -> Void

class GraduationDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func countriesAPI(completion:@escaping CountryResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.countryList,  ofType: .queryString, completion: completion)

    }
    
    func statesAPI(id: String ,completion:@escaping StateResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.stateList(countryId: id),  ofType: .queryString, completion: completion)

    }
}
