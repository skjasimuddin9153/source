import UIKit

extension UIWindow {

    func replaceRootViewControllerWith(_ replacementController: UIViewController,
                                       animated: Bool,
                                       completion: (() -> Void)?) {
        let snapshotImageView = UIImageView(image: self.snapshot())
        self.addSubview(snapshotImageView)

        let dismissCompletion = { [weak self] in // dismiss all modal view controllers
            self?.rootViewController = replacementController
            self?.bringSubviewToFront(snapshotImageView)
            if animated {
                UIView.animate(withDuration: 0.4, animations: { () -> Void in
                    snapshotImageView.alpha = 0
                }, completion: { (_) -> Void in
                    snapshotImageView.removeFromSuperview()
                    completion?()
                })
            } else {
                snapshotImageView.removeFromSuperview()
                completion?()
            }
        }

        DispatchQueue.main.async { [weak self] in
            if self?.rootViewController?.presentedViewController != nil {
                self?.rootViewController?.dismiss(animated: false, completion: dismissCompletion)
            } else {
                dismissCompletion()
            }
        }
    }
}

extension UIWindow {
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
