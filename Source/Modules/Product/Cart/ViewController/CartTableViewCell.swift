//
//  CartTableViewCell.swift
//  Source
//
//  Created by Techwens Software on 01/09/23.
//

import UIKit

class CartTableViewCell: UITableViewCell {
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productTitle: UILabel!
    @IBOutlet var productSubTitle: UILabel!
    @IBOutlet var productPrice: UILabel!
    @IBOutlet var itemCount: UILabel!
    var removeMorecart:(() -> Void)?
    var addMorecart:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onDeleteCartClicked(_ sender: Any) {
        removeMorecart?()
    }
    @IBAction func onAddcartClicked(_ sender: Any) {
        addMorecart?()
    }
}
