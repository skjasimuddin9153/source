//
//  SplashScreenVC.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SplashScreenVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = SplashScreenViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        createActivityIndicator()
        if isFirstTimeLogin() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                guard let vc = OnboardingVC.loadFromXIB() else { return }
                let navCtrl = PopNavigationController(rootViewController: vc)
                UIWindow.key?.replaceRootViewControllerWith(navCtrl, animated: true, completion: nil)
            })
        } else {
            viewModel.getUserData()
        }

      //  UIApplication.shared.windows.first?.replaceRootViewControllerWith(vc, animated: true, completion: nil)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    func isFirstTimeLogin() -> Bool {
        if KeychainManager.shared.authToken == nil {
            return true
        } else {
            debugPrint(KeychainManager.shared.authToken!)
            return false
        }
    }
    func createActivityIndicator() {

        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: view.bounds.midX - 15, y: view.bounds.maxY - 100, width: 30, height: 30), type: .ballRotateChase, color: UIColor.black, padding: 0)
       activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
   }
}

// MARK: - Load from storyboard with dependency
extension SplashScreenVC {
    class func loadFromXIB(withDependency dependency: SplashScreenDependency? = nil) -> SplashScreenVC? {
        let storyboard = UIStoryboard(name: "SplashScreen", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - SplashScreenAPIResponseDelegate
extension SplashScreenVC: SplashScreenAPIResponseDelegate {
    func handleGetUserResponse(data: DetailsResponseModel) {
        DataManager.shared.userLoginData = data
        switch data.user?.user_type {
        case .doctor,.nurse:
            DispatchQueue.main.async {
                let vc  = AppTabBarController()
                UIWindow.key?.replaceRootViewControllerWith(vc, animated: true, completion: nil)
            }
        case .hospital:
            DispatchQueue.main.async {
                let vc  = HospitalTabBarController()
                UIWindow.key?.replaceRootViewControllerWith(vc, animated: true, completion: nil)
            }
        default:
            ()
        }

    }
    
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) { [self] in
            view.isUserInteractionEnabled = true
            DataManager.shared.reset()
            guard let vc = LoginVC.loadFromXIB() else { return }
            let navVC = UINavigationController (rootViewController: vc)
            navVC.isNavigationBarHidden = true
            UIWindow.key?.replaceRootViewControllerWith(navVC, animated: true, completion: nil)
        }
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
    
}
