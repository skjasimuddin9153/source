//
//  SearchAPIDataManager.swift
//  Source
//
//  Created by Techwens on 27/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class SearchAPIDataManager : APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func getHospitalList(parameter: [String: AnyObject] ,completion:@escaping HospitalListResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.hospitalList, withParameters: parameter, ofType: .queryString, completion: completion)
    }
}
