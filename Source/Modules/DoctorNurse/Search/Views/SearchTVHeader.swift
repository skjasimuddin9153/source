//
//  SearchTVHeader.swift
//  Source
//
//  Created by Techwens on 28/07/22.
//

import UIKit

class SearchTVHeader: UIView {

    @IBOutlet weak var lblHeader: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func instanceFromNib() -> SearchTVHeader {
        return UINib(nibName: "SearchTVHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SearchTVHeader
    }
}
