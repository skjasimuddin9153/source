//
//  UIViewController.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//

import UIKit

extension UIViewController {
    func showAlert(title: String,
                   message: String,
                   primaryButtonTitle: String,
                   secondaryButtonTitle: String,
                   primaryButtonAction: @escaping () -> Void,
                   secondaryAction: @escaping () -> Void) {
        let alertController = UIAlertController(title: title, message: message,
                                                preferredStyle: .alert)
        let primaryActionButton = UIAlertAction(title: primaryButtonTitle, style: .default) { (_) in
            primaryButtonAction()
        }
        let secondaryActionButton = UIAlertAction(title: secondaryButtonTitle,
                                                  style: .destructive) { (_) in
                                                    secondaryAction()
        }
        alertController.addAction(secondaryActionButton)
        alertController.addAction(primaryActionButton)
        self.present(alertController, animated: true, completion: {})
    }

    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }

        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)

            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)

            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension UIViewController {
    
    func relativeDate(startDate: Date, endDate: Date)-> Bool
    {
        let diffComponents = Calendar.current.dateComponents([.year], from: startDate, to: endDate)
        let years = diffComponents.year
        return (years ?? 0) < 0
    }
    
    func isFileSizeAllowed(imageData: Data) -> Bool {
        let imageSize: Int = imageData.count
        let fileSize: Double = Double(imageSize) / 1000000.0
        print("actual size of image in KB: %f ", Double(imageSize) / 1000.0)
        print("actual size of image in MB: %f ", Double(imageSize) / 1000000.0)
        if fileSize > 4 {
            showSnackbar(withMessage: "File size too large")
            return false
        }
        return true
    }
}
