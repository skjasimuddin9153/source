//
//  PasswordSetupAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class PasswordSetupAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.register, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
    func hospitalregisterAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.hospitalRegister, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
