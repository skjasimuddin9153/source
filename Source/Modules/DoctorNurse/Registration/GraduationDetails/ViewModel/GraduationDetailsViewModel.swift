//
//  GraduationDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol GraduationDetailsAPIResponseDelegate: class {
    func handleCountryListResponse()
    func handleStateListResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class GraduationDetailsViewModel {
    weak var apiResponseDelegate: GraduationDetailsAPIResponseDelegate?
    lazy var localDataManager = GraduationDetailsLocalDataManager()
    lazy var apiDataManager = GraduationDetailsAPIDataManager()
    
    var dependency: GraduationDetailsDependency?
    var countryList: [Country_list] = []
    var stateList: [State_list] = []
    
    init() {
    }
    // Data fetch service methods goes here
    
    func getAllCountries() {
        apiDataManager.countriesAPI {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.countryList = data.country_list ?? []
                    weakSelf.stateList = []
                    weakSelf.apiResponseDelegate?.handleCountryListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
    
    func getAllState(byCountryId id: String ) {
        apiDataManager.statesAPI(id: id) {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.stateList = data.state_list ?? []
                    weakSelf.apiResponseDelegate?.handleStateListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
}
