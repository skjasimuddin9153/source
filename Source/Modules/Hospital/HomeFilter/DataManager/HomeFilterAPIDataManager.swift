//
//  HomeFilterAPIDataManager.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class HomeFilterAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func getSlotListData(completion:@escaping SlotListResponseCompletion){
        makeAPICallForListResponse(to: DoctorNurseEndpoints.slotList, completion: completion)
    }
}
