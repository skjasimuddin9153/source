//
//  LoginLocalDataManager.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//
struct PhoneDetails {
    let number: String
    let countryCode: String
}
import Foundation
import UIKit
class loginDataManager {
    static let shared = loginDataManager()

    var listCountryCode: [PhoneCode] = [
        PhoneCode(icon: UIImage(named: "ukrainFlag")!, label: "Ukrain", code: "+380"),
        PhoneCode(icon: UIImage(named: "unitedKingdom")!, label: "United Kingdom", code: "+44"),
        PhoneCode(icon: UIImage(named: "uruguay")!, label: "Uruguay", code: "+598"),
        PhoneCode(icon: UIImage(named: "uzbekisthan")!, label: "Uzbekisthan", code: "+998"),
        PhoneCode(icon: UIImage(named: "vanuatu")!, label: "Vanuatu", code: "+678"),
        PhoneCode(icon: UIImage(named: "icflag")!, label: "United States", code: "+1"),
  
    ] {
        didSet {
            // Notify observers that the data has changed
            dataDidChangeCallback?()
        }
    }

    var dataDidChangeCallback: (() -> Void)?
    
    var AuthToken:  String  = KeychainManager.shared.authToken ?? "" {
        didSet{
            KeychainManager.shared.authToken = self.AuthToken
        }
    }
    var phoneDetails: PhoneDetails?
    
}



