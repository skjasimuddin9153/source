//
//  MyAccountViewController.swift
//  Source
//
//  Created by Techwens Software on 05/09/23.
//

import UIKit

class MyAccountViewController: UIViewController {

    @IBOutlet var headerView: HeaderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.navigationController = self.navigationController
        headerView.rightBox.isHidden = true
        headerView.title.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onProfileClicked(_ sender: Any) {
        guard let vc = ProfileViewController.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onMyOrderClicked(_ sender: Any) {
        guard let vc = MyOrderViewController.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onSaveCardClicked(_ sender: Any) {
        guard let vc = MyCardsViewController.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyAccountViewController {
    class func loadFromXIB() -> MyAccountViewController? {
        let storyboard = UIStoryboard(name: "MyAccount", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MyAccountViewController") as? MyAccountViewController else {
            return nil
        }
        
        return viewController
    }
}
