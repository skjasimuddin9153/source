//
//  PasswordSetupVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class PasswordSetupVC: UIViewController {
    @IBOutlet weak var btnSend: AppSolidButton!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    // MARK: Instance variables
	lazy var viewModel = PasswordSetupViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfPassword.isSecureTextEntry = true
        tfPassword.setupSourceTextField(withPlaceholder: "Set Password")
        tfConfirmPassword.setupSourceTextField(withPlaceholder: "Confirm password")
        tfConfirmPassword.setLeftViewButton(with: UIImage(named: "icKey")!)
        tfPassword.setLeftViewButton(with: UIImage(named: "icKey")!)
        tfPassword.setRightViewButton(with: UIImage(named: "icEyeSlash")!)
        tfPassword.delegate = self
        tfConfirmPassword.delegate = self
        tfPassword.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tfPassword.isSecureTextEntry = !welf.tfPassword.isSecureTextEntry
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.register(prospect_id:"\(viewModel.dependency!.prospect_id!)",pin: tfPassword.text!, c_pin: tfConfirmPassword.text!)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    func allFieldsValid() -> Bool {
        if viewModel.dependency?.prospect_id == nil {
            showSnackbar(withMessage: "Prospect Not Found")
            return false
        }
        if (tfPassword.text?.isEmpty ?? true) || tfPassword.text == "" || tfPassword.text?.count != 4 {
            showSnackbar(withMessage: "PIN Invalid")
            tfPassword.shake()
            return false
        }
        if  (tfConfirmPassword.text?.isEmpty ?? true) || tfConfirmPassword.text == "" || tfConfirmPassword.text?.count != 4 {
            showSnackbar(withMessage: "Confirm PIN is Invalid")
            tfConfirmPassword.shake()
            return false
        }
        
        if tfPassword.text != tfConfirmPassword.text {
            showSnackbar(withMessage: "Both Pin must be same")
            tfPassword.shake()
            tfConfirmPassword.shake()
            return false
        }
       
        return true
    }
}

// MARK: - Load from storyboard with dependency
extension PasswordSetupVC {
    class func loadFromXIB(withDependency dependency: PasswordSetupDependency? = nil) -> PasswordSetupVC? {
        let storyboard = UIStoryboard(name: "PasswordSetup", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PasswordSetupVC") as? PasswordSetupVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - PasswordSetupAPIResponseDelegate
extension PasswordSetupVC: PasswordSetupAPIResponseDelegate {
    
    func handleRegisterResponse() {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        switch viewModel.dependency?.userType {
        case .hospital:
            let dependency = DefaultSlotDependency(prospect_id: viewModel.dependency?.prospect_id)
            guard let vc = DefaultSlotVC.loadFromXIB(withDependency: dependency) else { return }
            navigationController?.pushViewController(vc, animated: true)
        default:
            guard let vc = RegisterationDoneVC.loadFromXIB() else { return }
            navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
}

extension PasswordSetupVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case tfPassword :
            let maxLength = 4
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        case tfConfirmPassword :
            let maxLength = 4
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        default:
            return true
        }
       
    }
    
}
