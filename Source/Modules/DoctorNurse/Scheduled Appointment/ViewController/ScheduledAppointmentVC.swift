//
//  ScheduledAppointmentVC.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class ScheduledAppointmentVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    // MARK: Instance variables
	lazy var viewModel = ScheduledAppointmentViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        setupTableView()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getBookingList()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension ScheduledAppointmentVC {
    class func loadFromXIB(withDependency dependency: ScheduledAppointmentDependency? = nil) -> ScheduledAppointmentVC? {
        let storyboard = UIStoryboard(name: "ScheduledAppointment", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ScheduledAppointmentVC") as? ScheduledAppointmentVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ScheduledAppointmentAPIResponseDelegate
extension ScheduledAppointmentVC: ScheduledAppointmentAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleListAPI() {
        tableView.reloadData()
    }
    
}

extension ScheduledAppointmentVC {
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200.0
    }
}

extension ScheduledAppointmentVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = HomeTVHeader.instanceFromNib()
        header.lblHeader.text = "Your Appointments" 
        return header
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.bookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduledAppointmentsTVC") as? ScheduledAppointmentsTVC else { return UITableViewCell()}
        cell.data = viewModel.bookingList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dependency = ViewScheduleDependency(bookingData: viewModel.bookingList[indexPath.row])
        guard let vc = ViewScheduleVC.loadFromXIB(withDependency: dependency) else { return }
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}
