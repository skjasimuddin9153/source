/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Booking_list : Codable {
	let booking_id : Int?
	let booking_made_by_user_id : Int?
	let booking_received_by_user_id : Int?
	let booking_start_time : String?
	let booking_end_time : String?
	let booking_type : String?
	let booking_price : Int?
	let slot_id : Int?
	let slot_type : String?
	let slot_speciality : SlotType?
	let slot_start_time : String?
	let slot_end_time : String?
	let paid : Int?
	let booking_made_by : Booking_made_by?
	let booking_received_by : Booking_received_by?

	enum CodingKeys: String, CodingKey {

		case booking_id = "booking_id"
		case booking_made_by_user_id = "booking_made_by_user_id"
		case booking_received_by_user_id = "booking_received_by_user_id"
		case booking_start_time = "booking_start_time"
		case booking_end_time = "booking_end_time"
		case booking_type = "booking_type"
		case booking_price = "booking_price"
		case slot_id = "slot_id"
		case slot_type = "slot_type"
		case slot_speciality = "slot_speciality"
		case slot_start_time = "slot_start_time"
		case slot_end_time = "slot_end_time"
		case paid = "paid"
		case booking_made_by = "booking_made_by"
		case booking_received_by = "booking_received_by"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		booking_id = try values.decodeIfPresent(Int.self, forKey: .booking_id)
		booking_made_by_user_id = try values.decodeIfPresent(Int.self, forKey: .booking_made_by_user_id)
		booking_received_by_user_id = try values.decodeIfPresent(Int.self, forKey: .booking_received_by_user_id)
		booking_start_time = try values.decodeIfPresent(String.self, forKey: .booking_start_time)
		booking_end_time = try values.decodeIfPresent(String.self, forKey: .booking_end_time)
		booking_type = try values.decodeIfPresent(String.self, forKey: .booking_type)
		booking_price = try values.decodeIfPresent(Int.self, forKey: .booking_price)
		slot_id = try values.decodeIfPresent(Int.self, forKey: .slot_id)
		slot_type = try values.decodeIfPresent(String.self, forKey: .slot_type)
		slot_speciality = try values.decodeIfPresent(SlotType.self, forKey: .slot_speciality)
		slot_start_time = try values.decodeIfPresent(String.self, forKey: .slot_start_time)
		slot_end_time = try values.decodeIfPresent(String.self, forKey: .slot_end_time)
		paid = try values.decodeIfPresent(Int.self, forKey: .paid)
		booking_made_by = try values.decodeIfPresent(Booking_made_by.self, forKey: .booking_made_by)
		booking_received_by = try values.decodeIfPresent(Booking_received_by.self, forKey: .booking_received_by)
	}

}
