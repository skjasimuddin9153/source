import UIKit

@IBDesignable
class BorderButton: UIButton {
    var isValid: Bool = false {
        didSet {
            setup()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    private func setup() {
//        self.layer.cornerRadius = 10
//        self.layer.borderColor = isValid ? borderColor?.cgColor : UIColor.black.cgColor
//        self.layer.borderWidth = 1
        self.setTitleColor(isValid ? UIColor.white : UIColor(red: 0.7, green: 0.7, blue: 0.74, alpha: 1), for: .normal)
        self.backgroundColor = isValid ? UIColor.black : UIColor(red: 0.96, green: 0.96, blue: 0.97, alpha: 1)
        self.isEnabled = isValid
    }
    

}
