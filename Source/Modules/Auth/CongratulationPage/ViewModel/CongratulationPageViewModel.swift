//
//  CongratulationPageViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol CongratulationPageAPIResponseDelegate: class {
}

class CongratulationPageViewModel {
    weak var apiResponseDelegate: CongratulationPageAPIResponseDelegate?
    lazy var localDataManager = CongratulationPageLocalDataManager()
    lazy var apiDataManager = CongratulationPageAPIDataManager()
    
    var dependency: CongratulationPageDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
