//
//  LocationDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol LocationDetailsAPIResponseDelegate: class {
    func handleCountryListResponse()
    func handleStateListResponse()
    func handleRegisterResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class LocationDetailsViewModel {
    weak var apiResponseDelegate: LocationDetailsAPIResponseDelegate?
    lazy var localDataManager = LocationDetailsLocalDataManager()
    lazy var apiDataManager = LocationDetailsAPIDataManager()
    
    var dependency: LocationDetailsDependency?
    var countryList: [Country_list] = []
    var stateList: [State_list] = []
    init() {
    }
    // Data fetch service methods goes here
    
    func register(prospect_id: String, address_line: String, state: String, zip_code: String, latitude: String, longitude: String, district: String, country: String) {
        let parameters = [
            "prospect_id":prospect_id,
            "step":"5",
            "address_line_1":address_line,
            "state":state,
            "country" : country,
            "zip_code": zip_code,
            "latitude": latitude,
            "longitude":longitude,
            "district": district,
        ]
        apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRegisterResponse()
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
    
    func getAllCountries() {
        apiDataManager.countriesAPI {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.countryList = data.country_list ?? []
                    weakSelf.stateList = []
                    weakSelf.apiResponseDelegate?.handleCountryListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
    
    func getAllState(byCountryId id: String ) {
        apiDataManager.statesAPI(id: id) {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.stateList = data.state_list ?? []
                    weakSelf.apiResponseDelegate?.handleStateListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
}
