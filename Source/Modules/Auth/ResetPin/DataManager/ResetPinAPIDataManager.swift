//
//  ResetPinAPIDataManager.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class ResetPinAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func resetPinAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.resetPin, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
