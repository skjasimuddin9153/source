//
//  DemoVC.swift
//  Source
//
//  Created by Techwens on 25/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class DemoVC: UIViewController {
    
    var onReloadPress:(()->Void)?
    var onBtnGoHomePress:(()->Void)?
    // MARK: Instance variables
	lazy var viewModel = DemoViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    
    @IBAction func btnReload(_ sender: Any) {
        onReloadPress?()
    }
    
    @IBAction func btnGoHome(_ sender: Any) {
        onBtnGoHomePress?()
    }
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension DemoVC {
    class func loadFromXIB(withDependency dependency: DemoDependency? = nil) -> DemoVC? {
        let storyboard = UIStoryboard(name: "Demo", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "DemoVC") as? DemoVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - DemoAPIResponseDelegate
extension DemoVC: DemoAPIResponseDelegate {
}
