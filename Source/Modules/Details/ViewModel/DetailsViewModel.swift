//

//
//  LoginPinViewModel.swift
//  Source
//
//  Created by jasim0021 on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import Alamofire



final class DetailsViewModel {
  
    

    enum Event {
        case loading
        case stopLoading
        case dataLoaded
        case error(Error)
        case ProductData(ProductResponse)
        case LocationData(Locationresponse)
    }

    var eventHandler: ((Event) -> Void)?

    // Your existing login and other functions here...

    func getAllproduct(page: Int, perPage:Int) {
        self.eventHandler?(.loading) // Notify the loading state
        
    
        
        APIManager.shared.request(
            method: .get,
            endpoint:AuthEndpoints.getAllProduct(page: page, perPage: perPage)
          
          
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: ProductResponse = try JSONDecoder().decode(ProductResponse.self, from: data)
                    print("Success: \(json)")
                    self.eventHandler?(.ProductData(json)) // Notify success and pass data
                  
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
    func getLocationByLatLng(lat:Any,lng:Any){
        self.eventHandler?(.loading) // Notify the loading state
        
    
        
        APIManager.shared.request(
            method: .get,
            endpoint:AuthEndpoints.getLocationByLatLng(lat: lat, lng: lng)
          
          
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: Locationresponse = try JSONDecoder().decode(Locationresponse.self, from: data)
                    print("Success: \(json)")
                    self.eventHandler?(.LocationData(json)) // Notify success and pass data
                  
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
}
