//
//  UndergraduateDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol UndergraduateDetailsAPIResponseDelegate: class {
}

class UndergraduateDetailsViewModel {
    weak var apiResponseDelegate: UndergraduateDetailsAPIResponseDelegate?
    lazy var localDataManager = UndergraduateDetailsLocalDataManager()
    lazy var apiDataManager = UndergraduateDetailsAPIDataManager()
    
    var dependency: UndergraduateDetailsDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
