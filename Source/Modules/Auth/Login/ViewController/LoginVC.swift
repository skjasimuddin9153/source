//
//  LoginVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import TTGSnackbar
import Alamofire
class LoginVC: UIViewController {
    // MARK: Instance variables
    //    @IBOutlet weak var screenHeight: NSLayoutConstraint!
    
    @IBOutlet weak var tfMobilePhone: UITextField!
    @IBOutlet var btnGenerateOTP: BorderButton!
    @IBOutlet var countryCodeView: UIView!
    @IBOutlet var numberView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var countryFlag: UIImageView!
    @IBOutlet var countryCode: UILabel!
    
    var isTableVisible : Bool = true
    lazy var viewModel = LoginViewModel()
    // MARK: - View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        tfMobilePhone.delegate = self
        style()
        //    btnGenerateOTP.isValid = false
        tfMobilePhone.addTarget(self, action: #selector(phoneNumberDidChange(_:)), for: .editingChanged)
        
        
        
    }
    
    @IBAction func onbackBynClicked(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func oncountryselectClicked(_ sender: Any) {
        //        tableView.isHidden = isTableVisible
        //         isTableVisible = !isTableVisible
    }
    
    
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        screenHeight.constant = self.view.frame.height - 44
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
        debugPrint("\(self) deinitialized")
    }
    @objc func phoneNumberDidChange(_ textField: UITextField) {
        if let phoneNumber = textField.text, phoneNumber.count == 10 {
            textField.resignFirstResponder()
            btnGenerateOTP.isValid = true  // Enable the button
            
        } else {
            btnGenerateOTP.isValid = false // Disable the button
        }
    }
    
    @IBAction func tappedCreateAccountBn(_ sender: UIButton) {
        print("Archan:tap")
        
        guard let vc = LoginPinVC.loadFromXIB() else { return }
        
        print("number", tfMobilePhone?.text)
        
        if let phoneNumber = tfMobilePhone.text, phoneNumber.count == 10 {
            
            btnGenerateOTP.titleLabel?.text = "Loading..."
            
            viewModel.login(countryCode: countryCode.text ?? "+1", mobilePhone:tfMobilePhone?.text ?? "")
            vc.phoneNumber = phoneNumber
            vc.countryCode = countryCode.text ?? "+1"
            observeLoginViewModelEvents(viewController: vc)
            
            
        }
    }
        func observeLoginViewModelEvents(viewController:LoginPinVC) {
               viewModel.eventHandler = { [weak self] event in
                   guard let self = self else { return }
                
                   switch event {
                   case .loading:
                       // Handle loading state (e.g., show activity indicator)
                       print("Loading...")
                       btnGenerateOTP.startAnimation()
                   case .stopLoading:
                       // Handle stop loading state (e.g., hide activity indicator)
                       print("Stop loading...")
                       
                   case .data(let loginResponse):
                       viewController.authKey = loginResponse.auth_key ?? ""
                   case .dataLoaded:
                       // Handle data loaded event (e.g., update UI)
                       print("Data loaded...")
                       DispatchQueue.main.async {
                   
                           self.navigationController?.pushViewController(viewController, animated: true)
                       }
                   case .error(let error):
                       // Handle error (e.g., show an error message)
                       print("Error: \(error)")
                   
               }
           }
       
        
        
    }
    func style (){
        numberView.borderWidth  = 1
        numberView.borderColor = UIColor.black
        countryCodeView.borderWidth = 0.5
        countryCodeView.borderColor = UIColor.gray
        tableView.isHidden = isTableVisible
        tableView.separatorStyle = .none
        tableView.allCornersRadius = 10
        tableView.borderWidth = 0.1
        tableView.borderColor = .gray
        tableView.layer.shadowColor = UIColor.black.cgColor
        tableView.layer.shadowOpacity = 0.5
        tableView.layer.shadowOffset = CGSize(width: 2, height: 2)
        tableView.layer.shadowRadius = 4
    }
}

// MARK: - Load from storyboard with dependency
extension LoginVC {
    class func loadFromXIB() -> LoginVC? {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC else {
            return nil
        }
       
        return viewController
    }
}

extension LoginVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           // Get the current text and calculate the new length after replacement
           let currentText = textField.text ?? ""
           let newText = (currentText as NSString).replacingCharacters(in: range, with: string)
           
           // Limit the input to 10 characters
           return newText.count <= 10
       }
}
extension LoginVC: UITableViewDelegate,UITableViewDataSource {
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return loginDataManager.shared.listCountryCode.count
    }
    
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell:NumberListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NumberListTableViewCell
//      cell.selectionStyle = .none/
      cell.icon.image = loginDataManager.shared.listCountryCode[indexPath.row].icon
      cell.label.text = loginDataManager.shared.listCountryCode[indexPath.row].label
      cell.code.text = loginDataManager.shared.listCountryCode[indexPath.row].code
      
     
      
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        countryFlag.image = loginDataManager.shared.listCountryCode[indexPath.row].icon
        countryCode.text = loginDataManager.shared.listCountryCode[indexPath.row].code
        return tableView.isHidden = true
    }
    
    
}

// MARK: - LoginAPIResponseDelegate
//extension LoginVC: LoginAPIResponseDelegate {
//    func handleFindUserResponse(data: FindUserData) {
//        btnGenerateOTP.hideSpinner()
//        view.isUserInteractionEnabled = true
//        let dependency = LoginPinDependency(emailPhone: tfMobilePhone.text!, userType: .doctor , user: data.user!)
//        guard let vc = LoginPinVC.loadFromXIB(withDependency: dependency) else { return }
//        navigationController?.pushViewController(vc, animated: true)
////        switch data.user?.user_type {
////        case .doctor:
////            let dependency = LoginPinDependency(emailPhone: tfEmailMobile.text!, userType: .doctor , user: data.user!)
////            guard let vc = LoginPinVC.loadFromXIB(withDependency: dependency) else { return }
////            navigationController?.pushViewController(vc, animated: true)
////        case .hospital:
////            let dependency = LoginPinDependency(emailPhone: tfEmailMobile.text!, userType: .hospital ,user: data.user!)
////            guard let vc = LoginPinVC.loadFromXIB(withDependency: dependency) else { return }
////            navigationController?.pushViewController(vc, animated: true)
////        case .nurse:
////            let dependency = LoginPinDependency(emailPhone: tfEmailMobile.text!, userType: .nurse ,user: data.user!)
////            guard let vc = LoginPinVC.loadFromXIB(withDependency: dependency) else { return }
////            navigationController?.pushViewController(vc, animated: true)
////        default:
////            showSnackbar(withMessage: "Invalid User Type")
////            return
////        }
//
//    }
//
//    func handleAPIError(_ error: Error) {
//        btnGenerateOTP.hideSpinner()
//        view.isUserInteractionEnabled = true
//        showSnackbar(withMessage: error.localizedDescription)
//    }
//
//    func handleAPIMessage(message: String) {
//        showSnackbar(withMessage: message)
//    }
//
//}

//extension LoginVC : UITabBarDelegate,UITableViewDataSource {
////    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////        return 1
////    }
////
////    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////        guard cell:
////    }
//    
//    
//}
