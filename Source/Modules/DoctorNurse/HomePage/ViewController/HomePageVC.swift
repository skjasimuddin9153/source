//
//  HomePageVC.swift
//  Source
//
//  Created by Techwens on 22/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class HomePageVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var lblSalutation: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblAddress: UILabel!
    lazy var viewModel = HomePageViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        viewModel.favouriteNavigationDelegate = self
        setupTableView()

        lblAddress.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            guard let vc = ChangeLocationVC.loadFromXIB() else {
                return
            }
            welf.navigationController?.pushViewController(vc, animated: true)
        }
        guard let data = DataManager.shared.getUserData() else { return }
        lblSalutation.text = "Hello \(data.user?.user_type?.title ?? "") \(data.user?.first_name ?? "") \nGood morning"
        let fullAddress = (data.user?.state ?? "") + " " + "\(data.user?.zip_code ?? "0")"
        let mainAddress = (data.user?.address_line_1 ?? "") + " " + (data.user?.address_line_2 ?? "")
        lblAddress.text =  mainAddress + " " + fullAddress
    }
    override func viewDidLayoutSubviews() {

        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getExploreHospitalData()
        viewModel.getFavouriteHospitalData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension HomePageVC {
    class func loadFromXIB(withDependency dependency: HomePageDependency? = nil) -> HomePageVC? {
        let storyboard = UIStoryboard(name: "HomePage", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "HomePageVC") as? HomePageVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - HomePageAPIResponseDelegate
extension HomePageVC: HomePageAPIResponseDelegate {
    func handleFavHospitaListResponse() {
        tableView.reloadData()
    }
    
    func handleGetHospitaListResponse() {
        tableView.reloadData()
    }
    
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        //showSnackbar(withMessage: message)
    }
    
}
extension HomePageVC: FavouriteNavigationDelegate {
    func tappedHospitalFor(index: Int) {
        let id = viewModel.favouriteHospital[index].user_id
        let dependency = BookingTicketDependency(hospitalID: id)
        guard let vc = BookingTicketVC.loadFromXIB(withDependency: dependency) else { return }
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomePageVC {
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200.0
    }
}

extension HomePageVC: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
       return viewModel.numberOfSection()
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return viewModel.viewForHeaderInSection(section: section)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.cellForRowAt(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewModel.heightForSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch viewModel.currentTableState() {
        case .both:
            switch indexPath.section {
            case 1:
                print("navigate")
                if let id =  viewModel.currentAreaHospital[indexPath.row].id {
                    let dependency = BookingTicketDependency(hospitalID: id)
                    guard let vc = BookingTicketVC.loadFromXIB(withDependency: dependency) else { return }
                    vc.hidesBottomBarWhenPushed = true
                    navigationController?.pushViewController(vc, animated: true)
                }
                
            default:
                ()
            }
        case .local:
            print("navigate")
            let id = viewModel.currentAreaHospital[indexPath.row].id
            let dependency = BookingTicketDependency(hospitalID: id)
            guard let vc = BookingTicketVC.loadFromXIB(withDependency: dependency) else { return }
            vc.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(vc, animated: true)
        case .favourite, .none:
            ()
        }
    }
}


