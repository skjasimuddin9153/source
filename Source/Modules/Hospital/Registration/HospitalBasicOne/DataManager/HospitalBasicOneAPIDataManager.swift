//
//  HospitalBasicOneAPIDataManager.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class HospitalBasicOneAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func countriesAPI(completion:@escaping CountryResponseCompletion){
        makeAPICall(to: UtiltiyEndpoints.countryList,  ofType: .queryString, completion: completion)

    }
}
