/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Booking_made_by : Codable {
	let username : String?
	let first_name : String?
	let last_name : String?
	let email : String?
	let phone_code : Int?
	let phone_number : String?
	let user_type : String?
	let profile_image : String?

	enum CodingKeys: String, CodingKey {

		case username = "username"
		case first_name = "first_name"
		case last_name = "last_name"
		case email = "email"
		case phone_code = "phone_code"
		case phone_number = "phone_number"
		case user_type = "user_type"
		case profile_image = "profile_image"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		phone_code = try values.decodeIfPresent(Int.self, forKey: .phone_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		user_type = try values.decodeIfPresent(String.self, forKey: .user_type)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
	}

}