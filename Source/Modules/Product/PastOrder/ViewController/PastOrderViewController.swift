//
//  PastOrderViewController.swift
//  Source
//
//  Created by Techwens Software on 07/09/23.
//

import UIKit

class PastOrderViewController: UIViewController {

    @IBOutlet var clouseBtn: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var helpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clouseBtnClicked(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        print("btn Clicked")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
     @IBAction func clouseBtnClicked(_ sender: Any) {
     }
     }
    */
    private func style(){
        helpBtn.allCornersRadius = 20
        clouseBtn.allCornersRadius = 20
    }
}
extension PastOrderViewController {
    class func loadFromXIB() -> PastOrderViewController? {
        let storyboard = UIStoryboard(name: "PastOrder", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PastOrderViewController") as? PastOrderViewController else {
            return nil
        }
      
        return viewController
    }
}

extension PastOrderViewController :UITableViewDataSource,UITabBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PastOrderSecoundCell
        cell.selectionStyle = .none
    
        return cell
    }
    
    
}
