//
//  RegisterationDoneVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class RegisterationDoneVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = RegisterationDoneViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension RegisterationDoneVC {
    class func loadFromXIB(withDependency dependency: RegisterationDoneDependency? = nil) -> RegisterationDoneVC? {
        let storyboard = UIStoryboard(name: "RegisterationDone", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "RegisterationDoneVC") as? RegisterationDoneVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - RegisterationDoneAPIResponseDelegate
extension RegisterationDoneVC: RegisterationDoneAPIResponseDelegate {
}
