//
//  HospitalScheduleViewModel.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol HospitalScheduleAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleListAPI()
}

class HospitalScheduleViewModel {
    weak var apiResponseDelegate: HospitalScheduleAPIResponseDelegate?
    lazy var localDataManager = HospitalScheduleLocalDataManager()
    lazy var apiDataManager = HospitalScheduleAPIDataManager()
    
    var dependency: HospitalScheduleDependency?
    var slotList: [SlotListResponseDataModel] = []
    var fullSlotList: [SlotListResponseDataModel] = []
    init() {
    }
    // Data fetch service methods goes here
    func getBankList() {
        apiDataManager.getSlotListData{ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                guard let data = response.data else { return }
                weakSelf.slotList = data
                weakSelf.fullSlotList = data
                weakSelf.apiResponseDelegate?.handleListAPI()
            }
        }
    }
    
    func deleteSlotWithID(id :Int) {
        let parameter = ["slot_id" : id] as! [String: Any]
        apiDataManager.deleteSlotData(parameter: parameter){ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(_):
                weakSelf.getBankList()
            }
        }
    }
}
