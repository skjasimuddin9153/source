//
//  UIView.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//

import UIKit

extension UIView {

    func snapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result ?? UIImage()
    }
    
    func addBlur() {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.5
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
}

extension UIView {

    func roundedCorners(_ corners: CACornerMask, radius: CGFloat) {
        layer.cornerRadius = radius
        layer.maskedCorners = corners
    }
    
    func shake() {
        self.transform = CGAffineTransform(translationX: 10, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}

extension UIView {
    // add gradient to uiview
    public func applyGradient(color: [UIColor], location: [NSNumber]?) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = color.map({$0.cgColor})
        gradient.locations = location
        gradient.startPoint = CGPoint(x: 0.5, y: 0.0)// CGPoint(x: 0.0,y: 0.5)
        gradient.endPoint = CGPoint(x: 0.5, y: 1.0)// CGPoint(x: 1.0,y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            if let borderColor = layer.borderColor {
                return UIColor(cgColor: borderColor)
            } else {
                return nil
            }
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }

    @IBInspectable var topCornersRadius: Int {
        get {
            return Int(self.layer.cornerRadius)
        }
        set {
            roundedCorners([.layerMinXMinYCorner, .layerMaxXMinYCorner],
                           radius: CGFloat(newValue))
        }
    }

    @IBInspectable var topLeftCornersRadius: Int {
        get {
            return Int(self.layer.cornerRadius)
        }
        set {
            roundedCorners([.layerMinXMinYCorner],
                           radius: CGFloat(newValue))

        }
    }

    @IBInspectable var allCornersRadius: Int {
        get {
            return Int(self.layer.cornerRadius)
        }
        set {
            roundedCorners([.layerMinXMinYCorner,
                            .layerMaxXMinYCorner,
                            .layerMaxXMaxYCorner,
                            .layerMinXMaxYCorner],
                           radius: CGFloat(newValue))
        }
    }

    @IBInspectable var bottomCornersRadius: Int {
        get {
            return Int(self.layer.cornerRadius)
        }
        set {
            roundedCorners([.layerMinXMaxYCorner, .layerMaxXMaxYCorner],
                           radius: CGFloat(newValue))
        }
    }

    @IBInspectable var bottomRightCornerRadius: Int {
        get {
            return Int(self.layer.cornerRadius)
        }
        set {
            roundedCorners([.layerMaxXMaxYCorner],
                           radius: CGFloat(newValue))
        }
    }
}

extension UIView{

    func showLoader(withBlur blur: Bool = false){
        let loaderView  = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        loaderView.tag = -888754
        if blur {
            loaderView.addBlur()
        }
//        let activityIndicator = NVActivityIndicatorView(frame: CGRect(x: bounds.midX, y: bounds.midY, width: 60, height: 60), type: .ballRotateChase, color: .greenButton , padding: 0)
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: bounds.midX, y: bounds.midY, width: 60, height: 60))
        activityIndicator.center = loaderView.center
        activityIndicator.startAnimating()
        loaderView.addSubview(activityIndicator)
        self.subviews.forEach { $0.isHidden = true }
        self.addSubview(loaderView)
    }


    func dismissLoader(){
        self.viewWithTag(-888754)?.removeFromSuperview()
        self.subviews.forEach { $0.isHidden = false }
    }
}


extension UIView {
  func addDashedBorder() {
      let color = UIColor.gray.cgColor

    let shapeLayer:CAShapeLayer = CAShapeLayer()
    let shapeRect = self.bounds
    shapeLayer.bounds = shapeRect
    shapeLayer.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2)
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 2
    shapeLayer.lineJoin = CAShapeLayerLineJoin.round
    shapeLayer.lineDashPattern = [8,8]
    shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

    self.layer.addSublayer(shapeLayer)
      
    }
}

extension UIView {
   func createDottedLine(width: CGFloat, color: CGColor) {
      let caShapeLayer = CAShapeLayer()
      caShapeLayer.strokeColor = color
      caShapeLayer.lineWidth = width
      caShapeLayer.lineDashPattern = [5,5]
      let cgPath = CGMutablePath()
      let cgPoint = [CGPoint(x: 0, y: 0), CGPoint(x: self.frame.width, y: 0)]
      cgPath.addLines(between: cgPoint)
      caShapeLayer.path = cgPath
      layer.addSublayer(caShapeLayer)
   }
}

extension UIView {
  
  
  /* Usage Example
   * bgView.addBottomRoundedEdge(desiredCurve: 1.5)
   */
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
}

extension UIView {
    func  applyLightBottomShadow() {

        layer.shadowColor = UIColor(white: 0.0, alpha: 0.2).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowOpacity = 0.6
        layer.shadowRadius = 1
    }
    func  applyBottomShadow(color: UIColor) {

        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 2
    }

    func  applyTopShadow(color: UIColor) {

        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: -9.0)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 7.0
    }
    func  applyLightShadow() {

        layer.shadowColor = UIColor(white: 0.0, alpha: 0.2).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 3.0
    }

    func applyAllAroundShadow() {

        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 8
    }

    func applyLightAllAroundShadow() {

        layer.shadowColor = UIColor(white: 0.0, alpha: 0.3).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 5
    }

    func applyCommonShadow() {

        layer.shadowColor = UIColor(white: 0.0, alpha: 0.4).cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 1.8)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 4.0
    }

    func applyDarkAllAroundShadow() {

        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 1.0
    }

    func removeShadow() {

        layer.shadowColor = UIColor.clear.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.0
        layer.shadowRadius = 0.0
    }

    func applyShadow(withColor color: UIColor) {

        layer.shadowColor = color.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
    }
}
