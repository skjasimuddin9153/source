//
//  HospitalBasicOneViewModel.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol HospitalBasicOneAPIResponseDelegate: class {
    func handleCountryListResponse()
    func handleAPIError(_ error: Error)
}

class HospitalBasicOneViewModel {
    weak var apiResponseDelegate: HospitalBasicOneAPIResponseDelegate?
    lazy var localDataManager = HospitalBasicOneLocalDataManager()
    lazy var apiDataManager = HospitalBasicOneAPIDataManager()
    
    var dependency: HospitalBasicOneDependency?
    var countryList: [Country_list] = []
    
    init() {
    }
    // Data fetch service methods goes here
    func getAllCountries() {
        apiDataManager.countriesAPI {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.countryList = data.country_list ?? []
                    weakSelf.apiResponseDelegate?.handleCountryListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
}
