//
//  RegistrationDoneViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol RegistrationDoneAPIResponseDelegate: class {
}

class RegistrationDoneViewModel {
    weak var apiResponseDelegate: RegistrationDoneAPIResponseDelegate?
    lazy var localDataManager = RegistrationDoneLocalDataManager()
    lazy var apiDataManager = RegistrationDoneAPIDataManager()
    
    var dependency: RegistrationDoneDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
