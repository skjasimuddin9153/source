//
//  HospitalScheduleAPIDataManager.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias SlotListResponseCompletion = (Result<APIListResponse<SlotListResponseDataModel>, Error>) -> Void

class HospitalScheduleAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func getSlotListData(completion:@escaping SlotListResponseCompletion){
        makeAPICallForListResponse(to: DoctorNurseEndpoints.slotList, completion: completion)
    }
    
    func deleteSlotData(parameter : [String: Any],completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.deleteSlot,withParameters: parameter, ofType: .httpBody, completion: completion)
    }
}
