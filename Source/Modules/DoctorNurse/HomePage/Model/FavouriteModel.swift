/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct FavouriteModel : Codable {
	let favourite_id : Int?
	let user_id : Int?
	let hospital_name : String?
	let address_line_1 : String?
	let address_line_2 : String?
	let latitude : String?
	let longitude : String?
	let state : String?
	let district : String?
	let zip_code : Int?
	let number_of_emergency_beds : Int?
	let number_of_icu_units : Int?
	let number_of_icu_beds : Int?
	let hospital_image : String?
	let speciality : String?

	enum CodingKeys: String, CodingKey {

		case favourite_id = "favourite_id"
		case user_id = "user_id"
		case hospital_name = "hospital_name"
		case address_line_1 = "address_line_1"
		case address_line_2 = "address_line_2"
		case latitude = "latitude"
		case longitude = "longitude"
		case state = "state"
		case district = "district"
		case zip_code = "zip_code"
		case number_of_emergency_beds = "number_of_emergency_beds"
		case number_of_icu_units = "number_of_icu_units"
		case number_of_icu_beds = "number_of_icu_beds"
		case hospital_image = "hospital_image"
		case speciality = "speciality"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		favourite_id = try values.decodeIfPresent(Int.self, forKey: .favourite_id)
		user_id = try values.decodeIfPresent(Int.self, forKey: .user_id)
		hospital_name = try values.decodeIfPresent(String.self, forKey: .hospital_name)
		address_line_1 = try values.decodeIfPresent(String.self, forKey: .address_line_1)
		address_line_2 = try values.decodeIfPresent(String.self, forKey: .address_line_2)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		zip_code = try values.decodeIfPresent(Int.self, forKey: .zip_code)
		number_of_emergency_beds = try values.decodeIfPresent(Int.self, forKey: .number_of_emergency_beds)
		number_of_icu_units = try values.decodeIfPresent(Int.self, forKey: .number_of_icu_units)
		number_of_icu_beds = try values.decodeIfPresent(Int.self, forKey: .number_of_icu_beds)
		hospital_image = try values.decodeIfPresent(String.self, forKey: .hospital_image)
		speciality = try values.decodeIfPresent(String.self, forKey: .speciality)
	}

}
