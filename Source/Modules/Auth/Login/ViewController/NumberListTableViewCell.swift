//
//  NumberListTableViewCell.swift
//  Source
//
//  Created by Techwens Software on 27/09/23.
//

import UIKit

class NumberListTableViewCell: UITableViewCell {

    @IBOutlet var icon: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var code: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

