//
//  PickerViewViewModel.swift
//  UAE10
//
//  Created by Techwens on 19/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol PickerViewAPIResponseDelegate: class {
}

class PickerViewViewModel {
    weak var apiResponseDelegate: PickerViewAPIResponseDelegate?
    lazy var localDataManager = PickerViewLocalDataManager()
    lazy var apiDataManager = PickerViewAPIDataManager()
    
    var dependency: PickerViewDependency?
    var dataList: [String] = []
    var originaldataList: [String] = []
    
    init() {
    }
    // Data fetch service methods goes here
}
