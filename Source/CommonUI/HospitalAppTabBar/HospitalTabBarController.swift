//
//  HospitalTabBarController.swift
//  Source
//
//  Created by Techwens on 20/09/22.
//

import UIKit
import Localize_Swift

class HospitalTabBarController: UITabBarController {

    let viewModel = HospitalTabViewModel()
    static weak var current: HospitalTabBarController?

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        setupUI()
        HospitalTabBarController.current = self
        setupViewControllers()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    // MARK: UI Setup
    func setupUI() {
        self.tabBar.backgroundColor = UIColor.white
        self.tabBar.topCornersRadius = 10
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.tabBar.backgroundColor = .white
        self.tabBar.tintColor = .accentColor
        self.tabBar.unselectedItemTintColor = UIColor.gray
        
        tabBar.layer.shadowOffset = CGSize(width: 0, height: -3)
        tabBar.layer.shadowRadius = 5
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.3
        
        // Remove the line
        if #available(iOS 13.0, *) {
            let appearance = self.tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            self.tabBar.standardAppearance = appearance
        } else {
            self.tabBar.shadowImage = UIImage()
            self.tabBar.backgroundImage = UIImage()
        }
    }

    // MARK: VC Setup
    func setupViewControllers() {
        viewControllers = viewModel.tabItemData.compactMap { (tabItemData) -> UINavigationController? in
            guard let viewController = tabItemData.viewController else {
                return nil
            }
            
            return setTabBarItem(forViewController: viewController, withTitle: tabItemData.title ,image: tabItemData.image, selectedImage: tabItemData.selectedImage)
        }
    }

    func setTabBarItem(forViewController viewController: UIViewController, withTitle title: String? = nil, image: UIImage, selectedImage: UIImage) -> UINavigationController {
        let tabBarItem = UITabBarItem(title: title,
                                           image: image.withRenderingMode(.alwaysOriginal),
                                           selectedImage: selectedImage.withRenderingMode(.alwaysOriginal))
        let navVC = PopNavigationController(rootViewController: viewController)
        navVC.tabBarItem = tabBarItem
        return navVC
    }
}

// MARK: - UITabBarController Delegate
extension HospitalTabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
}

