//
//  PickerViewDependency.swift
//  UAE10
//
//  Created by Techwens on 19/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct PickerViewDependency {
    let title: String
    let dataList: [String]
}

struct PickerDataModel {
    let id: Int
    let value : String
}
