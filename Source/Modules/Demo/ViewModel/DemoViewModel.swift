//
//  DemoViewModel.swift
//  Source
//
//  Created by Techwens on 25/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol DemoAPIResponseDelegate: class {
}

class DemoViewModel {
    weak var apiResponseDelegate: DemoAPIResponseDelegate?
    lazy var localDataManager = DemoLocalDataManager()
    lazy var apiDataManager = DemoAPIDataManager()
    
    var dependency: DemoDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
