//
//  VerificationDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias RegStepTwoResponseCompletion = (Result<APIResponse<EmptyAPIResponseModel>, Error>) -> Void

class VerificationDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here    
    func registerAPI(with payload: UploadPayloadData, parameters: [String:Any], completion:@escaping RegStepTwoResponseCompletion){
        makeMultipartAPICall(to: AuthEndpoints.register, withBody: parameters, payload: [payload], progressHandler: nil, completion: completion)
    }
}
