//
//  ViewScheduleDependency.swift
//  Source
//
//  Created by Techwens on 07/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

struct ViewScheduleDependency {
    var bookingData: Booking_list
}

struct CalendarData {
    var startDate: String
    var endDate : String
    var availableDates: [String]
    var selectedCalendarData: [String]
}
