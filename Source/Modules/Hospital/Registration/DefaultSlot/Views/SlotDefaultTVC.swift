//
//  SlotDefaultTVC.swift
//  Source
//
//  Created by Techwens on 03/07/22.
//

import UIKit

protocol ModifySlotDelegate {
    func deleteSlotAt(index: Int)
    func editSlotAt(index: Int)
}
extension ModifySlotDelegate {
    func editSlotAt(index: Int) {
        
    }
}

class SlotDefaultTVC: UITableViewCell {

    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblSlotTime: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    var delegate: ModifySlotDelegate?
    var index = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func tappedDeleteBtn(_ sender: UIButton) {
        delegate?.deleteSlotAt(index: index)
    }
    
    @IBAction func tappedEditBtn(_ sender: UIButton) {
        delegate?.editSlotAt(index: index)
    }
}
