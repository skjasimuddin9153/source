//
//  UserTypeRegVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
enum UserType : String, Codable {
    case doctor = "doctor"
    case nurse = "nurse"
    case hospital = "hospital"
    
    var placeholderImage: UIImage {
        switch self {
        case .doctor:
            return UIImage(named: "icDocPlaceholder")!
        case .nurse:
            return UIImage(named: "icNurse")!
        case .hospital:
            return UIImage(named: "icHospital")!
        }
    }
    
    var title: String {
        switch self {
        case .doctor:
            return "Doctor"
        case .nurse:
            return "Nurse"
        case .hospital:
            return "Hospital"
        }
    }
}

class UserTypeRegVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var docView: UIView!
    @IBOutlet weak var nurseView: UIView!
    @IBOutlet weak var hospitalView: UIView!
    @IBOutlet weak var lblDoc: UILabel!
    @IBOutlet weak var lblNurse: UILabel!
    @IBOutlet weak var lblHospital: UILabel!
    @IBOutlet weak var docSelector: UIView!
    @IBOutlet weak var nurseSelector: UIView!
    @IBOutlet weak var hospitalSelector: UIView!
    
    @IBOutlet var selectorView: [UIView]!
    @IBOutlet var lblBtnTitle: [UILabel]!
    @IBOutlet var btnView: [UIView]!
    lazy var viewModel = UserTypeRegViewModel()
    var selectedType: UserType? {
        didSet {
            switch selectedType {
            case .doctor:
                btnView.forEach({$0.backgroundColor = .lightGrey ?? .lightGray})
                lblBtnTitle.forEach({$0.textColor = .black })
                selectorView.forEach({$0.backgroundColor = .clear})
                docView.backgroundColor = .accentColor ?? .yellow
                lblDoc.textColor = .white
                docSelector.backgroundColor = .appDark ?? .black
            case .nurse:
                btnView.forEach({$0.backgroundColor = .lightGrey ?? .lightGray})
                lblBtnTitle.forEach({$0.textColor = .black })
                selectorView.forEach({$0.backgroundColor = .clear})
                nurseView.backgroundColor = .accentColor ?? .yellow
                lblNurse.textColor = .white
                nurseSelector.backgroundColor = .appDark ?? .black
            case .hospital:
                btnView.forEach({$0.backgroundColor = .lightGrey ?? .lightGray})
                lblBtnTitle.forEach({$0.textColor = .black })
                selectorView.forEach({$0.backgroundColor = .clear})
                hospitalView.backgroundColor = .accentColor ?? .yellow
                lblHospital.textColor = .white
                hospitalSelector.backgroundColor = .appDark ?? .black
            default:
                ()
            }
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        selectorView.forEach({$0.backgroundColor = .clear})
        docView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selectedType = .doctor
            let dependency = PersonalDetailsDependency(userType: .doctor)
            guard let vc = PersonalDetailsVC.loadFromXIB(withDependency: dependency) else {
                return
            }
            welf.navigationController?.pushViewController(vc, animated: true)
        }
        nurseView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selectedType = .nurse
            let dependency = PersonalDetailsDependency(userType: .nurse)
            guard let vc = PersonalDetailsVC.loadFromXIB(withDependency: dependency) else {
                return
            }
            welf.navigationController?.pushViewController(vc, animated: true)
        }
        hospitalView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selectedType = .hospital
            let dependency = HospitalBasicOneDependency(userType: .hospital)
            guard let vc = HospitalBasicOneVC.loadFromXIB(withDependency: dependency) else {
                return
            }
            welf.navigationController?.pushViewController(vc, animated: true)
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - Load from storyboard with dependency
extension UserTypeRegVC {
    class func loadFromXIB(withDependency dependency: UserTypeRegDependency? = nil) -> UserTypeRegVC? {
        let storyboard = UIStoryboard(name: "UserTypeReg", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "UserTypeRegVC") as? UserTypeRegVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - UserTypeRegAPIResponseDelegate
extension UserTypeRegVC: UserTypeRegAPIResponseDelegate {
}
