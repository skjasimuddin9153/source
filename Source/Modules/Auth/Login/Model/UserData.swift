/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserData : Codable {
	let first_name : String?
	let last_name : String?
	let email : String?
	let username : String?
	let user_type : UserType?

	enum CodingKeys: String, CodingKey {

		case first_name = "first_name"
		case last_name = "last_name"
		case email = "email"
		case username = "username"
		case user_type = "user_type"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		user_type = try values.decodeIfPresent(UserType.self, forKey: .user_type)
	}

}


struct LoginResponse: Decodable {
    let status: Bool
    let data: DataObject?
    let message:String?
    let auth_key:String?
    
}
   

struct DataObject: Decodable {
    let message: String?
    let status: Bool?
    let response: String?
    let error: String?
    let customerType: String?  // Use camelCase property name here
    
    // Define custom coding keys to convert customerType to snake_case
    private enum CodingKeys: String, CodingKey {
        case message
        case status
        case response
        case error
        case customerType = "customer_type"
     
    }
    
    
    // Define an enum to represent either a ResponseObject or a String
    enum ResponseData: Decodable {
        case responseObject(ResponseObject)
        case stringResponse(String)
        
        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            
            if let responseObject = try? container.decode(ResponseObject.self) {
                self = .responseObject(responseObject)
            } else if let responseString = try? container.decode(String.self) {
                self = .stringResponse(responseString)
            } else {
                throw DecodingError.dataCorruptedError(in: container, debugDescription: "Failed to decode ResponseData")
            }
        }
    }
}

struct ResponseObject: Decodable {
   let authKey: String
 
   private enum CodingKeys: String, CodingKey {
       case authKey = "auth_key"
   }
}
