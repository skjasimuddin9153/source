//
//  HeaderView.swift
//  Source
//
//  Created by Techwens Software on 31/08/23.
//

import UIKit

class HeaderView: UIView {
    @IBOutlet var containtView: UIView!
//    var isleftBtn : Bool = true
    @IBOutlet var leftBackBtn: UIButton!
    @IBOutlet var rightBox: UIView!
    
    @IBOutlet var title: UILabel!
    weak var navigationController: UINavigationController?
    var onBackBtnTapped : (() -> Void)?
    
    override init(frame:CGRect){
        super.init(frame: frame)
        commitInt()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInt()
    }
    
    private func commitInt(){
        Bundle.main.loadNibNamed("HeaderView", owner: self,options: nil)
        addSubview(containtView)
        containtView.frame = self.bounds
        containtView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        rightBox.layer.cornerRadius = 20
        
        leftBackBtn.addTarget(self, action: #selector(onLeftBackClick), for: .touchUpInside)
        
      
    }
  
    @objc  private func onLeftBackClick () -> Void{
       
//        if let onBackBtnTappedI = onBackBtnTapped?() {
//            return onBackBtnTapped?()
//        }
        guard let navigationController = navigationController else {
                  return
              }
              
              // Trigger the pop action
              navigationController.popViewController(animated: true)
              
              // Call the custom back button handler if provided
          
            
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
