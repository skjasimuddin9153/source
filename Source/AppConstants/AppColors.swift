//
//  AppColors.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//


import UIKit

extension UIColor {
     
    @nonobjc class var accentColor: UIColor? {
        return UIColor(named:"AccentColor")
    }
    @nonobjc class var appDark: UIColor? {
        return UIColor(named:"appDark")
    }
    @nonobjc class var appLight: UIColor? {
        return UIColor(named:"appLight")
    }
    @nonobjc class var darkAccent: UIColor? {
        return UIColor(named:"darkAccent")
    }
    @nonobjc class var lightBlue: UIColor? {
        return UIColor(named:"lightBlue")
    }
    @nonobjc class var lightGrey: UIColor? {
        return UIColor(named:"lightGrey")
    }
    @nonobjc class var pickerGrey: UIColor? {
        return UIColor(named:"pickerGrey")
    }
    @nonobjc class var veryLightAccent: UIColor? {
        return UIColor(named:"veryLightAccent")
    }
    @nonobjc class var pinBG: UIColor? {
        return UIColor(named:"pinBG")
    }
    @nonobjc class var placeholderColor: UIColor? {
        return UIColor(named:"placeholderColor")
    }
    @nonobjc class var grayDot: UIColor? {
        return UIColor(named:"grayDot")
    }
    
    
}
