//
//  SplashScreenAPIDataManager.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias UserDetailsResponseCompletion = (Result<APIResponse<DetailsResponseModel>, Error>) -> Void
class SplashScreenAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func getUseDetails(completion:@escaping UserDetailsResponseCompletion){
        makeAPICall(to: DoctorNurseEndpoints.userDetails, completion: completion)
    }
}
