//
//  OtpTextFields.swift
//  Source
//
//  Created by Techwens Software on 29/08/23.
//

import UIKit

class OTPField: UITextField {
  weak var previousTextField: OTPField?
  weak var nextTextField: OTPField?
  override public func deleteBackward(){
    text = ""
    previousTextField?.becomeFirstResponder()
   }
}
