//
//  PickerViewVC.swift
//  UAE10
//
//  Created by Techwens on 19/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

protocol SelectInfoFromPickerDelegate : AnyObject {
    func didInfoSelected(value : String,index : Int)
    func noSelection()
}

class PickerViewVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblPickerTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    lazy var viewModel = PickerViewViewModel()
    weak var delegate: SelectInfoFromPickerDelegate?
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfSearch.setupSourceTextField(withPlaceholder: "Search")
        viewModel.dataList = viewModel.dependency?.dataList ?? []
        viewModel.originaldataList = viewModel.dependency?.dataList ?? []
        tableView.dataSource = self
        tableView.delegate = self
        lblPickerTitle.text = viewModel.dependency?.title
        tfSearch.addTarget(self, action: #selector(textUpdate), for: .editingChanged)
        setupViews()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @objc func textUpdate(textfield: UITextField){
        // filter name
        
        if let searchText = textfield.text{
            if searchText != "" {
                DispatchQueue.main.async { [unowned self] in
                    viewModel.dataList = viewModel.originaldataList.filter({$0.lowercased().contains(searchText.lowercased()) })
                    tableView.reloadData()
                }
            } else {
                DispatchQueue.main.async { [unowned self] in
                    viewModel.dataList = viewModel.originaldataList
                    tableView.reloadData()
                }
            }
            
        }
        else {
            viewModel.dataList = viewModel.originaldataList
            tableView.reloadData()
        }
    }
    func setupViews () {
           containerView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
           containerView.layer.cornerRadius = 30
       }
       @IBAction func crossButtonClicked(_ sender: UIButton) {
           dismiss(animated: true, completion: nil)
           delegate?.noSelection()
       }
}

// MARK: - Load from storyboard with dependency
extension PickerViewVC {
    class func loadFromXIB(withDependency dependency: PickerViewDependency? = nil) -> PickerViewVC? {
        let storyboard = UIStoryboard(name: "PickerView", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PickerViewVC") as? PickerViewVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - PickerViewAPIResponseDelegate
extension PickerViewVC: PickerViewAPIResponseDelegate {
}


extension PickerViewVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "mediaPickerTVC") as? MediaPickerTVC else { return UITableViewCell() }
        
        let item = viewModel.dataList[indexPath.row]
        cell.lblTitle.text = item
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true)
        let item = viewModel.dataList[indexPath.row]
        delegate?.didInfoSelected(value: item , index: viewModel.originaldataList.firstIndex(of: item) ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
}
