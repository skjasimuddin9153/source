//
//  LoginViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
enum FindType {
    case email
    case phone
}

protocol LoginAPIResponseDelegate: class {
    func handleFindUserResponse(data: FindUserData)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}


final class LoginViewModel {
  
    

    enum Event {
        case loading
        case stopLoading
        case dataLoaded
        case error(Error)
        case data(LoginResponse)
    }

    var eventHandler: ((Event) -> Void)?

    // Your existing login and other functions here...

    func login(countryCode: String, mobilePhone: String) {
        self.eventHandler?(.loading) // Notify the loading state
        
        let loginData: [String: Any] = [
            "country_code": countryCode,
            "phone": mobilePhone
        ]
        
        APIManager.shared.request(
            method: .post,
            endpoint:AuthEndpoints.login,
            parameters: loginData
        ) { result in
            switch result {
            case .success(let data):
                do {
                    let json: LoginResponse = try JSONDecoder().decode(LoginResponse.self, from: data)
                    print("Success: \(json)")
                    KeychainManager.shared.authToken = json.auth_key
                    self.eventHandler?(.data(json)) // Notify success and pass data
                    self.eventHandler?(.dataLoaded) // Notify data loaded
                } catch {
                    print("Error parsing JSON: \(error)")
                    self.eventHandler?(.error(error))//Notify error
                }
            case .failure(let error):
                self.eventHandler?(.error(error))
            }
        }
    }
}

