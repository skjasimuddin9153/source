//
//  CodeVerificationVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class CodeVerificationVC: UIViewController {
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblCounter: UILabel!
    @IBOutlet weak var pinView: UIView!
    // MARK: Instance variables
    var timer: Timer?
	lazy var viewModel = CodeVerificationViewModel()
    var otpTextField: OTPStackView?
    var otpCounter = 0 {
        didSet {
            lblCounter.text = "In \(timeInMinSec(fromSeconds: otpCounter))"
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        hideKeyboardWhenTappedAround()
//        setupPinView()
        btnResend.isEnabled = false
        otpCounter = viewModel.dependency?.otp_valid_for ?? 0
       timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
        view.endEditing(true)

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    
    @IBAction func tappedResendBtn(_ sender: UIButton) {
        
        switch viewModel.dependency?.flow {
        case .registration:
            guard let id = viewModel.dependency?.prospect_id else {
                showSnackbar(withMessage: "prospect_id not found")
                return
            }
            viewModel.resubmit(id: id)
        case .login:
            guard let emailPhone = viewModel.dependency?.email else {
                showSnackbar(withMessage: "Invalid Email/Phone")
                return
            }
            switch getInputType() {
            case .email:
                viewModel.resendForgotPassword(type: .email, withEmail: emailPhone)
            case .phone:
                viewModel.resendForgotPassword(type: .phone, withEmail: emailPhone)
            default:
               showSnackbar(withMessage: "Invalid Input Type")
            }
        default:
            showSnackbar(withMessage: "Invalid Flow")
        }
     
    }
    func timeInMinSec(fromSeconds seconds: Int) -> String {
        let min = seconds / 60 % 60
        let sec = seconds % 60
        return String(format: "%02i : %02i", min,sec)
    }
    @objc func fireTimer() {
            if otpCounter == 0 {
                timer?.invalidate()
                btnResend.isEnabled = true
            } else {
                otpCounter = otpCounter - 1
            }
        }
    func getInputType() -> FindType? {
        if viewModel.dependency!.email!.validateWith(regex: .email) {
            return .email
        }
        if viewModel.dependency!.email!.validateWith(regex: .phoneNo) {
            return .phone
        }
        return nil
    }
}

// MARK: - Load from storyboard with dependency
extension CodeVerificationVC {
    class func loadFromXIB(withDependency dependency: CodeVerificationDependency? = nil) -> CodeVerificationVC? {
        let storyboard = UIStoryboard(name: "CodeVerification", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CodeVerificationVC") as? CodeVerificationVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - CodeVerificationAPIResponseDelegate
extension CodeVerificationVC: CodeVerificationAPIResponseDelegate {
    func handleSendOTPForgotPasswordResponse(message: String) {
        showSnackbar(withMessage: message)
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        let dependency = ResetPinDependency(email: viewModel.dependency?.email ?? "")
        guard let vc = ResetPinVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleReSendOTPResponse(message: String) {
        showSnackbar(withMessage: message)
        btnResend.isEnabled = false
        otpCounter = 210
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(fireTimer), userInfo: nil, repeats: true)
    }
    
    func handleSendOTPResponse(message: String) {
        showSnackbar(withMessage: message)
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        switch viewModel.dependency?.user {
        case .doctor,.nurse:
            let dependency = VerificationDetailsDependency(prospect_id: viewModel.dependency?.prospect_id)
            guard let vc = VerificationDetailsVC.loadFromXIB(withDependency: dependency) else { return }
            navigationController?.pushViewController(vc, animated: true)
        case .hospital:
            let depenedency = OwnerVerificationDependency(prospect_id: viewModel.dependency?.prospect_id)
            guard let vc = OwnerVerificationVC.loadFromXIB(withDependency: depenedency) else { return }
            navigationController?.pushViewController(vc, animated: true)

        default:
            showSnackbar(withMessage: "User Type error")
        }
        
        
        
    }
    
    func handleAPIError(_ error: Error) {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
}

//extension CodeVerificationVC {
//    func setupPinView(){
//        let otpFields = OTPStackView(frame: pinView.frame, fields: 6)
//         pinView.addSubview(otpFields)
//         NSLayoutConstraint.activate([
//             otpFields.topAnchor.constraint(equalTo: pinView.topAnchor),
//             otpFields.leadingAnchor.constraint(equalTo: pinView.leadingAnchor),
//             otpFields.trailingAnchor.constraint(equalTo: pinView.trailingAnchor),
//             otpFields.bottomAnchor.constraint(equalTo: pinView.bottomAnchor)
//         ])
//        self.otpTextField = otpFields
//        self.otpTextField?.delegate = self
//
//    }
//}

//extension CodeVerificationVC: OTPDelegate {
//    func didChangeOTP(otp: String) {
//        <#code#>
//    }
//
//
//    func didChangeValidity(isValid: Bool) {
//        if isValid {
//            switch viewModel.dependency?.flow {
//            case .registration:
//                    guard let otp = otpTextField?.getOTP() else {
//                        showSnackbar(withMessage: "Unable to get OTP")
//                        return
//                    }
//                    guard let id = viewModel.dependency?.prospect_id else {
//                        showSnackbar(withMessage: "prospect_id not found")
//                        return
//                    }
//                    view.showLoader()
//                    viewModel.submit(otp: otp, id: id)
//
//
//            case .login:
//                guard let otp = otpTextField?.getOTP() else {
//                    showSnackbar(withMessage: "Unable to get OTP")
//                    return
//                }
//                guard let id = viewModel.dependency?.email else {
//                    showSnackbar(withMessage: "email/phone not found")
//                    return
//                }
//                view.showLoader()
//                viewModel.submitForgotPassword(otp: otp, emailId: id)
//            default:
//                ()
//            }
//
//        } else {
//            Log.i("waiting for otp completion")
//        }
//    }
//}
