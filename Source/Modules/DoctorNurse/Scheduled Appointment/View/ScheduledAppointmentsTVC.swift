//
//  ScheduledAppointmentsTVC.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//

import UIKit

class ScheduledAppointmentsTVC: UITableViewCell {
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblSpecaility: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    var data: Booking_list? {
        didSet {
            lblHospitalName.text = data?.booking_received_by?.hospital_name ?? ""
            lblSpecaility.text = data?.slot_speciality?.rawValue ?? ""
            lblTime.text = (data?.slot_start_time ?? "") + " - " + (data?.slot_end_time ?? "")
            
            let formatter  = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
            let bookingDate = formatter.date(from: data?.booking_start_time ?? "")
            formatter.dateFormat = "dd-MMM-yyyy"
            let formattedData = formatter.string(from: bookingDate!)
            lblDate.text = formattedData
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
