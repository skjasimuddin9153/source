//
//  ForgotPinVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import TTGSnackbar

class ForgotPinVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var btnSubmit: BorderButton!
    @IBOutlet var tfuserName: UITextField!
    @IBOutlet var ivThick: UIImageView!
    @IBOutlet weak var tfEmailPhone: UITextField!
    lazy var viewModel = ForgotPinViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
      
        viewModel.apiResponseDelegate = self
       

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedUseAccountBtn(_ sender: UIButton) {
        
        print ("tab jasim  ")
        
        guard let vc = LoginVC.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
   
    }
    
    @IBAction func onGuestBtnClicked(_ sender: Any) {
        guard let vc = DetailsViewController.loadFromXIB() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getInputType() -> FindType? {
        if tfEmailPhone.validateWith(regex: .email) {
            return .email
        }
        if tfEmailPhone.validateWith(regex: .phoneNo) {
            return .phone
        }
        return nil
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        // This method will be called when the text field's content changes
        // You can put your custom logic here
        
        print(textField.text,"text is here")
        
        if !textField.text!.hasPrefix("@") {
                   textField.text = "@" + (textField.text ?? "")
               }
        let username = textField.text ?? ""
        

       let isValid = viewModel.validateUsername(username)
        
        btnSubmit.isValid = isValid
        
        if (isValid){
            ivThick.isHidden = false
        }else{
            let snackbar = TTGSnackbar(message: "Invalid Username ", duration: .middle)
            snackbar.backgroundColor = .gray
            snackbar.show()
            ivThick.isHidden = true
        }
        
          
        
    }

    
}

// MARK: - Load from storyboard with dependency
extension ForgotPinVC {
    class func loadFromXIB(withDependency dependency: ForgotPinDependency? = nil) -> ForgotPinVC? {
        let storyboard = UIStoryboard(name: "ForgotPin", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ForgotPinVC") as? ForgotPinVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - ForgotPinAPIResponseDelegate
extension ForgotPinVC: ForgotPinAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
       // btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
      //  btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
        let dependency = CodeVerificationDependency(user: viewModel.dependency?.user ?? .doctor, flow: .login, prospect_id: nil, otp_valid_for: 120, email: tfEmailPhone.text!)
        guard let vc = CodeVerificationVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
