//
//  PostgraduateDetailsVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit


class PostgraduateDetailsVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfDegreeType: UITextField!
    @IBOutlet weak var tfCollegeName: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var tfRegistrationNo: UITextField!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var frontBtnView: UIView!
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var btnSkip: UIButton!
    lazy var viewModel = PostgraduateDetailsViewModel()
    var activeImagePicker: DocumentPage?
    var imagePicker = UIImagePickerController()
    var payloadData : [UploadPayloadData] = []
    var parameter : [String: String] = [:]
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfDegreeType.setupSourceTextField(withPlaceholder: "Degree Type")
        tfCollegeName.setupSourceTextField(withPlaceholder: "College Name")
        tfStartDate.setupSourceTextField(withPlaceholder: "Year Appeared")
        tfStartDate.setInputViewDatePicker(target: self, selector: #selector(tapDonetfStart(sender:datePicker1:)), maxDate: Date())
        tfStartDate.setRightViewButton(with: UIImage(named: "icCalendar")!)
        tfEndDate.setupSourceTextField(withPlaceholder: "Year Passed")
        tfEndDate.setInputViewDatePicker(target: self, selector: #selector(tapDonetfEnd(sender:datePicker1:)), maxDate: Date())
        tfEndDate.setRightViewButton(with: UIImage(named: "icCalendar")!)
        tfRegistrationNo.setupSourceTextField(withPlaceholder: "Registration Number")
        tfRegistrationNo.autocapitalizationType = .allCharacters
        frontBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .first
            welf.showPickerAlert()
        }
        backBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .second
            welf.showPickerAlert()
        }
    }
    @objc func tapDonetfStart(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfStartDate.inputView as? UIDatePicker { // 2.1
            self.tfStartDate.text = datePicker.date.toString(format: .custom("yyyy-MM-dd"))
        }
        self.tfStartDate.resignFirstResponder() // 2.5
    }
    @objc func tapDonetfEnd(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfEndDate.inputView as? UIDatePicker { // 2.1
            self.tfEndDate.text = datePicker.date.toString(format: .custom("yyyy-MM-dd"))
        }
        self.tfEndDate.resignFirstResponder() // 2.5
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSkipBtn(_ sender: UIButton) {
        
        payloadData = viewModel.dependency?.payloadData ?? []
        parameter["post_graduation_skipped"] = "1"
        parameter["post_graduation_course_name"] =  ""
        parameter["post_graduation_college_name"] = ""
        parameter["post_graduation_start_date"] = ""
        parameter["post_graduation_end_date"] = ""
        parameter["post_graduation_registration_number"] = ""
        print(parameter)
        print(payloadData)
        parameter.merge(dict: viewModel.dependency?.parameter ?? [:])
        let dependency = FellowshipDetailsDependency(prospect_id: viewModel.dependency?.prospect_id, payloadData: payloadData, parameter: parameter)
        guard let vc = FellowshipDetailsVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        
        guard let frontImage = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        guard let backImage = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        let pgFrontpayloadData = UploadPayloadData(data: frontImage, name: "post_graduation_registration_document_front_file", filename: "pgFrontImage.jpeg", mimeType: .jpeg)
        let pgBackpayloadData = UploadPayloadData(data: backImage, name: "post_graduation_registration_document_back_file", filename: "pgBackImage.jpeg", mimeType: .jpeg)
        payloadData.append(pgFrontpayloadData)
        payloadData.append(pgBackpayloadData)
        parameter["post_graduation_skipped"] = "0"
        parameter["post_graduation_course_name"] = tfDegreeType.text!
        parameter["post_graduation_college_name"] = tfCollegeName.text!
        parameter["post_graduation_start_date"] = tfStartDate.text!
        parameter["post_graduation_end_date"] = tfEndDate.text!
        parameter["post_graduation_registration_number"] = tfRegistrationNo.text!
        

        print(parameter)
        print(payloadData)
        payloadData.append(contentsOf: viewModel.dependency?.payloadData ?? [])
        parameter.merge(dict: viewModel.dependency?.parameter ?? [:])
        let dependency = FellowshipDetailsDependency(prospect_id: viewModel.dependency?.prospect_id, payloadData: payloadData, parameter: parameter)
        guard let vc = FellowshipDetailsVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    // MARK: Deinitialization
    
    func allFieldsValid() -> Bool {
        if (tfDegreeType.text?.isEmpty ?? true) || tfDegreeType.text == ""{
            showSnackbar(withMessage: "Degree Type is Invalid")
            tfDegreeType.shake()
            return false
        }
        if (tfCollegeName.text?.isEmpty ?? true) || tfCollegeName.text == ""{
            showSnackbar(withMessage: "College Name is Invalid")
            tfCollegeName.shake()
            return false
        }
        if  (tfRegistrationNo.text?.isEmpty ?? true) || tfRegistrationNo.text == "" {
            showSnackbar(withMessage: "Registration No is Invalid")
            tfRegistrationNo.shake()
            return false
        }
        
        if tfStartDate.text == "" {
            showSnackbar(withMessage: "Start date cannot be Empty")
            tfStartDate.shake()
            return false
        }
        
        if tfEndDate.text == "" {
            showSnackbar(withMessage: "End date cannot be Empty")
            tfEndDate.shake()
            return false
        }
        let endDate = tfEndDate.text?.stringToDate() ?? Date()
        let startDate = tfStartDate.text?.stringToDate() ?? Date()
        
        if relativeDate(startDate: startDate, endDate: endDate) {
            showSnackbar(withMessage: "Invalid Date Range")
            tfStartDate.shake()
            tfEndDate.shake()
            return false
        }
        
        guard let data1 = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Front Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data1) {
            showSnackbar(withMessage: "Image is too large")
            frontImageView.shake()
            return false
        }
        guard let data2 = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Back Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data2) {
            showSnackbar(withMessage: "Image is too large")
            backImageView.shake()
            return false
        }
        return true
    }
    
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension PostgraduateDetailsVC {
    class func loadFromXIB(withDependency dependency: PostgraduateDetailsDependency? = nil) -> PostgraduateDetailsVC? {
        let storyboard = UIStoryboard(name: "PostgraduateDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PostgraduateDetailsVC") as? PostgraduateDetailsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - PostgraduateDetailsAPIResponseDelegate
extension PostgraduateDetailsVC: PostgraduateDetailsAPIResponseDelegate {
}


extension PostgraduateDetailsVC {
    func openPicker(type: UIImagePickerController.SourceType) {
            imagePicker.sourceType = type
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .currentContext
            imagePicker.delegate = self
            present(imagePicker, animated: true)
        
        
    }
    
    func showPickerAlert(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ [self] _ in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                openPicker(type: .camera)
            }
            else{
                showSnackbar(withMessage: "No Camera Available On This Device")
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default , handler:{ [self] _ in
            openPicker(type: .photoLibrary)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ _ in
            
        }))
        
       present(alert, animated: true, completion: nil)
    }
}

extension PostgraduateDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        switch activeImagePicker {
        case .first:
            frontImageView.image = tempImage
        case .second:
            backImageView.image = tempImage
        default:
            ()
        }
        activeImagePicker = nil
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activeImagePicker = nil
        dismiss(animated: true, completion: nil)
    }
}
