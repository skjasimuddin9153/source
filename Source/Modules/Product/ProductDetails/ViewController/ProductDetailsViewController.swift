//
//  ProductDetailsViewController.swift
//  Source
//
//  Created by Techwens Software on 01/09/23.
//

import UIKit
import ImageSlideshow
import AlamofireImage


class ProductDetailsViewController: UIViewController {
 
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var productName: UILabel!
    @IBOutlet var productStatus: UILabel!
    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productDescription: UILabel!
    lazy var viewModel = ProductDetailsViewModel()
    var productId:String?
    var productDetails:ProductDetailsResponse?
    //
    var counter : Int = 1
    struct categoryObj {
        var label: String
        var subLabel: String
    }
    var category : [categoryObj] = [
        categoryObj(label: "Category", subLabel: "Flower"),
        categoryObj(label: "THC", subLabel: "28.47%"),
        categoryObj(label: "SKU", subLabel: "G52HT21"),
        categoryObj(label: "CBN", subLabel: "5.00%"),
    ]
    
    let localSource = [BundleImageSource(imageString: "productDemo"), BundleImageSource(imageString: "productDemo"), BundleImageSource(imageString: "productDemo"), BundleImageSource(imageString: "productDemo")]
//
//    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        return .portrait
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
//        imageSlider()
        // Do any additional setup after loading the view.
//        let cartGesture = UITapGestureRecognizer(target: self, action: #selector(cartClicked))
//        addCart.addGestureRecognizer(cartGesture)
//        addCart.isUserInteractionEnabled = true
     
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.getProductDetails(productId: productId ?? "")
        observeLoginViewModelEvents()
    }
    
    func observeLoginViewModelEvents() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }

            switch event {
            case .loading:
                // Handle loading state (e.g., show activity indicator)
              
                print("Loading...")
            case .stopLoading:
                // Handle stop loading state (e.g., hide activity indicator)
              
                print("Stop loading...")
            case .data(let product):
                print("Data loaded...")
                DispatchQueue.main.async {
                  
                    self.productDetails = product
                    print("productDetails111 \(product)")
                  
                    self.setProductDetails()
                }
            case .dataLoaded:
                // Handle data loaded event (e.g., update UI)
                print("Data loaded event")
            case .error(let error):
                // Handle error (e.g., show an error message)
                print("Error: \(error)")
            }
        }
    }
    
    private func setProductDetails() {
    
        self.productName.text = self.productDetails?.data.name
        self.productStatus.text = self.productDetails?.data.Strain.name
        self.productDescription.text = self.productDetails?.data.about
    
        self.productImage.setImage(with: self.productDetails?.data.imageUrl ?? "")
           
        
    }

//    private func setProductDetails(){
//        productName.text = productDetails?.data.name
//        productStatus.text = productDetails?.data.Strain.name
//        productDescription.text = productDetails?.data.about
//        if let imageUrl = URL(string: productDetails?.data.imageUrl ?? "") {
//            productImage.af.setImage(withURL: imageUrl)
//
//        } else {
//            productImage.image = nil
//        }
//
//
//    }
    @IBAction func onBrandBtnClicked(_ sender: Any) {
        guard let vc = BrandDetailsViewController.loadFromXIB() else {return}
        
        
        self.present(vc, animated: true)
    }
    
    @objc func cartClicked (){
        guard let vc = CartViewController.loadFromXIB() else {return}
      
    }
    
    @IBAction func onAddTocartClicked(_ sender: Any) {
        cartdataManager.shared.data.append(CartProduct(id: 1, productTitle: "product 1", productSubTitle: "subtitle", productImage: UIImage(named: "k1 og hyperzoom 1")!, productPrice: "$123.56", itemCount: 1))
    }
    
//     func imageSlider(){
//        slider.slideshowInterval = 5.0
//        slider.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
//        slider.contentScaleMode = UIViewContentMode.scaleAspectFill
//
//
//
//               // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
//        slider.activityIndicator = DefaultActivityIndicator()
//        slider.delegate = self
//         let pageControl = UIPageControl()
//         pageControl.currentPageIndicatorTintColor = UIColor.black
//         pageControl.pageIndicatorTintColor = UIColor.lightGray
//         slider.pageIndicator = pageControl
////         slider.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 30))
//               // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
//        slider.setImageInputs(localSource)
//
//               let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
//        slider.addGestureRecognizer(recognizer)
//    }
//    @objc func didTap() {
//          let fullScreenController = slider.presentFullScreenController(from: self)
          // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
//          fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
//      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
 
   
}
//extension ProductDetailsViewController: ImageSlideshowDelegate {
//    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
//        print("current page:", page)
//    }
//}

extension ProductDetailsViewController {
    class func loadFromXIB() -> ProductDetailsViewController? {
        let storyboard = UIStoryboard(name: "ProductDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as? ProductDetailsViewController else {
            return nil
        }
//        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// collectionView

extension ProductDetailsViewController:UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCollectionViewCell
        cell.calegoryLbl.text = category[indexPath.row].label
        cell.categorySubLbl.text = category[indexPath.row].subLabel
        return cell
    }
    
    
}
