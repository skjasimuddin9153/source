//
//  HomeFilterVC.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class HomeFilterVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = HomeFilterViewModel()
    @IBOutlet weak var lblTimeSlot: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnEmergency: UIButton!
    @IBOutlet weak var btnICU: UIButton!
    @IBOutlet var specaialityBtnGroup: [UIButton]!
    var selectedSlotType: SlotType? {
        didSet {
            deselectAllBtn()
            switch selectedSlotType {
            case .emergency:
                selectSpecialityBtn(btnEmergency)
            case .icu:
                selectSpecialityBtn(btnICU)
            default:
                ()
            }
            selectedTypes(user: selectedUSerType, speciality: selectedSlotType)
        }
    }
    
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnNurse: UIButton!
    @IBOutlet var userBtnGroup: [UIButton]!
    var selectedUSerType: UserType? {
        didSet {
            deselectAllUserBtn()
            switch selectedUSerType {
            case .doctor:
                selectUserBtn(btnDoc)
            case .nurse:
                selectUserBtn(btnNurse)
            default:
                ()
            }
            selectedTypes(user: selectedUSerType, speciality: selectedSlotType)
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        viewModel.getSlotsList()
        selectedTypes(user: selectedUSerType, speciality: selectedSlotType)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    
    func deselectAllBtn() {
        specaialityBtnGroup.forEach({$0.backgroundColor = .pickerGrey ?? .gray})
    }
    func selectSpecialityBtn(_ sender: UIButton){
        sender.backgroundColor = .accentColor ?? .yellow
    }
    @IBAction func tappedEmergencyBtn(_ sender: UIButton) {
        selectedSlotType = .emergency
    }
    @IBAction func tappedICUBtn(_ sender: UIButton) {
        selectedSlotType = .icu
    }
    
    func deselectAllUserBtn() {
        userBtnGroup.forEach({$0.backgroundColor = .pickerGrey ?? .gray})
    }
    func selectUserBtn(_ sender: UIButton){
        sender.backgroundColor = .accentColor ?? .yellow
    }
    @IBAction func tappedDocBtn(_ sender: UIButton) {
        selectedUSerType = .doctor
    }
    @IBAction func tappedNurseBtn(_ sender: UIButton) {
        selectedUSerType = .nurse
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func selectedTypes(user: UserType? , speciality: SlotType?) {
        if user == nil || speciality == nil {
            collectionView.isHidden  = true
            lblTimeSlot.isHidden = true
        } else {
            collectionView.isHidden  = false
            lblTimeSlot.isHidden = false
            viewModel.slotList = viewModel.fullSlotList.filter({$0.type == user!.rawValue})
            viewModel.slotList = viewModel.slotList.filter({$0.speciality == speciality!.rawValue})
            collectionView.reloadData()
        }
        
    }
}

// MARK: - Load from storyboard with dependency
extension HomeFilterVC {
    class func loadFromXIB(withDependency dependency: HomeFilterDependency? = nil) -> HomeFilterVC? {
        let storyboard = UIStoryboard(name: "HomeFilter", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "HomeFilterVC") as? HomeFilterVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - HomeFilterAPIResponseDelegate
extension HomeFilterVC: HomeFilterAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleListAPI() {
        collectionView.reloadData()
    }
    
}

extension HomeFilterVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.slotList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingSlotCVC", for: indexPath) as? BookingSlotCVC
        else {
            return UICollectionViewCell()
        }
        let data = viewModel.slotList[indexPath.row]
        cell.lblTimeSlot.text = (data.start_time ?? "") + " to " + (data.end_time ?? "")
        return cell
    }
}
