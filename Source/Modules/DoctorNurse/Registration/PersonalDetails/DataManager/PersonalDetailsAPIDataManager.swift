//
//  PersonalDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

typealias RegStepOneResponseCompletion = (Result<APIResponse<RegisterStepOneData>, Error>) -> Void

class PersonalDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping RegStepOneResponseCompletion){
        makeAPICall(to: AuthEndpoints.register, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
}
