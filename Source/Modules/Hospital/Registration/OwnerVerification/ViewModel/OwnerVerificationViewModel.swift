//
//  OwnerVerificationViewModel.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol OwnerVerificationAPIResponseDelegate: class {
    func handleRegisterationStep2Response(message:String)
    func handleAPIError(_ error: Error)
}

class OwnerVerificationViewModel {
    weak var apiResponseDelegate: OwnerVerificationAPIResponseDelegate?
    lazy var localDataManager = OwnerVerificationLocalDataManager()
    lazy var apiDataManager = OwnerVerificationAPIDataManager()
    
    var dependency: OwnerVerificationDependency?
    let documents: [DocTypes] = [.adhar,.driving_license,.electoral_id,.passport]
    
    init() {
    }
    // Data fetch service methods goes here
    func registerStepTwo(with imageData: Data, prospect_id: Int, panNumber: String, docType: DocTypes, docNumber: String) {
        let payloadData = UploadPayloadData(data: imageData, name: "uploaded_document_file", filename: "image.jpeg", mimeType: .jpeg)
        let parameters = [
            "prospect_id":"\(prospect_id)",
            "step":"2",
            "pan_number": panNumber,
            "uploaded_document_type": docType.rawValue,
            "uploaded_document_number": docNumber
        ]
        apiDataManager.registerAPI(with: [payloadData], parameters: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRegisterationStep2Response(message: response.message)

            }
            
        })
    }
}
