import UIKit
import NVActivityIndicatorView

@IBDesignable
class AppSolidButton: UIButton, SpinnerButton {

    @IBInspectable var bgColor: UIColor = UIColor.appDark ?? .black
    @IBInspectable var normalBorderColor: UIColor = .clear
    @IBInspectable var selectedBorderColor: UIColor = .clear
    var titleFont = UIFont.poppinsRegular(ofSize: 15.0)
    var activityIndicator: NVActivityIndicatorView?
    @IBInspectable var spinnerColor: UIColor = .accentColor ?? .white
    internal var cover: UIView?
    var tempTitle : String?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    func setupViews() {
        
        tempTitle = title(for: .normal)
        layer.cornerRadius =  5
        setTitleColor(self.titleLabel?.textColor, for: .normal)
        setTitleColor(self.titleLabel?.textColor, for: .highlighted)
        setTitleColor(self.titleLabel?.textColor, for: .disabled)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        switch state {
        case .normal:
            backgroundColor = bgColor
            layer.borderColor = normalBorderColor.cgColor
        case .highlighted:
            backgroundColor = bgColor
            layer.borderColor = normalBorderColor.cgColor
        case .disabled:
            backgroundColor = .appLight
            layer.borderColor = normalBorderColor.withAlphaComponent(0.5).cgColor
        case .selected:
            backgroundColor = bgColor
            layer.borderColor = selectedBorderColor.cgColor
        default:
            backgroundColor = bgColor
            layer.borderColor = normalBorderColor.cgColor
        }
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupViews()
    }

    @IBInspectable var shadowColor: UIColor? {
        get {
            guard let shadowColor = layer.shadowColor else {
                return nil
            }
            return UIColor(cgColor: shadowColor)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }

    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }

    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }

    @IBInspectable var shadowOffset : CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    func showSpinner() {
        titleLabel?.isHidden = true
        isUserInteractionEnabled = false
        if activityIndicator == nil {
            activityIndicator = createActivityIndicator()
        }
        showSpinning()
    }
    
    func hideSpinner() {
        titleLabel?.isHidden = false
        cover?.removeFromSuperview()
        cover = nil
        isUserInteractionEnabled = true
        activityIndicator?.stopAnimating()
    }
}
