//
//  FellowshipDetailsVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit


class FellowshipDetailsVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var btnSend: AppSolidButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var tfCollegeName: UITextField!
    @IBOutlet weak var tfStartDate: UITextField!
    @IBOutlet weak var tfEndDate: UITextField!
    @IBOutlet weak var tfRegistrationNo: UITextField!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var frontImageView: UIImageView!
    @IBOutlet weak var frontBtnView: UIView!
    @IBOutlet weak var backBtnView: UIView!
	lazy var viewModel = FellowshipDetailsViewModel()
    var activeImagePicker: DocumentPage?
    var imagePicker = UIImagePickerController()
    var payloadData : [UploadPayloadData] = []
    var parameter : [String: String] = [:]
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfCollegeName.setupSourceTextField(withPlaceholder: "Course Name")
        tfStartDate.setupSourceTextField(withPlaceholder: "Year Appeared")
        tfStartDate.setInputViewDatePicker(target: self, selector: #selector(tapDonetfStart(sender:datePicker1:)), maxDate: Date())
        tfStartDate.setRightViewButton(with: UIImage(named: "icCalendar")!)
        tfEndDate.setupSourceTextField(withPlaceholder: "Year Passed")
        tfEndDate.setInputViewDatePicker(target: self, selector: #selector(tapDonetfEnd(sender:datePicker1:)), maxDate: Date())
        tfEndDate.setRightViewButton(with: UIImage(named: "icCalendar")!)
        tfRegistrationNo.setupSourceTextField(withPlaceholder: "Registration Number")
        tfRegistrationNo.autocapitalizationType = .allCharacters
        
        frontBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .first
            welf.showPickerAlert()
        }
        backBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.activeImagePicker = .second
            welf.showPickerAlert()
        }
    }
    @objc func tapDonetfStart(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfStartDate.inputView as? UIDatePicker { // 2.1
            self.tfStartDate.text = datePicker.date.toString(format: .custom("yyyy-MM-dd"))
        }
        self.tfStartDate.resignFirstResponder() // 2.5
    }
    @objc func tapDonetfEnd(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfEndDate.inputView as? UIDatePicker { // 2.1
            self.tfEndDate.text = datePicker.date.toString(format: .custom("yyyy-MM-dd"))
        }
        self.tfEndDate.resignFirstResponder() // 2.5
    }
    
    func allFieldsValid() -> Bool {
        if (tfCollegeName.text?.isEmpty ?? true) || tfCollegeName.text == ""{
            showSnackbar(withMessage: "College Name is Invalid")
            tfCollegeName.shake()
            return false
        }
        if  (tfRegistrationNo.text?.isEmpty ?? true) || tfRegistrationNo.text == "" {
            showSnackbar(withMessage: "Registration No is Invalid")
            tfRegistrationNo.shake()
            return false
        }
        
        if tfStartDate.text == "" {
            showSnackbar(withMessage: "Start date cannot be Empty")
            tfStartDate.shake()
            return false
        }
        
        if tfEndDate.text == "" {
            showSnackbar(withMessage: "End date cannot be Empty")
            tfEndDate.shake()
            return false
        }
        let endDate = tfEndDate.text?.stringToDate() ?? Date()
        let startDate = tfStartDate.text?.stringToDate() ?? Date()
        
        if relativeDate(startDate: startDate, endDate: endDate) {
            showSnackbar(withMessage: "Invalid Date Range")
            tfStartDate.shake()
            tfEndDate.shake()
            return false
        }
        
        guard let data1 = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Front Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data1) {
            showSnackbar(withMessage: "Image is too large")
            frontImageView.shake()
            return false
        }
        guard let data2 = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Back Side of the Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data2) {
            showSnackbar(withMessage: "Image is too large")
            backImageView.shake()
            return false
        }
        return true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        guard let frontImage = frontImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        guard let backImage = backImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        let pgFrontpayloadData = UploadPayloadData(data: frontImage, name: "certification_registration_document_front_file", filename: "cFrontImage.jpeg", mimeType: .jpeg)
        let pgBackpayloadData = UploadPayloadData(data: backImage, name: "certification_registration_document_back_file", filename: "cBackImage.jpeg", mimeType: .jpeg)
        payloadData.append(pgFrontpayloadData)
        payloadData.append(pgBackpayloadData)
        parameter["certification_skipped"] = "0"
        parameter["certification_college_name"] = tfCollegeName.text!
        parameter["certification_start_date"] = tfStartDate.text!
        parameter["certification_end_date"] = tfEndDate.text!
        parameter["certification_registration_number"] = tfRegistrationNo.text!
        payloadData.append(contentsOf: viewModel.dependency?.payloadData ?? [])
        parameter.merge(dict: viewModel.dependency?.parameter ?? [:])
        print(parameter)
        print(payloadData)
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.registerStepThree(with: payloadData, parameter: &parameter, prospect_id: viewModel.dependency!.prospect_id!)
        
        
    }
    @IBAction func tappedSkipBtn(_ sender: UIButton) {
        payloadData = viewModel.dependency?.payloadData ?? []
        parameter["certification_skipped"] = "1"
        parameter["certification_college_name"] = ""
        parameter["certification_start_date"] = ""
        parameter["certification_end_date"] = ""
        parameter["certification_registration_number"] = ""
        parameter.merge(dict: viewModel.dependency?.parameter ?? [:])
        print(parameter)
        print(payloadData)
        view.showLoader()
        view.isUserInteractionEnabled = false
        viewModel.registerStepThree(with: payloadData, parameter: &parameter, prospect_id: viewModel.dependency!.prospect_id!)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension FellowshipDetailsVC {
    class func loadFromXIB(withDependency dependency: FellowshipDetailsDependency? = nil) -> FellowshipDetailsVC? {
        let storyboard = UIStoryboard(name: "FellowshipDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "FellowshipDetailsVC") as? FellowshipDetailsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - FellowshipDetailsAPIResponseDelegate
extension FellowshipDetailsVC: FellowshipDetailsAPIResponseDelegate {
    func handleRegisterResponse() {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        let dependency = BankDetailsDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = BankDetailsVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
    
}

extension FellowshipDetailsVC {
    func openPicker(type: UIImagePickerController.SourceType) {
            imagePicker.sourceType = type
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .currentContext
            imagePicker.delegate = self
            present(imagePicker, animated: true)
        
        
    }
    
    func showPickerAlert(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ [self] _ in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                openPicker(type: .camera)
            }
            else{
                showSnackbar(withMessage: "No Camera Available On This Device")
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default , handler:{ [self] _ in
            openPicker(type: .photoLibrary)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ _ in
            
        }))
        
       present(alert, animated: true, completion: nil)
    }
}

extension FellowshipDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        switch activeImagePicker {
        case .first:
            frontImageView.image = tempImage
        case .second:
            backImageView.image = tempImage
        default:
            ()
        }
        activeImagePicker = nil
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        activeImagePicker = nil
        dismiss(animated: true, completion: nil)
    }
}
