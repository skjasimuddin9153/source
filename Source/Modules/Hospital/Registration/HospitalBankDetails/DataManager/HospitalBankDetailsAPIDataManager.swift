//
//  HospitalBankDetailsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class HospitalBankDetailsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func registerAPI(with parameter: [String: Any] , completion:@escaping EmptyAPIResponseCompletion){
        makeAPICall(to: AuthEndpoints.hospitalRegister, withParameters: parameter, ofType: .httpBody, completion: completion)

    }
    func getBankAPI( completion:@escaping BankAPIResponseCompletion){
        makeAPICallForListResponse(to: UtiltiyEndpoints.bankList, completion: completion)

    }
}
