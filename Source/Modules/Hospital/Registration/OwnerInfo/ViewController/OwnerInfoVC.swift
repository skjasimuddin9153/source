//
//  OwnerInfoVC.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class OwnerInfoVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfFirstName: UITextField!
    @IBOutlet weak var tfLastName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPhone: UITextField!
    @IBOutlet weak var btnSend: AppSolidButton!
	lazy var viewModel = OwnerInfoViewModel()
    var parameter : [String: String] = [:]
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tfFirstName.setupSourceTextField(withPlaceholder: "First Name")
        tfLastName.setupSourceTextField(withPlaceholder: "Last Name")
        tfEmail.setupSourceTextField(withPlaceholder: "Email")
        tfPhone.keyboardType = .emailAddress
        tfPhone.setupSourceTextField(withPlaceholder: "Phone No")
        tfPhone.keyboardType = .phonePad
        tfPhone.delegate = self
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        
        if !allFieldsValid() {
           return
        }
        parameter["step"] = "1"
        parameter["user_type"] = UserType.hospital.rawValue
        parameter["first_name"] = tfFirstName.text!
        parameter["last_name"] = tfLastName.text!
        parameter["middle_name"] = ""
        parameter["email"] = tfEmail.text!
        parameter["gender"] = "M"
        parameter["phone_code"] = "91"
        parameter["phone_number"] = tfPhone.text!
        parameter["ip_address"] = ""
        parameter["hospital_address_line_1"] =  ""
        parameter["hospital_address_line_2"] =  ""
        parameter["hospital_latitude"] =  ""
        parameter["hospital_longitude"] =  ""
        parameter["hospital_state" ] = ""
        parameter["hospital_zip_code"] = "700005"
        parameter["number_of_emergency_beds"] = "100"
        parameter["number_of_icu_units"] =  "10"
        parameter["number_of_icu_beds"] =  "100"
        parameter.merge(dict: viewModel.dependency?.parameter ?? [:])
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.hospitalRegisterStepOne(parameter: parameter)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    func allFieldsValid() -> Bool {
        if !tfFirstName.validateWith(regex: .name) {
            showSnackbar(withMessage: TextFieldValidationRegex.name.errorMessage)
            return false
        }
        if !tfLastName.validateWith(regex: .name) {
            showSnackbar(withMessage: TextFieldValidationRegex.name.errorMessage)
            return false
        }
        if !tfEmail.validateWith(regex: .email) {
            showSnackbar(withMessage: TextFieldValidationRegex.email.errorMessage)
            return false
        }
        if !tfPhone.validateWith(regex: .phoneNo) {
            showSnackbar(withMessage: TextFieldValidationRegex.phoneNo.errorMessage)
            return false
        }
        return true
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension OwnerInfoVC {
    class func loadFromXIB(withDependency dependency: OwnerInfoDependency? = nil) -> OwnerInfoVC? {
        let storyboard = UIStoryboard(name: "OwnerInfo", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "OwnerInfoVC") as? OwnerInfoVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - OwnerInfoAPIResponseDelegate
extension OwnerInfoVC: OwnerInfoAPIResponseDelegate {
    func handleRegisterResponse(data: HospitalRegisterDataModel) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        let dependency = CodeVerificationDependency(user: .hospital, flow: .registration, prospect_id: data.prospect_id, otp_valid_for: data.otp_valid_for)
        guard let vc = CodeVerificationVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
    
}

extension OwnerInfoVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)

        return newString.count <= maxLength
    }
}
