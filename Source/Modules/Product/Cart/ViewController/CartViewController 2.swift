//
//  CartViewController.swift
//  Source
//
//  Created by Techwens Software on 01/09/23.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet var tavleView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tavleView.separatorStyle = .none

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backbtnClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension CartViewController:UITableViewDataSource,UITabBarDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CartTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! CartTableViewCell
        return cell
    }
    
    
}
extension CartViewController {
    class func loadFromXIB() -> CartViewController? {
        let storyboard = UIStoryboard(name: "Cart", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CartViewController") as? CartViewController else {
            return nil
        }
        return viewController
    }
}
