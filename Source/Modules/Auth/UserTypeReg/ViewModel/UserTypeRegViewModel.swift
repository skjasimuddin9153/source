//
//  UserTypeRegViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol UserTypeRegAPIResponseDelegate: AnyObject {
}

class UserTypeRegViewModel {
    weak var apiResponseDelegate: UserTypeRegAPIResponseDelegate?
    lazy var localDataManager = UserTypeRegLocalDataManager()
    lazy var apiDataManager = UserTypeRegAPIDataManager()
    
    var dependency: UserTypeRegDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
