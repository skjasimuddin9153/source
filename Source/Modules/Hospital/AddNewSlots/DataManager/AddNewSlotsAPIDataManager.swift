//
//  AddNewSlotsAPIDataManager.swift
//  Source
//
//  Created by Techwens on 24/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

class AddNewSlotsAPIDataManager: APIDataManager {
    init() {
    }
    // Data fetch service methods goes here
    func addNewSlot(parameter: [String:Any], completion:@escaping EmptyAPIResponseCompletion) {
        makeAPICall(to: DoctorNurseEndpoints.addSlot,withParameters: parameter, ofType: .httpBody, completion: completion)
    }
}
