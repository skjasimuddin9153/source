//
//  HospitalHomeVC.swift
//  Source
//
//  Created by Techwens on 23/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class HospitalHomeVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var lblSalutation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblBookingData: UILabel!
    lazy var viewModel = HospitalHomeViewModel()
    var toolBar = UIToolbar()
    var picker  = UIDatePicker()
    // MARK: - View Life Cycle Methods
    var bookingDate: Date = Date() {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            if dateFormatter.string(from: bookingDate)  == dateFormatter.string(from: Date()) {
                lblBookingData.text = "Booking for Today"
            } else {

                lblBookingData.text = "Booking for \(dateFormatter.string(from: picker.date))"
            }
        }
    }
    var parameter: [String: Any] = [:]
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        hideKeyboardWhenTappedAround()
        guard let data = DataManager.shared.getUserData() else { return }
        lblSalutation.text = "Good morning\n\(data.user?.hospital_name ?? "")! "
        let fullAddress = (data.user?.state ?? "") + " " + "\(data.user?.zip_code ?? " ")"
        let mainAddress = (data.user?.address_line_1 ?? "") + " " + (data.user?.address_line_2 ?? "")
        lblAddress.text =  mainAddress + " " + fullAddress
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getBookingList(parameters: parameter)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedFilterBtn(_ sender: UIButton) {
        guard let vc = HomeFilterVC.loadFromXIB() else { return }
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func UpdateDOBClick(_ sender: UIButton) {
        picker = UIDatePicker.init()
        picker.datePickerMode = .date
        picker.preferredDatePickerStyle = .wheels
        picker.minimumDate = Date()
        picker.backgroundColor = UIColor.white
        picker.setValue(UIColor.black, forKey: "textColor")
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        self.view.addSubview(picker)
        
        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 44))
        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
        tabBarController?.view.addSubview(toolBar)
        
    }
    @objc func onDoneButtonTapped() {
        toolBar.removeFromSuperview()
        picker.removeFromSuperview()
        bookingDate = picker.date
        
    }
}

// MARK: - Load from storyboard with dependency
extension HospitalHomeVC {
    class func loadFromXIB(withDependency dependency: HospitalHomeDependency? = nil) -> HospitalHomeVC? {
        let storyboard = UIStoryboard(name: "HospitalHome", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "HospitalHomeVC") as? HospitalHomeVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}


// MARK: - HospitalHomeAPIResponseDelegate
extension HospitalHomeVC: HospitalHomeAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleListAPI() {
        tableView.reloadData()
    }
    
}

extension HospitalHomeVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.bookingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalHomeTVC") as? HospitalHomeTVC else { return UITableViewCell() }
        cell.selectionStyle = .none
        cell.data = viewModel.bookingList[indexPath.row]
        return cell
    }
    

}
