//
//  TermConditionVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class TermConditionVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = TermConditionViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedConfirmBtn(_ sender: AppSolidButton) {
        navigationController?.popViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension TermConditionVC {
    class func loadFromXIB(withDependency dependency: TermConditionDependency? = nil) -> TermConditionVC? {
        let storyboard = UIStoryboard(name: "TermCondition", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "TermConditionVC") as? TermConditionVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - TermConditionAPIResponseDelegate
extension TermConditionVC: TermConditionAPIResponseDelegate {
}
