/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct UserDetails : Codable {
	let id : Int?
	let first_name : String?
	let middle_name : String?
	let last_name : String?
	let username : String?
	let email : String?
	let email_verified : Bool?
	let email_verified_at : String?
	let phone_code : Int?
	let phone_number : String?
	let user_type : UserType?
	let profile_image : String?
	let pan_number : String?
	let uploaded_document_type : String?
	let uploaded_document_number : String?
	let uploaded_document : String?
	let registration_state : String?
	let registration_number : String?
	let registration_document_front : String?
	let registration_document_back : String?
	let address_line_1 : String?
	let address_line_2 : String?
	let latitude : String?
	let longitude : String?
	let state : String?
	let zip_code : String?
	let country_code : String?
    let hospital_name: String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case first_name = "first_name"
		case middle_name = "middle_name"
		case last_name = "last_name"
		case username = "username"
		case email = "email"
		case email_verified = "email_verified"
		case email_verified_at = "email_verified_at"
		case phone_code = "phone_code"
		case phone_number = "phone_number"
		case user_type = "user_type"
		case profile_image = "profile_image"
		case pan_number = "pan_number"
		case uploaded_document_type = "uploaded_document_type"
		case uploaded_document_number = "uploaded_document_number"
		case uploaded_document = "uploaded_document"
		case registration_state = "registration_state"
		case registration_number = "registration_number"
		case registration_document_front = "registration_document_front"
		case registration_document_back = "registration_document_back"
		case address_line_1 = "address_line_1"
		case address_line_2 = "address_line_2"
		case latitude = "latitude"
		case longitude = "longitude"
		case state = "state"
		case zip_code = "zip_code"
		case country_code = "country_code"
        case hospital_name = "hospital_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		email_verified = try values.decodeIfPresent(Bool.self, forKey: .email_verified)
		email_verified_at = try values.decodeIfPresent(String.self, forKey: .email_verified_at)
		phone_code = try values.decodeIfPresent(Int.self, forKey: .phone_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		user_type = try values.decodeIfPresent(UserType.self, forKey: .user_type)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		pan_number = try values.decodeIfPresent(String.self, forKey: .pan_number)
		uploaded_document_type = try values.decodeIfPresent(String.self, forKey: .uploaded_document_type)
		uploaded_document_number = try values.decodeIfPresent(String.self, forKey: .uploaded_document_number)
		uploaded_document = try values.decodeIfPresent(String.self, forKey: .uploaded_document)
		registration_state = try values.decodeIfPresent(String.self, forKey: .registration_state)
		registration_number = try values.decodeIfPresent(String.self, forKey: .registration_number)
		registration_document_front = try values.decodeIfPresent(String.self, forKey: .registration_document_front)
		registration_document_back = try values.decodeIfPresent(String.self, forKey: .registration_document_back)
		address_line_1 = try values.decodeIfPresent(String.self, forKey: .address_line_1)
		address_line_2 = try values.decodeIfPresent(String.self, forKey: .address_line_2)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		zip_code = try values.decodeIfPresent(String.self, forKey: .zip_code)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
        hospital_name = try values.decodeIfPresent(String.self, forKey: .hospital_name)
	}

}
