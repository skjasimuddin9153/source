//
//  LogoutDemoViewModel.swift
//  Source
//
//  Created by Techwens on 24/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol LogoutDemoAPIResponseDelegate: class {
}

class LogoutDemoViewModel {
    weak var apiResponseDelegate: LogoutDemoAPIResponseDelegate?
    lazy var localDataManager = LogoutDemoLocalDataManager()
    lazy var apiDataManager = LogoutDemoAPIDataManager()
    
    var dependency: LogoutDemoDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
