//
//  RegistrationDocsViewModel.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol RegistrationDocsAPIResponseDelegate: class {
    func handleRegisterationStep3Response(message:String)
    func handleCountryListResponse()
    func handleStateListResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class RegistrationDocsViewModel {
    weak var apiResponseDelegate: RegistrationDocsAPIResponseDelegate?
    lazy var localDataManager = RegistrationDocsLocalDataManager()
    lazy var apiDataManager = RegistrationDocsAPIDataManager()
    
    var dependency: RegistrationDocsDependency?
    var countryList: [Country_list] = []
    var stateList: [State_list] = []
    
    init() {
    }
    // Data fetch service methods goes here
    func getAllCountries() {
        apiDataManager.countriesAPI {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.countryList = data.country_list ?? []
                    weakSelf.stateList = []
                    weakSelf.apiResponseDelegate?.handleCountryListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
    
    func getAllState(byCountryId id: String ) {
        apiDataManager.statesAPI(id: id) {[weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.stateList = data.state_list ?? []
                    weakSelf.apiResponseDelegate?.handleStateListResponse()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
            }
        }
    }
    
    
    func hospitalRegisterStepThree(parameter :  [String: String], payload: [UploadPayloadData]) {
        apiDataManager.registerAPI( with: payload, parameters: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                    weakSelf.apiResponseDelegate?.handleRegisterationStep3Response(message: response.message)
            }
            
        })
    }
}
