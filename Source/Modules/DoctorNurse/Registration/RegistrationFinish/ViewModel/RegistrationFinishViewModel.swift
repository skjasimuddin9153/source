//
//  RegistrationFinishViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol RegistrationFinishAPIResponseDelegate: class {
}

class RegistrationFinishViewModel {
    weak var apiResponseDelegate: RegistrationFinishAPIResponseDelegate?
    lazy var localDataManager = RegistrationFinishLocalDataManager()
    lazy var apiDataManager = RegistrationFinishAPIDataManager()
    
    var dependency: RegistrationFinishDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
