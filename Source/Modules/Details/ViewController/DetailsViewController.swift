//
//  DetailsViewController.swift
//  Source
//
//  Created by Techwens Software on 30/08/23.
//

import UIKit
import CoreLocation
import AlamofireImage

class DetailsViewController: UIViewController {

   
    @IBOutlet var tableView: UITableView!
    @IBOutlet var foterMainView: UIView!
    @IBOutlet var fotterView: UIView!
    @IBOutlet var locationIcon: UIStackView!
    var totalPage = 1
    var currentPage = 1
    @IBOutlet var topLocation: UILabel!
    var currLocation:String?
    var skeletoLoader:(() -> Void)?
    var apiRequestAlreadyMade = false {
        didSet{
            skeletoLoader?()
        }
    }

    @IBOutlet var btnViewCart: UIButton!
    var bottomSheetView = LocationView()
    var allProduct: ProductResponse?
    lazy var  viewModel = DetailsViewModel()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        addCornerRadiusToFooterView()
        presentBottomSheet()
        tablocation()
        BottomSheetFunc()
        refereshControll()
       
    }
    
    func refereshControll(){
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(onReferesh), for: .valueChanged)
    }
    func BottomSheetFunc(){
        
         // Do any additional setup after loading the view.
         bottomSheetView.onCloseButtonTapped = { [weak self] in
                   self?.hideBottomSheet()
               }
           
                
         bottomSheetView.onAllowButtonTapped = {
             self.handleAllowButtonTap()
                    // Handle allow button click action here
                }
         let data = cartdataManager.shared.data
        
         
         cartdataManager.shared.dataDidChangeCallback = { [weak self] in
                self?.updateUIForDataChange()
            }
            
            // Check the initial state of the data array
            updateUIForDataChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.getAllproduct(page: currentPage, perPage: 15)
        viewModel.getLocationByLatLng(lat:LocationManager.shared.location?.coordinate.latitude ?? 0 , lng: LocationManager.shared.location?.coordinate.longitude ?? 0)
        observeLoginViewModelEvents()
    }
    
    @objc func onReferesh (){
    currentPage = 1
        print("refershing...")
     
        viewModel.getAllproduct(page: 1, perPage: 10)
    }
    
    func observeLoginViewModelEvents() {
        viewModel.eventHandler = { [weak self] event in
            guard let self = self else { return }
                
            switch event {
            case .loading:
                // Handle loading state (e.g., show activity indicator)
                self.tableView.refreshControl?.beginRefreshing()
                print("Loading...")
            case .stopLoading:
                // Handle stop loading state (e.g., hide activity indicator)
              
                print("Stop loading...")
            case .LocationData(let locationData):
                DispatchQueue.main.async {
                    self.currLocation = locationData.data
                    self.topLocation.text = locationData.data
                }
            case .ProductData(let loginResponse):
                print("Data loaded...")
                DispatchQueue.main.async {
                   
                    if self.currentPage == 1 {
                        // Refreshing or initial load, clear previous data
                        self.allProduct = loginResponse
                    } else {
                        // Pagination, append new data to the existing array
                        self.allProduct?.data.products.rows.append(contentsOf: loginResponse.data.products.rows)
                      
                    }
                    self.totalPage = loginResponse.data.products.count/10
                   
                
                    self.apiRequestAlreadyMade = true
                    self.tableView.reloadData()
                }
            case .dataLoaded:
                // Handle data loaded event (e.g., update UI)
                self.tableView.refreshControl?.endRefreshing()
                print("Data loaded event")
            case .error(let error):
                 guard let vc = DemoVC.loadFromXIB() else {return}
                
                
                vc.onReloadPress = {
                    self.navigationController?.pushViewController(Self.loadFromXIB() ?? vc, animated: true)
                }
                vc.onBtnGoHomePress = {
                    self.navigationController?.pushViewController(Self.loadFromXIB() ?? vc, animated: true)
                }
                self.navigationController?.pushViewController(vc, animated: true)
                
                // Handle error (e.g., show an error message)
                print("Error: \(error)")
           
            }
        }
    }


 
    
//    function for fotter cart hide/show
    func updateUIForDataChange() {
        let data = cartdataManager.shared.data
        if data.isEmpty {
            foterMainView.isHidden = true
        } else {
            foterMainView.isHidden = false
        }
    }

    
    @IBAction func btnTappedCart(_ sender: Any) {
        guard let vc = CartViewController.loadFromXIB() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    func addCornerRadiusToFooterView() {
        
//         foter mainview
//        foterMainView.backgroundColor = UIColor.init(white: 0.0, alpha: 0.0)

        let cornerRadius: CGFloat = 30
        
        
        fotterView.layer.cornerRadius = cornerRadius
        fotterView.layer.masksToBounds = true
//        fotterView.roundedCorners(_, radius: cornerRadius)
        btnViewCart.layer.cornerRadius = 20
        btnViewCart.layer.masksToBounds = true
    }
    
    
    func presentBottomSheet() {
      
        
        view.addSubview(bottomSheetView)
        
        let height = view.frame.height * 0.4 // Adjust this value as needed
        let startY = view.frame.height
        
        bottomSheetView.frame = CGRect(x: 0, y: startY, width: view.frame.width, height: height)
        
        UIView.animate(withDuration: 0.3) { [self] in
            self.bottomSheetView.frame.origin.y = self.view.frame.height - height
        }
       
    }
    
    func hideBottomSheet() {
          let height = view.frame.height * 0.4 // Adjust this value to match the original height of the bottom sheet view
          
          UIView.animate(withDuration: 0.3, animations: { [weak self] in
              self?.bottomSheetView.frame.origin.y += height
          }) { [weak self] _ in
              self?.bottomSheetView.removeFromSuperview()
          }
      }

    
    func handleAllowButtonTap() {
          let locationManager = CLLocationManager()
          locationManager.delegate = self

          if CLLocationManager.locationServicesEnabled() {
              switch CLLocationManager.authorizationStatus() {
              case .notDetermined:
                  locationManager.requestWhenInUseAuthorization() // Or .requestAlwaysAuthorization()
              case .authorizedWhenInUse, .authorizedAlways:
                  locationManager.startUpdatingLocation()
              case .denied, .restricted: break
            
                  // Handle the case when location access is denied or restricted
              @unknown default: break
                  // Handle future authorization status cases
              }
          }
        hideBottomSheet()
        
        print("latitude==\(LocationManager.shared.location?.coordinate.latitude ?? 0)", "longitude==\(LocationManager.shared.location?.coordinate.longitude ?? 0)")
      }
    
    // Add tap gesture recognizer to locationIcon
    func tablocation (){
        locationIcon.isUserInteractionEnabled = true

        let tabGesture = UITapGestureRecognizer(target: self, action: #selector(locationIconTapped))
        locationIcon.addGestureRecognizer(tabGesture)
    }
    
    @objc func locationIconTapped() {
          print("Location icon tapped")
        
        guard  let vc = AddressSelectionViewController.loadFromXIB() else {return}
        
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.modalPresentationStyle = .popover // Adjust the presentation style as needed
           
           present(navigationController, animated: true, completion: nil)
          // Handle your action here, e.g., show location details, start location-related functionality, etc.
      }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onAccountBtnClicked(_ sender: Any) {
        guard let vc = MyAccountViewController.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // Remove the callback when the view controller is deinitialized
       deinit {
           cartdataManager.shared.dataDidChangeCallback = nil
       }
    
}

extension DetailsViewController {
    class func loadFromXIB(withDependency dependency: AllProduct? = nil) -> DetailsViewController? {
        let storyboard = UIStoryboard(name: "Detail", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController else {
            return nil
        }
      
        return viewController
    }
}


extension DetailsViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 160
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Add 1 to the count for the loading cell if there are more pages to load
        return allProduct?.data.products.rows.count ?? 15
//        if currentPage < totalPage {
//            return (allProduct?.data.countPerPage ?? 15) + 1
//        } else {
//            return  ?? 15
//        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if currentPage < totalPage && indexPath.row == (allProduct?.data.products.rows.count ?? 10) - 1 {
            // This is the loading cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "loading", for: indexPath)
            return cell
        } else {
            let cell: DetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DetailsTableViewCell
           

            if let product = allProduct?.data.products.rows[indexPath.row] {
                // Populate your cell with data from the `Product` structure
                cell.productImage.setImage(with: product.imageUrl)
                cell.productTitle.text = product.name
                cell.productPrice.text = String(product.marketPrice)
                cell.skelitonBox.isHidden = apiRequestAlreadyMade
                // You can similarly populate `productSubTitle` and `productPrice`
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard   let vc = ProductDetailsViewController.loadFromXIB() else {return}
        if let product = allProduct?.data.products.rows[indexPath.row] {
            vc.productId = String(product.id)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if currentPage < totalPage && indexPath.row == (allProduct?.data.products.rows.count ?? 10) - 1 {
            currentPage += 1
            viewModel.getAllproduct(page: currentPage, perPage: 10)
        }
    }

    
    
}


// MARK: - Get Location

extension DetailsViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
           
            // Store the location in the global variable
//            globalLocation = location
            print(location,"LocationView")
            
          
     
        }
        manager.stopUpdatingLocation()
       
    }
}

