import Foundation
extension String {
    static func value(fromJSON json: [AnyHashable: Any]) -> String? {
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            let convertedString = String(data: data1, encoding: .utf8)
            return convertedString
        } catch let myJSONError {
            debugPrint(myJSONError)
            return nil
        }
    }
}

extension String {

    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }

    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String {
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
    
    func isValidPassword() -> Bool {
        let passwordRegEx =  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,}$"
        let passwordPred = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordPred.evaluate(with: self)
    }
    
    func isValidName() -> Bool {
        let nameRegEx =  "^([a-zA-z]+\\s?)*\\s*$"
        let namePred = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return namePred.evaluate(with: self)
    }
    func isValidNameWithSpace() -> Bool {
        let nameRegEx =  "^[a-zA-Z ]*$"
        let namePred = NSPredicate(format:"SELF MATCHES %@", nameRegEx)
        return namePred.evaluate(with: self)
    }
    func isValidPhoneNumber() -> Bool {
        let numberRegEx =  "^[0-9]{8,10}$"
        let numberPred = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return numberPred.evaluate(with: self)
    }
    
    func isValidOtp() -> Bool {
        let numberRegEx =  "^[0-9]{4}$"
        let numberPred = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return numberPred.evaluate(with: self)
    }
    
    func isValidPin() -> Bool {
        let numberRegEx =  "^[0-9]{5}$"
        let numberPred = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        return numberPred.evaluate(with: self)
    }
}

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}

extension String {
    func slotFormatTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh: mm a"
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = "HHmm"
            return dateFormatter.string(from: date)
        }
        return ""
    }
    func slotTimeTodate() -> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh: mm a"
        if let date = dateFormatter.date(from: self) {
            return date
        }
        return nil
    }
    
    func stringToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: self)
        return date
    }
    
    func localToUTC(dateStr: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = Calendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let date = dateFormatter.date(from: dateStr) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "H:mm:ss"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    func utcToLocal() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat =  "dd-MMM-yyyy h:mm:a"
        
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    func utcToLocalSplit() -> (String?,String?) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let dateFull = dateFormatter.date(from: self) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat =  "dd-MMM-yyyy"
            let date = dateFormatter.string(from: dateFull)
            dateFormatter.dateFormat =  "h:mm a"
            let time = dateFormatter.string(from: dateFull)
            return (date, time)
        }
            
        return (nil,nil)
    }
}

extension String {
    func validateWith(regex: TextFieldValidationRegex) -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        return predicate.evaluate(with: self)
    }
}

struct JSON {
    static let encoder = JSONEncoder()
}
extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
