//
//  HomeFavouriteTVC.swift
//  Source
//
//  Created by Techwens on 23/07/22.
//

import UIKit

class HomeFavouriteTVC: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var favouriteHospitalList: [FavouriteModel] = [] {
        didSet{
            collectionView.reloadData()
        }
    }
    var tappedFavHospitalCompletion: ((Int) -> Void) = {_ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension HomeFavouriteTVC : UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favouriteHospitalList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavouriteCVC", for: indexPath) as? FavouriteCVC
        else { return UICollectionViewCell() }
        cell.data = favouriteHospitalList[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        tappedFavHospitalCompletion(indexPath.row)
    }
    
}
