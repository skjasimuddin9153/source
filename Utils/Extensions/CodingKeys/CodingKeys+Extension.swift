import Foundation

extension CodingKey {
    // Custom implementation to convert camelCase to snake_case
    var snakeCase: String {
        let key = stringValue
        let pattern = "([a-z0-9])([A-Z])"
        let regex = try! NSRegularExpression(pattern: pattern, options: [])
        let range = NSRange(key.startIndex..<key.endIndex, in: key)
        
        return regex.stringByReplacingMatches(
            in: key,
            options: [],
            range: range,
            withTemplate: "$1_$2"
        ).lowercased()
    }
}
