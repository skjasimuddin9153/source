//
//  PersonalDetailsViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol PersonalDetailsAPIResponseDelegate: AnyObject {
    func handleRegisterResponse(data: RegisterStepOneData)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class PersonalDetailsViewModel {
    weak var apiResponseDelegate: PersonalDetailsAPIResponseDelegate?
    lazy var localDataManager = PersonalDetailsLocalDataManager()
    lazy var apiDataManager = PersonalDetailsAPIDataManager()
    
    var dependency: PersonalDetailsDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func register( userType: UserType, firstName: String, lastName: String, email: String, gender: Gender, phoneNo: String) {
        let parameters = [
            "step": "1",
            "user_type": userType.rawValue,
            "first_name": firstName,
            "last_name": lastName,
            "middle_name": "",
            "email": email,
            "gender": gender.rawValue,
            "ip_address": "",
            "phone_code": "91",
            "phone_number": phoneNo
        ]
        apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.apiResponseDelegate?.handleRegisterResponse(data: data)
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
