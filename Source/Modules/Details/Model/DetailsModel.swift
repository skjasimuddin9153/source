//
//  DetailsModel.swift
//  Source
//
//  Created by Techwens Software on 30/08/23.
//

import Foundation
import UIKit

struct AllProduct {
  let id:Int
  let productImage:UIImage
   let productTitle:String?
   let productSubTitle:String?
    let productPrice:String?

}
struct Product: Codable {
    let id: Int
    let name: String
    let thcPercentage: String
    let marketPrice: Double
    let actualPrice: Double
    let rating: Int?
    let imageUrl: String
    let strain: Strain?
    let brand: Brand?
    
    struct Strain: Codable {
        let name: String?
    }
    
    struct Brand: Codable {
        let name: String?
    }
}

struct ProductResponse: Codable {
    let status: Bool
    var data: DataObject
    
    struct DataObject: Codable {
        var products: Products
        let countPerPage: Int
        
        struct Products: Codable {
            let count: Int
            var rows: [Product]
        }
    }
}

struct Locationresponse:Codable{
    let status:Bool
    let data : String
}
