//
//  LoginPinVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import Alamofire

var isValid:Bool = false

class LoginPinVC: UIViewController {

  
    
 
//    func didChangeValidity(isValid: Bool) {
//
//       }
    // MARK: Instance variables
//    Variable to store the entered OTP
    var enteredOTP: String = ""
    
    @IBOutlet var Header: HeaderView!
    @IBOutlet var otpView: OTPStackView!
    @IBOutlet var OtpSubmitBtn: BorderButton!
    var otp:String = ""
    
    var phoneNumber:String = ""
    var authKey:String = ""
    var countryCode = ""
    lazy var viewModel = LoginPinViewModel()
    var otpValue : String = ""
   
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        OtpSubmitBtn.isValid = false
        super.viewDidLoad()
//        otpLogic()
        let otpStackView = OTPStackView()
        otpStackView.delegate = self
        otpView.addSubview(otpStackView)
        otpView.delegate = self
        
        Header.navigationController = self.navigationController
        
        Header.title.isHidden = true
      
//        viewModel.apiResponseDelegate = self
//        tfPin.setupSourceTextField(withPlaceholder: "Enter Pin")
//        tfPin.setLeftViewButton(with: UIImage(named: "icKey")!)
//        tfPin.addTarget(self, action: #selector(textUpdate), for: .editingChanged)
//        tfPin.delegate = self
        
//        if let data = viewModel.dependency {
//            userTypeImage.image = data.userType.placeholderImage
//            switch data.userType{
//            case .doctor:
//                lblName.text = "Dr. \(data.user.first_name ?? "")"
//                lblIncorrectUser.text = "Not Dr. \(data.user.first_name ?? "") ?"
//            case .nurse:
//                lblName.text = "Nurse \(data.user.first_name ?? "")"
//                lblIncorrectUser.text = "Not Nurse \(data.user.first_name ?? "") ?"
//            case .hospital:
//                lblName.text = "Hospital \(data.user.first_name ?? "")"
//                lblIncorrectUser.text = "Not Hospital \(data.user.first_name ?? "") ?"
//
//            }
//        }
    }
    


    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    
    @objc func  textComplete(stackView:OTPStackView){
        
//        print("otpStackView.getOTP()",stackView.otpEnter)
    }
//    @objc func textUpdate(textfield: UITextField){
//
//        if let pin = textfield.text, pin.count == 4{
//            viewModel.login(withEmail: viewModel.dependency?.user.email ?? "", andPassword: pin)
//
//        }
//    }
    
   
    @IBAction func tappedSubmitOTPBtn(_ sender: UIButton) {
//        print("Archan:tap",otpView.otpEnter)
        guard let vc = KYCVC.loadFromXIB() else { return }
        viewModel.verify(countryCode: countryCode, mobilePhone: phoneNumber, otp: otp,authKey: authKey)
        observeLoginViewModelEvents(viewController: vc)
    }
    func observeLoginViewModelEvents(viewController:KYCVC) {
           viewModel.eventHandler = { [weak self] event in
               guard let self = self else { return }
            
               switch event {
               case .loading:
                   // Handle loading state (e.g., show activity indicator)
                   print("Loading...")
               case .stopLoading:
                   // Handle stop loading state (e.g., hide activity indicator)
                   print("Stop loading...")
                   
               case .data(let loginResponse):
                   showtoast(message: loginResponse .data?.message ?? "" , color: .green)
               case .dataLoaded:
                   // Handle data loaded event (e.g., update UI)
                   print("Data loaded...")
                   DispatchQueue.main.async {
               
                       self.navigationController?.pushViewController(viewController, animated: true)
                   }
               case .error(let error):
                   // Handle error (e.g., show an error message)
                   showtoast(message: "Please enter a valid otp" , color: .red)
                   print("Error: \(error)")
               
           }
       }
   
    
    
}
    

    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSubmitBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - Load from storyboard with dependency
extension LoginPinVC {
    class func loadFromXIB() -> LoginPinVC? {
        let storyboard = UIStoryboard(name: "LoginPin", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "LoginPinVC") as? LoginPinVC else {
            return nil
        }
       
        return viewController
    }
}

extension LoginPinVC : OTPDelegate {
    func didChangeValidity(isValid: Bool, otpVal: String) {
        OtpSubmitBtn.isValid = isValid
        print(otpVal,"otpVal")
        otp = otpVal
    }
    
    
}

// MARK: - LoginPinAPIResponseDelegate
//extension LoginPinVC: LoginPinAPIResponseDelegate {
//    func handleLoginResponse(data: UserType) {
//        switch data {
//        case .doctor,.nurse:
//            UIWindow.key?.replaceRootViewControllerWith(AppTabBarController(), animated: true, completion: nil)
//        case .hospital:
//            UIWindow.key?.replaceRootViewControllerWith(HospitalTabBarController(), animated: true, completion: nil)
//        }
//
//    }
//
//    func handleAPIError(_ error: Error) {
//        view.isUserInteractionEnabled = true
//        showSnackbar(withMessage: error.localizedDescription)
//        print(error)
//    }
//
//    func handleAPIMessage(message: String) {
//        view.isUserInteractionEnabled = true
//        showSnackbar(withMessage: message)
//        print(message)
//    }
//}
//
//extension LoginPinVC: UITextFieldDelegate {
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let maxLength = 4
//        let currentString = (textField.text ?? "") as NSString
//        let newString = currentString.replacingCharacters(in: range, with: string)
//
//        return newString.count <= maxLength
//    }
//}
//extension LoginPinVC {
//    func setupPinView(){
//        let otpFields = OTPStackView(frame: otpView.frame, fields: 6)
//        otpView.addSubview(otpFields)
//         NSLayoutConstraint.activate([
//             otpFields.topAnchor.constraint(equalTo: otpView.topAnchor),
//             otpFields.leadingAnchor.constraint(equalTo: otpView.leadingAnchor),
//             otpFields.trailingAnchor.constraint(equalTo: otpView.trailingAnchor),
//             otpFields.bottomAnchor.constraint(equalTo: otpView.bottomAnchor)
//         ])
//        self.otpTextField = otpFields
//        self.otpTextField?.delegate = self
//
//
//    }
//}
//
//extension LoginPinVC: OTPDelegate {
//
//    func didChangeValidity(isValid: Bool) {
//        if isValid {
//
//            otpValue = otpTextField?.getOTP() ?? ""
//        } else {
//
//        }
    

//}
//}



