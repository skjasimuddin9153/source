//
//  ScheduledAppointmentViewModel.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol ScheduledAppointmentAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleListAPI()
}

class ScheduledAppointmentViewModel {
    weak var apiResponseDelegate: ScheduledAppointmentAPIResponseDelegate?
    lazy var localDataManager = ScheduledAppointmentLocalDataManager()
    lazy var apiDataManager = ScheduledAppointmentAPIDataManager()
    
    var dependency: ScheduledAppointmentDependency?
    var bookingList: [Booking_list] = []
    
    init() {
    }
    // Data fetch service methods goes here
    func getBookingList() {
        
        apiDataManager.getBookingData { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.bookingList = data.booking_list ?? []
                    weakSelf.apiResponseDelegate?.handleListAPI()
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }
               

            }
        }
    }
}
