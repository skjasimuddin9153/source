//
//  BookingTicketVC.swift
//  Source
//
//  Created by Techwens on 08/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import JTAppleCalendar

class BookingTicketVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblSpeciallity: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAddressBottom: UILabel!
    @IBOutlet weak var btnFav: UIButton!
    
    @IBOutlet weak var hospitalDetailsView: UIView!
    @IBOutlet var calendarView: JTACMonthView!
    @IBOutlet weak var bookingTimeCollectionView: UICollectionView!
    var formatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        return formatter
    }
    var slotData: [DoctorNurseSlotModel] = []
    
    var data: CalendarData = CalendarData(startDate: "", endDate:  "", availableDates: [], selectedCalendarData: [])
    var selectedCalendarData: [String] = []
	lazy var viewModel = BookingTicketViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        if let id = viewModel.dependency?.hospitalID {
            viewModel.getHospitalDetailsData(hospitalId: id)
        }
        let availableDate: [String] = Date.dates(from: Date(), to: Date().addMonth(n: 2)).map({formatter.string(from: $0)})
        data.availableDates = availableDate
        data.startDate = formatter.string(from: Date())
        data.endDate  = formatter.string(from: Date().addMonth(n: 2))
        hospitalDetailsView.applyLightAllAroundShadow()
        calendarView.register(UINib(nibName: "DataCell", bundle: nil), forCellWithReuseIdentifier: "DataCell")
        calendarView.register(UINib(nibName: "DataHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier:  "DataHeader")
        calendarView.scrollDirection = .horizontal
        calendarView.scrollingMode   = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = false
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        bookingTimeCollectionView.dataSource = self
        bookingTimeCollectionView.delegate = self
        
        calendarView.reloadData()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedHeartBtn(_ sender: UIButton) {
        guard let id = viewModel.dependency?.hospitalID else {
            return
        }
        if sender.isSelected {
            viewModel.removeFromFavourite(hospitalId: id, button: sender)
        } else {
            viewModel.markAsFavourite(hospitalId: id, button: sender)
        }
    }
    @IBAction func tappedConfirmBtn(_ sender: AppSolidButton) {
        
        if data.selectedCalendarData.isEmpty {
            showSnackbar(withMessage: "No selected Date")
              return
        } else {
            let selectedDate  = data.selectedCalendarData.first!
            let formatterNew = DateFormatter()
            formatterNew.dateFormat = "dd-MMM-yyyy"
            let nd = formatterNew.date(from: selectedDate)
            formatterNew.dateFormat = "yyyy-MM-dd"
            let stringDate = formatterNew.string(from: nd!)
            if let selectedIndex = bookingTimeCollectionView.indexPathsForSelectedItems {
                if selectedIndex.isEmpty {
                  showSnackbar(withMessage: "No selected Slot")
                    return
                } else {
                   let index = selectedIndex.first?.row
                    let id = slotData[index ?? 0].id
                    //make api call to select slot with id
                    viewModel.submitBookingData(hospitalId: viewModel.dependency!.hospitalID!, slotDate: stringDate, slotTime: id!)
                }
            } else {
                showSnackbar(withMessage: "No selected Slot")
                  return
            }
        }

        
    }
}

// MARK: - Load from storyboard with dependency
extension BookingTicketVC {
    class func loadFromXIB(withDependency dependency: BookingTicketDependency? = nil) -> BookingTicketVC? {
        let storyboard = UIStoryboard(name: "BookingTicket", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "BookingTicketVC") as? BookingTicketVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - BookingTicketAPIResponseDelegate
extension BookingTicketVC: BookingTicketAPIResponseDelegate {
    func handleMarkAsFavResponse(button: UIButton, message: String) {
        showSnackbar(withMessage: message)
        button.isSelected = true
        button.tintColor = .systemPink
    }
    
    func handleRemoveFavResponse(button: UIButton, message: String) {
        showSnackbar(withMessage: message)
        button.isSelected = false
        button.tintColor = .black
    }
    
    func handleBookingMessageResponse(message: String) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
        let dependency = ConfirmationCancellationDependency(status: .success)
        guard let vc = ConfirmationCancellationVC.loadFromXIB(withDependency: dependency) else {
            return
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGetHospitaDetailsResponse(data: HospitalDetailsResponseModel) {
        //..
        if (data.is_favourite ?? false) {
            btnFav.isSelected = true
            btnFav.tintColor = .systemPink
        } else {
            btnFav.isSelected = false
            btnFav.tintColor = .black
        }
        let fullAddress = (data.district ?? "") + " " + (data.state ?? "") + " " + "\(data.zip_code ?? 0)"
        let mainAddress = (data.address_line_1 ?? "") + " " + (data.address_line_2 ?? "")
        lblAddress.text =  mainAddress + " " + fullAddress
        lblAddressBottom.text = mainAddress + " " + fullAddress
        lblHospitalName.text = data.hospital_name ?? ""
        lblSpeciallity.text = data.speciality?.rawValue ?? ""
        guard let userType =  DataManager.shared.userLoginData?.user?.user_type,
        let slotData = data.slots else {
            Log.e("Unknown User Type")
            return
        }
        switch userType {
        case .doctor:
            self.slotData = slotData.doctor ?? []
        case .nurse:
            self.slotData = slotData.nurse ?? []
        case .hospital:
            self.slotData = []
        }
        bookingTimeCollectionView.reloadData()
    }
    
    func handleAPIError(_ error: Error) {
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        showSnackbar(withMessage: message)
    }
    
}

extension BookingTicketVC {
    
    func configureCell(view: JTACDayCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
        cell.dateLabel.text = cellState.text
        handleCellTextColor(cell: cell, cellState: cellState)
       
    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
        
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.isHidden = false
        } else {
            cell.dateLabel.isHidden = true
        }
        let dateString = formatter.string(from: cellState.date)
        if data.availableDates.contains(dateString) {
            cell.dateLabel.textColor = .green
        } else {
            cell.dateLabel.textColor = .lightGray
        }
        if data.selectedCalendarData.contains(dateString) {
            cell.dotView.isHidden = false
        } else {
            cell.dotView.isHidden = true
        }
    }
    
    func handleCellEvents( cellState: CellState) {
        let dateString = formatter.string(from: cellState.date)
        if data.availableDates.contains(dateString) {
            if data.selectedCalendarData.contains(dateString) {
                data.selectedCalendarData = data.selectedCalendarData.filter({$0 != dateString})
            } else {
                data.selectedCalendarData = [dateString]
            }
        }
        calendarView.reloadData()
    }
    
    
}

extension BookingTicketVC: JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        let startDate = formatter.date(from: data.startDate)!
        let endDate = formatter.date(from: data.endDate)!
        return ConfigurationParameters(startDate: startDate, endDate: endDate)
    }
}

extension BookingTicketVC: JTACMonthViewDelegate {
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        handleCellEvents(cellState: cellState)
    }
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "DataCell", for: indexPath) as! DateCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        return cell
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let formatter = DateFormatter()  // Declare this outside, to avoid instancing this heavy class multiple times.
        formatter.dateFormat = "MMM"

        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "DataHeader", for: indexPath) as! DateHeader
        header.monthTitle.text = formatter.string(from: range.start)
        header.completionNext = {
            calendar.scrollToSegment(.next)
        }
        header.completionPrevious = {
            calendar.scrollToSegment(.previous)
        }
        return header
    }

    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return MonthSize(defaultSize: 110)
    }
}
extension BookingTicketVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bookingTimeCollectionView {
            return slotData.count
        } else {
            return 0
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = bookingTimeCollectionView.dequeueReusableCell(withReuseIdentifier: "BookingSlotCVC", for: indexPath) as? BookingSlotCVC
        else {
            return UICollectionViewCell()
        }
        let data = slotData[indexPath.row]
        cell.lblTimeSlot.text = (data.start_time ?? "") + " to " + (data.end_time ?? "")
        return cell
    }
    
    
}

extension BookingTicketVC: UICollectionViewDelegateFlowLayout {
    
}
