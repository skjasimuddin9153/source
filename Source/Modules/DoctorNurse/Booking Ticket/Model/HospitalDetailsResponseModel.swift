/* 
Copyright (c) 2022 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct HospitalDetailsResponseModel : Codable {
	let id : Int?
	let hospital_name : String?
	let number_of_emergency_beds : Int?
	let number_of_icu_units : Int?
	let number_of_icu_beds : Int?
	let profile_image : String?
	let username : String?
	let first_name : String?
	let last_name : String?
	let middle_name : String?
	let email : String?
	let phone_code : Int?
	let phone_number : String?
	let speciality : SlotType?
	let is_favourite : Bool?
	let address_line_1 : String?
	let address_line_2 : String?
	let latitude : String?
	let longitude : String?
	let district : String?
	let state : String?
	let country_code : String?
	let zip_code : Int?
	let slots : BookingSlotsModel?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case hospital_name = "hospital_name"
		case number_of_emergency_beds = "number_of_emergency_beds"
		case number_of_icu_units = "number_of_icu_units"
		case number_of_icu_beds = "number_of_icu_beds"
		case profile_image = "profile_image"
		case username = "username"
		case first_name = "first_name"
		case last_name = "last_name"
		case middle_name = "middle_name"
		case email = "email"
		case phone_code = "phone_code"
		case phone_number = "phone_number"
		case speciality = "speciality"
		case is_favourite = "is_favourite"
		case address_line_1 = "address_line_1"
		case address_line_2 = "address_line_2"
		case latitude = "latitude"
		case longitude = "longitude"
		case district = "district"
		case state = "state"
		case country_code = "country_code"
		case zip_code = "zip_code"
		case slots = "slots"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		hospital_name = try values.decodeIfPresent(String.self, forKey: .hospital_name)
		number_of_emergency_beds = try values.decodeIfPresent(Int.self, forKey: .number_of_emergency_beds)
		number_of_icu_units = try values.decodeIfPresent(Int.self, forKey: .number_of_icu_units)
		number_of_icu_beds = try values.decodeIfPresent(Int.self, forKey: .number_of_icu_beds)
		profile_image = try values.decodeIfPresent(String.self, forKey: .profile_image)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		first_name = try values.decodeIfPresent(String.self, forKey: .first_name)
		last_name = try values.decodeIfPresent(String.self, forKey: .last_name)
		middle_name = try values.decodeIfPresent(String.self, forKey: .middle_name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		phone_code = try values.decodeIfPresent(Int.self, forKey: .phone_code)
		phone_number = try values.decodeIfPresent(String.self, forKey: .phone_number)
		speciality = try values.decodeIfPresent(SlotType.self, forKey: .speciality)
		is_favourite = try values.decodeIfPresent(Bool.self, forKey: .is_favourite)
		address_line_1 = try values.decodeIfPresent(String.self, forKey: .address_line_1)
		address_line_2 = try values.decodeIfPresent(String.self, forKey: .address_line_2)
		latitude = try values.decodeIfPresent(String.self, forKey: .latitude)
		longitude = try values.decodeIfPresent(String.self, forKey: .longitude)
		district = try values.decodeIfPresent(String.self, forKey: .district)
		state = try values.decodeIfPresent(String.self, forKey: .state)
		country_code = try values.decodeIfPresent(String.self, forKey: .country_code)
		zip_code = try values.decodeIfPresent(Int.self, forKey: .zip_code)
		slots = try values.decodeIfPresent(BookingSlotsModel.self, forKey: .slots)
	}

}
