//
//  DefaultSlotVC.swift
//  Source
//
//  Created by Techwens on 02/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit
import EmptyDataSet_Swift

enum SlotType: String, Codable {
    case icu = "ICU"
    case emergency = "EMG"
    case all = "ALL"
}

class DefaultSlotVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var emergencyBtnView: UIView!
    @IBOutlet weak var icuBtnView: UIView!
    @IBOutlet weak var emergencyRadioImage: UIImageView!
    @IBOutlet weak var slotSeperator: UIView!
    @IBOutlet weak var icuRadioImage: UIImageView!
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var specialitySepertor: UIView!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var tfPrice: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSave: AppSolidButton!
    lazy var viewModel = DefaultSlotViewModel()
    var totalHoursCovered: Double = 0
    var selecedSlot: SlotType? {
        didSet {
            switch selecedSlot {
            case .icu:
                icuRadioImage.image = #imageLiteral(resourceName: "icRadioCheck")
                emergencyRadioImage.image = #imageLiteral(resourceName: "icRadio")
            case .emergency:
                emergencyRadioImage.image = #imageLiteral(resourceName: "icRadioCheck")
                icuRadioImage.image = #imageLiteral(resourceName: "icRadio")
            default:
                ()
            }
            tableView.reloadData()
        }
    }
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        selecedSlot = .icu
        tfStartTime.setupSourceTextField(withPlaceholder: "Start Time")
        tfStartTime.setInputViewTimePicker(target: self, selector: #selector(tapDonetfStart(sender:datePicker1:)))
        tfStartTime.setRightViewButton(with: UIImage(named: "icTime")!)
        tfEndTime.setupSourceTextField(withPlaceholder: "End Time")
        tfEndTime.setInputViewTimePicker(target: self, selector: #selector(tapDonetfEnd(sender:datePicker1:)))
        tfEndTime.setRightViewButton(with: UIImage(named: "icTime")!)
        tfPrice.setupSourceTextField(withPlaceholder: "XX,XXX /per hourr")
        tfPrice.setLeftViewButton(with: UIImage(named: "icRupee")!)
        tfPrice.keyboardType = .decimalPad
        tableView.delegate = self
        tableView.dataSource = self
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.rowHeight = 45
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: "SlotDefaultTVC", bundle: nil), forCellReuseIdentifier: "SlotDefaultTVC")
        
        icuBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selecedSlot = .icu
        }
        emergencyBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.selecedSlot = .emergency
        }
       
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        specialitySepertor.createDottedLine(width: 1.0, color: UIColor.grayDot?.cgColor ?? UIColor.clear.cgColor)
        slotSeperator.createDottedLine(width: 1.0, color: UIColor.black.cgColor)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @objc func tapDonetfStart(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfStartTime.inputView as? UIDatePicker { // 2.1
            self.tfStartTime.text = datePicker.date.toString(format: .custom("hh: mm a"))
        }
        self.tfStartTime.resignFirstResponder() // 2.5
    }
    @objc func tapDonetfEnd(sender: Any, datePicker1: UIDatePicker) {
        print(datePicker1)
        if let datePicker = self.tfEndTime.inputView as? UIDatePicker { // 2.1
            self.tfEndTime.text = datePicker.date.toString(format: .custom("hh: mm a"))
        }
        self.tfEndTime.resignFirstResponder() // 2.5
    }
    
    func areAllFieldValid() -> Bool {
        if selecedSlot == nil {
            showSnackbar(withMessage: "Select A Speciality")
            return false
        }
        if tfStartTime.text == nil || ( tfStartTime.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "Start time Invalid")
            return false
        }
        if tfEndTime.text == nil ||  (tfEndTime.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "End time Invalid")
            return false
        }
        if tfPrice.text == nil ||  (tfPrice.text?.isEmpty ?? true) {
            showSnackbar(withMessage: "Price Invalid")
            return false
        }
        return true
    }
    @IBAction func tappedAddSlotBtn(_ sender: UIButton) {
        if !areAllFieldValid() {
            return
        }
        guard let startTime = tfStartTime.text!.slotTimeTodate() else {
            showSnackbar(withMessage: "Error Time Format")
            return
        }
        guard let endTime = tfEndTime.text!.slotTimeTodate() else {
            showSnackbar(withMessage: "Error Time Format")
            return
        }
        if !isSlotAvailable(startTime: startTime, endTime: endTime).0{
            return
        }
        switch selecedSlot {
        case .icu:
            viewModel.icuSlots.append(SlotDataModel(startTime: tfStartTime.text!, endTime: tfEndTime.text!, price: tfPrice.text!, isDayChanger: isSlotAvailable(startTime: startTime, endTime: endTime).1))
        case .emergency:
            viewModel.emergencySlots.append(SlotDataModel(startTime: tfStartTime.text!, endTime: tfEndTime.text!, price: tfPrice.text!, isDayChanger: isSlotAvailable(startTime: startTime, endTime: endTime).1))
            
        default:
            ()
        }
        tfStartTime.text = ""
        tfEndTime.text = ""
        tfPrice.text = ""
        tableView.reloadData()
    }
    
    func isSlotAvailable(startTime: Date, endTime:Date) -> (Bool,Bool) {
    
        let calendar = Calendar.current
        let now = Date()
        
        let currentStartHour = startTime.component(.hour)!
        let currentStartMin = startTime.component(.minute)!
        let currentEndHour = endTime.component(.hour)!
        let currentEndMin = endTime.component(.minute)!
        
        let startNew = calendar.date(
          bySettingHour: currentStartHour,
          minute: currentStartMin,
          second: 0,
          of: now)!

        let endNew = calendar.date(
          bySettingHour: currentEndHour,
          minute: currentEndMin,
          second: 0,
          of: now)!
        
        
        if (selecedSlot == .emergency ? viewModel.emergencySlots : viewModel.icuSlots).isEmpty {
            
            if startNew < endNew {
                return (true ,false)
            } else {
                return (true, true)
            }
        }
        for item in selecedSlot == .emergency ? viewModel.emergencySlots : viewModel.icuSlots{
            let bookedStartHour = item.startTime.slotTimeTodate()!.component(.hour)!
            let bookedStartMin = item.startTime.slotTimeTodate()!.component(.minute)!
            let bookedEndHour = item.endTime.slotTimeTodate()!.component(.hour)!
            let bookedEndMin = item.endTime.slotTimeTodate()!.component(.minute)!
            
            let startOld = calendar.date(
              bySettingHour: bookedStartHour,
              minute: bookedStartMin,
              second: 0,
              of: now)!

            let endOld = calendar.date(
              bySettingHour: bookedEndHour,
              minute: bookedEndMin,
              second: 0,
              of: now)!
            
            if isSpecailSlotThere() {
    
            if isSpecialSlot(slotStartTime: startOld, slotEndTime: endOld) {
                if (startNew < startOld && startNew < endOld) || (startNew > startOld && startNew > endOld) || !(startNew <= startOld && startNew >= endOld){
                    showSnackbar(withMessage: "start date failed special check")
                      return (false,false)
                } else if (endNew < startOld && endNew < endOld) || (endNew > startOld && endNew > endOld) || !(endNew <= startOld && endNew >= endOld){
                        showSnackbar(withMessage: "end date failed special check")
                          return (false,false)
                } else {
                    if isSpecialSlot(slotStartTime: startNew, slotEndTime: endNew) {
                        showSnackbar(withMessage: "not allowed")
                        return (false,false)
                    } else if normalConditionCheck2(slotStartTime: startOld, slotEndTime: endOld, enteredStartTime: startNew, enteredEndTime: endNew) == false {
                        return (false,false)
                    }
                }
            } else if normalConditionCheck(slotStartTime: startOld, slotEndTime: endOld, enteredStartTime: startNew, enteredEndTime: endNew) == false {
                        return (false,false)
                }
            } else {
                if isSpecialSlot(slotStartTime: startNew, slotEndTime: endNew) {
                    return (true,true)
                } else if normalConditionCheck(slotStartTime: startOld, slotEndTime: endOld, enteredStartTime: startNew, enteredEndTime: endNew) == false {
                        return (false,false)
                }
            }
            

        }
        
        return (true,false)
    }
    func normalConditionCheck2(slotStartTime : Date, slotEndTime: Date, enteredStartTime: Date, enteredEndTime: Date) -> Bool {
        if enteredStartTime >= slotStartTime &&
            enteredEndTime <= slotEndTime
        {
          showSnackbar(withMessage: "The time slot is already booked")
            return false
        }
        
        if enteredStartTime >= slotStartTime && enteredStartTime < slotEndTime
        {
            showSnackbar(withMessage: "The slot timing starts in another time slot")
            return false
        }

        if enteredEndTime > slotStartTime && enteredEndTime <= slotEndTime
        {
            showSnackbar(withMessage: "The slot timing end in another time slot")
            return false
        }
        return true
    }
    func normalConditionCheck(slotStartTime : Date, slotEndTime: Date, enteredStartTime: Date, enteredEndTime: Date) -> Bool {
        if enteredStartTime >= slotStartTime &&
            enteredEndTime <= slotEndTime
        {
          showSnackbar(withMessage: "The time slot is already booked")
            return false
        }

        if enteredStartTime <= slotStartTime &&
            enteredEndTime >= slotEndTime
        {
            showSnackbar(withMessage: "The complete slot is not available for booking ")
            return false
        }
        
        if enteredStartTime >= slotStartTime && enteredStartTime < slotEndTime
        {
            showSnackbar(withMessage: "The slot timing starts in another time slot")
            return false
        }

        if enteredEndTime > slotStartTime && enteredEndTime <= slotEndTime
        {
            showSnackbar(withMessage: "The slot timing end in another time slot")
            return false
        }
        return true
    }
    func isSpecailSlotThere() -> Bool {
        let specialSlot = (selecedSlot == .emergency ? viewModel.emergencySlots : viewModel.icuSlots).filter({$0.isDayChanger == true})
        if specialSlot.count != 0{
            return true
        } else {
            return false
        }
    }
    func isSpecialSlot(slotStartTime : Date, slotEndTime: Date ) -> Bool {
        if  slotStartTime >= slotEndTime {
            return true
        }
        return false
    }
    
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        sender.showSpinner()
        viewModel.register()
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension DefaultSlotVC {
    class func loadFromXIB(withDependency dependency: DefaultSlotDependency? = nil) -> DefaultSlotVC? {
        let storyboard = UIStoryboard(name: "DefaultSlot", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "DefaultSlotVC") as? DefaultSlotVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - DefaultSlotAPIResponseDelegate
extension DefaultSlotVC: DefaultSlotAPIResponseDelegate {
    func handleAPIError(_ error: Error) {
        btnSave.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        btnSave.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
        let dependency = DefaultSlotNurseDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = DefaultSlotNurseVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension DefaultSlotVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selecedSlot {
        case .icu:
            return viewModel.icuSlots.count
        case .emergency:
            return viewModel.emergencySlots.count
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SlotDefaultTVC") as? SlotDefaultTVC else { return UITableViewCell() }
        cell.index = indexPath.row
        cell.btnEdit.isHidden = true
        cell.delegate = self
        switch selecedSlot {
        case .icu:
            let item = viewModel.icuSlots[indexPath.row]
            cell.lblSlotTime.text = "\(item.startTime) - \(item.endTime)"
        case .emergency:
            let item = viewModel.emergencySlots[indexPath.row]
            cell.lblSlotTime.text = "\(item.startTime) - \(item.endTime)"
        default:
            ()
        }
        return cell
    }
    
    
}

extension DefaultSlotVC: EmptyDataSetSource,EmptyDataSetDelegate {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let mainTitle = NSAttributedString.init(string: "No Slot Created",attributes: [NSAttributedString.Key.font : UIFont.sfProTextSemibold(ofSize: 15),NSAttributedString.Key.foregroundColor: UIColor.black])
        return mainTitle
    }
}

extension DefaultSlotVC: ModifySlotDelegate {
    
    func deleteSlotAt(index: Int) {
        switch selecedSlot {
        case .icu:
            viewModel.icuSlots.remove(at: index)
            tableView.reloadData()
        case .emergency:
            viewModel.emergencySlots.remove(at: index)
            tableView.reloadData()
        default:
            ()
        }
    }
    
    
}
