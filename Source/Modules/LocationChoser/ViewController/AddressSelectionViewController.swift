//
//  AddressSelectionViewController.swift
//  Source
//
//  Created by Techwens Software on 31/08/23.
//

import UIKit
import GooglePlaces
class AddressSelectionViewController: UIViewController {

    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var locationBtn: UIButton!
    @IBOutlet var labelLocation: UILabel!
    @IBOutlet var tfSearch: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        addCornerRedius()
        // Do any additional setup after loading the view.
     
    }
    
    @objc func openGooglePlaceAutoComplete (){
        tfSearch.resignFirstResponder()
           let acController = GMSAutocompleteViewController()
           acController.delegate = self
           present(acController, animated: true, completion: nil)
    }
    
    @IBAction func onSearchClicked(_ sender: Any) {
        tfSearch.resignFirstResponder()
           let acController = GMSAutocompleteViewController()
           acController.delegate = self
           present(acController, animated: true, completion: nil)
    }
    
    
    func addCornerRedius () {
        closeBtn.layer.cornerRadius = 20
        locationBtn.layer.cornerRadius = 20
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddressSelectionViewController {
    class func loadFromXIB() -> AddressSelectionViewController? {
        let storyboard = UIStoryboard(name: "AddressSelection", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "AddressSelectionViewController") as? AddressSelectionViewController else {
            return nil
        }
      
        return viewController
    }
}
extension AddressSelectionViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
      tfSearch.text = place.name
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
