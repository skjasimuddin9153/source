//
//  LocationTVHeader.swift
//  Source
//
//  Created by Techwens on 24/07/22.
//

import UIKit

class LocationTVHeader: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    class func instanceFromNib() -> LocationTVHeader {
        return UINib(nibName: "LocationTVHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LocationTVHeader
    }

}
