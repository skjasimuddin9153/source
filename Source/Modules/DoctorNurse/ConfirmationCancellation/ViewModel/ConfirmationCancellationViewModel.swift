//
//  ConfirmationCancellationViewModel.swift
//  Source
//
//  Created by Techwens on 06/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol ConfirmationCancellationAPIResponseDelegate: class {
}

class ConfirmationCancellationViewModel {
    weak var apiResponseDelegate: ConfirmationCancellationAPIResponseDelegate?
    lazy var localDataManager = ConfirmationCancellationLocalDataManager()
    lazy var apiDataManager = ConfirmationCancellationAPIDataManager()
    
    var dependency: ConfirmationCancellationDependency?
    
    init() {
    }
    // Data fetch service methods goes here
}
