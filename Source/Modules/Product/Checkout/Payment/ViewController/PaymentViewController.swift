//
//  PaymentViewController.swift
//  Source
//
//  Created by Techwens Software on 08/09/23.
//

import UIKit
import SwiftSignatureView
class PaymentViewController: UIViewController {
   
    @IBOutlet var signaturePad: SwiftSignatureView!
    override func viewDidLoad() {
        super.viewDidLoad()
        signaturePad.signature
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnClearSign(_ sender: Any) {
        signaturePad.clear()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentViewController {
    
    class func loadFromXIB() -> PaymentViewController? {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as? PaymentViewController else {
            return nil
        }
        return viewController
    }
    
}
