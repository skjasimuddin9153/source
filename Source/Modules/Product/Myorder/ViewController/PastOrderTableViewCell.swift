//
//  PastOrderTableViewCell.swift
//  Source
//
//  Created by Techwens Software on 07/09/23.
//

import UIKit

class PastOrderTableViewCell: UITableViewCell {

    @IBOutlet var productPastBtn: UIButton!
    @IBOutlet var reorderBox: UIView!
    @IBOutlet var productReorderBtn: UIButton!

    
    @IBOutlet var orderId: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var itemCount: UILabel!
    var onPastProductClicked : (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        style()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        productPastBtn.addTarget(self, action: #selector(productSelect), for: .touchUpInside)
    }
    
    @objc private func productSelect (){
        onPastProductClicked?()
    }
    private func style(){
        reorderBox.borderWidth = 0.5
        reorderBox.borderColor = .gray
        productReorderBtn.allCornersRadius = 20
    }
  
    
}
