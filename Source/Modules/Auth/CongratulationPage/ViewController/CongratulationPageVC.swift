//
//  CongratulationPageVC.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class CongratulationPageVC: UIViewController {
    // MARK: Instance variables
	lazy var viewModel = CongratulationPageViewModel()
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}

// MARK: - Load from storyboard with dependency
extension CongratulationPageVC {
    class func loadFromXIB(withDependency dependency: CongratulationPageDependency? = nil) -> CongratulationPageVC? {
        let storyboard = UIStoryboard(name: "CongratulationPage", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CongratulationPageVC") as? CongratulationPageVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - CongratulationPageAPIResponseDelegate
extension CongratulationPageVC: CongratulationPageAPIResponseDelegate {
}
