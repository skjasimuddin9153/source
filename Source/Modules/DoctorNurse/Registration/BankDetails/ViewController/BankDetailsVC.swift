//
//  BankDetailsVC.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum BankDetailsDropdown{
    case bankName
}

class BankDetailsVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var tfBankName: UITextField!
    @IBOutlet weak var tfBankAddress: UITextField!
    @IBOutlet weak var tfAccountNo: UITextField!
    @IBOutlet weak var tfIFSCCode: UITextField!
    @IBOutlet weak var btnSend: AppSolidButton!
    lazy var viewModel = BankDetailsViewModel()
    var acticeDropDown: BankDetailsDropdown?
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        view.showLoader(withBlur: true)
        view.isUserInteractionEnabled = false
        viewModel.getBankList()
        tfBankName.setupSourceTextField(withPlaceholder: "Bank Name")
        tfBankAddress.setupSourceTextField(withPlaceholder: "Account Holder Name")
        tfAccountNo.setupSourceTextField(withPlaceholder: "Account Number*")
        tfIFSCCode.setupSourceTextField(withPlaceholder: "IFSC Code*")
        tfBankName.delegate = self
        tfBankName.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfBankName.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedBankNameDropDown()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        guard let bankCode = viewModel.bankList.filter({$0.name?.lowercased() == tfBankName.text?.lowercased()}).first?.code else {
            showSnackbar(withMessage: "Bank Name is Invalid")
            tfBankName.shake()
            return
        }
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.register(prospect_id:"\(viewModel.dependency!.prospect_id!)", bankName: bankCode, account_holder_name: tfBankAddress.text!, account_number: tfAccountNo.text!, ifsc_code: tfIFSCCode.text!)
    }
    func tappedBankNameDropDown() {
        acticeDropDown = .bankName
        let dependency = PickerViewDependency(title: "Select Bank", dataList: viewModel.bankList.compactMap({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
    
    func allFieldsValid() -> Bool {
        if viewModel.dependency?.prospect_id == nil {
            showSnackbar(withMessage: "Prospect Not Found")
            return false
        }
        if (tfBankName.text?.isEmpty ?? true) || tfBankName.text == ""{
            showSnackbar(withMessage: "Bank Name is Invalid")
            tfBankName.shake()
            return false
        }
        if !viewModel.bankList.contains(where: {$0.name?.lowercased() == tfBankName.text?.lowercased() ?? ""}) {
            showSnackbar(withMessage: "State Code is Invalid")
            tfBankName.shake()
            return false
        }
        if  (tfBankAddress.text?.isEmpty ?? true) || tfBankAddress.text == "" {
            showSnackbar(withMessage: "Account Holder Name is Invalid")
            tfBankAddress.shake()
            return false
        }
        if  (tfAccountNo.text?.isEmpty ?? true) || tfAccountNo.text == "" {
            showSnackbar(withMessage: "Account No is Invalid")
            tfAccountNo.shake()
            return false
        }
        if  (tfIFSCCode.text?.isEmpty ?? true) || tfIFSCCode.text == "" {
            showSnackbar(withMessage: "IFSC codeo is Invalid")
            tfIFSCCode.shake()
            return false
        }
        return true
    }
}

// MARK: - Load from storyboard with dependency
extension BankDetailsVC {
    class func loadFromXIB(withDependency dependency: BankDetailsDependency? = nil) -> BankDetailsVC? {
        let storyboard = UIStoryboard(name: "BankDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "BankDetailsVC") as? BankDetailsVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - BankDetailsAPIResponseDelegate
extension BankDetailsVC: BankDetailsAPIResponseDelegate {
    func handleBankResponse() {
        view.dismissLoader()
        view.isUserInteractionEnabled = true
    }
    
   
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
    func handleAPIMessage(message: String) {
        btnSend.hideSpinner()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: message)
        let dependency = LocationDetailsDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = LocationDetailsVC.loadFromXIB(withDependency: dependency) else { return }
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension BankDetailsVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case tfBankName :
            tappedBankNameDropDown()
            return false
        default:
            return true
        }
    }
}

extension BankDetailsVC: SelectInfoFromPickerDelegate {
    func didInfoSelected(value: String, index: Int) {
        switch acticeDropDown {
        case .bankName:
            tfBankName.text = value
        default:
            acticeDropDown = nil
        }
        acticeDropDown = nil
    }
    
    func noSelection() {
        acticeDropDown = nil
    }
}
