//
//  CartDependencies.swift
//  Source
//
//  Created by Techwens Software on 04/09/23.
//


import UIKit

struct CartProduct {
    let id : Int
    let productTitle:String
    let productSubTitle:String
    let productImage:UIImage
    let productPrice:String
    var itemCount : Int
}
