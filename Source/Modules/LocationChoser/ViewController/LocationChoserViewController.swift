//
//  LocationChoserViewController.swift
//  Source
//
//  Created by Techwens Software on 13/09/23.
//

import UIKit
import GooglePlaces
class LocationChoserViewController: UIViewController {


    @IBOutlet var searchBox: UIView!
    @IBOutlet var tvAutoCompletePlace: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBox.borderColor = .black
        searchBox.borderWidth = 1
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onSearchClicked(_ sender: Any) {
        tvAutoCompletePlace.resignFirstResponder()
           let acController = GMSAutocompleteViewController()
           acController.delegate = self
           present(acController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LocationChoserViewController: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
      tvAutoCompletePlace.text = place.name
// Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}
