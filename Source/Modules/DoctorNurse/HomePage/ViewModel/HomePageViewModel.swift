//
//  HomePageViewModel.swift
//  Source
//
//  Created by Techwens on 22/07/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

enum ListingState {
    case favourite
    case local
    case both
    case none
}

protocol HomePageAPIResponseDelegate: AnyObject {
    func handleFavHospitaListResponse()
    func handleGetHospitaListResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

protocol FavouriteNavigationDelegate: AnyObject {
    func tappedHospitalFor(index: Int)
}
class HomePageViewModel {
    weak var apiResponseDelegate: HomePageAPIResponseDelegate?
    weak var favouriteNavigationDelegate: FavouriteNavigationDelegate?
    lazy var localDataManager = HomePageLocalDataManager()
    lazy var apiDataManager = HomePageAPIDataManager()
    
    var dependency: HomePageDependency?
    var favouriteHospital: [FavouriteModel] = []
    var currentAreaHospital: [HospitalsModel] = []
    var favouriteNavigationCompletion: ((Int) -> Void) = {_ in}

    
    init() {
    }
    // Data fetch service methods goes here
    
    func getExploreHospitalData() {
        apiDataManager.getHospitalList { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.currentAreaHospital = data.hospitals ?? []
                    weakSelf.apiResponseDelegate?.handleGetHospitaListResponse()
                    weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }

            }
        }
    }
    
    func getFavouriteHospitalData() {
        apiDataManager.getFavouriteList{ [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.favouriteHospital = data
                    weakSelf.apiResponseDelegate?.handleFavHospitaListResponse()

                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }

            }
        }
    }
}

// MARK: - TABLE VIEW DATA VIEWMODEL FUNCTIONS
extension HomePageViewModel {
    
    
    func currentTableState() -> ListingState {
        if favouriteHospital.isEmpty && currentAreaHospital.isEmpty {
            return .none
        } else if !favouriteHospital.isEmpty && currentAreaHospital.isEmpty{
            return .favourite
        } else if favouriteHospital.isEmpty && !currentAreaHospital.isEmpty {
            return .local
        } else {
            return .both
        }
    }
    
    func heightForSection(section: Int) -> CGFloat {
        switch currentTableState() {
        case .both:
            if section == 0 {
                return 0.0
            } else {
                return 44.0
            }
        case .local:
            return 44.0
        case .favourite:
            return 0.0
        case .none:
            return 0.0
        }
    }
    func cellForRowAt(tableView: UITableView,indexPath: IndexPath) -> UITableViewCell {
        switch currentTableState() {
        case .both:
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0:
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFavouriteTVC") as? HomeFavouriteTVC else { return UITableViewCell() }
                    cell.favouriteHospitalList = favouriteHospital
                    cell.tappedFavHospitalCompletion = { [unowned self] index in
                        print(" \(index) tapped")
                        favouriteNavigationDelegate?.tappedHospitalFor(index: index)
                    }
                    return cell
                default:
                    return UITableViewCell()
                }
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHospitalTVC") as? HomeHospitalTVC else { return UITableViewCell()}
                cell.hospitalData = currentAreaHospital[indexPath.row]
                return cell
            default:
                return UITableViewCell()
            }
        case .local:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeHospitalTVC") as? HomeHospitalTVC else { return UITableViewCell()}
            cell.hospitalData = currentAreaHospital[indexPath.row]
            return cell
        case .favourite:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFavouriteTVC") as? HomeFavouriteTVC else { return UITableViewCell() }
            cell.favouriteHospitalList = favouriteHospital
            cell.tappedFavHospitalCompletion = { [unowned self] index in
                print(" \(index) tapped")
                favouriteNavigationDelegate?.tappedHospitalFor(index: index)
            }
            return cell
        case .none:
            return UITableViewCell()
        }
    }
    
    func numberOfRowInSection(section: Int) -> Int {
        switch currentTableState() {
        case .both:
            if section == 0 {
                return 1
            } else {
                return currentAreaHospital.count
            }
        case .local:
            return currentAreaHospital.count
        case .favourite:
            return 1
        case .none:
            return 0
        }
    }
    
    func numberOfSection() -> Int {
        switch currentTableState() {
        case .both:
            return 2
        case .local:
            return 1
        case .favourite:
            return 1
        case .none:
            return 0
        }
    }
    
    func viewForHeaderInSection(section: Int) -> UIView? {
        switch currentTableState() {
        case .both:
            if section == 1 {
                let header = HomeTVHeader.instanceFromNib()
                return header
            } else {
                return nil
            }
        case .local:
            let header = HomeTVHeader.instanceFromNib()
            return header
        case .favourite:
            return UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: .leastNonzeroMagnitude))
        case .none:
            return nil
        }
    }
    
}
