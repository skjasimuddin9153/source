//
//  BookingTicketViewModel.swift
//  Source
//
//  Created by Techwens on 08/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import UIKit

protocol BookingTicketAPIResponseDelegate: class {
    func handleGetHospitaDetailsResponse(data: HospitalDetailsResponseModel)
    func handleBookingMessageResponse(message: String)
    func handleMarkAsFavResponse(button: UIButton,message: String)
    func handleRemoveFavResponse(button: UIButton,message: String)
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class BookingTicketViewModel {
    weak var apiResponseDelegate: BookingTicketAPIResponseDelegate?
    lazy var localDataManager = BookingTicketLocalDataManager()
    lazy var apiDataManager = BookingTicketAPIDataManager()
    
    var dependency: BookingTicketDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    
    func getHospitalDetailsData(hospitalId: Int) {
        let parameter =  ["hospital_id": hospitalId ,
                          ] as [String: AnyObject]
        apiDataManager.getHospitalDetails(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                if let data = response.data {
                    weakSelf.apiResponseDelegate?.handleGetHospitaDetailsResponse(data: data)
                  //  weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }else{
                    weakSelf.apiResponseDelegate?.handleAPIError(NetworkError.parseError)
                }

            }
        }
    }
    
    func submitBookingData(hospitalId: Int, slotDate: String, slotTime: Int) {
        let parameter =  ["user_id": hospitalId ,
                          "booking_start_date": slotDate ,
                          "slot_id": slotTime ,
                          ] as [String: AnyObject]
        apiDataManager.submitBookingData(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleBookingMessageResponse(message: response.message)

            }
        }
    }
    
    func markAsFavourite(hospitalId: Int, button: UIButton) {
        let parameter =  ["user_id": hospitalId ,
                          ] as [String: AnyObject]
        apiDataManager.addToFavourite(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleMarkAsFavResponse(button: button, message: response.message)

            }
        }
    }
    
    func removeFromFavourite(hospitalId: Int, button: UIButton) {
        let parameter =  ["user_id": hospitalId ,
                          ] as [String: AnyObject]
        apiDataManager.removeFromFavourite(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRemoveFavResponse(button: button, message: response.message)

            }
        }
    }
    
}
