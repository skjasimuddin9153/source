//
//  UITextfield.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//

import UIKit

extension UITextField {
        
    func setupSourceTextField(withPlaceholder placeholder: String, radius: Int? = nil, textSize: CGFloat? = nil) {
        self.borderStyle = .none
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.accentColor?.cgColor
        self.allCornersRadius = radius ?? 10
        let tfEmailPlaceholder = placeholder
        let myAttribute = [ NSAttributedString.Key.font: UIFont.sfProTextMedium(ofSize: textSize ?? 13),
                            NSAttributedString.Key.foregroundColor: UIColor.placeholderColor ?? .black ]
        let finalTextOne = NSAttributedString(string: tfEmailPlaceholder, attributes: myAttribute)
        self.attributedPlaceholder =  finalTextOne
        self.font = UIFont.sfProTextMedium(ofSize: textSize ?? 13)
        self.textColor = .black
        self.setLeftPaddingPoints(20)
        self.setRightPaddingPoints(20)
    }
    
    func setRightViewButton( with icon: UIImage) {
        self.setRightPaddingPoints(0)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height:self.frame.height))
        let btnView = UIButton(frame: view.frame)
        btnView.setImage(icon, for: .normal)
        btnView.isUserInteractionEnabled = false
        self.rightViewMode = .always
        view.addSubview(btnView)
        self.rightView = view
    }
    
    func setLeftViewButton( with icon: UIImage ) {
        self.setLeftPaddingPoints(0)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height:self.frame.height))
        let btnView = UIButton(frame: view.frame)
        btnView.setImage(icon, for: .normal)
        btnView.isUserInteractionEnabled = false
        self.leftViewMode = .always
        view.addSubview(btnView)
        self.leftView = view
    }
    func setLeftPaddingPoints(_ amount:CGFloat){
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.leftView = paddingView
           self.leftViewMode = .always
       }
       func setRightPaddingPoints(_ amount:CGFloat) {
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.rightView = paddingView
           self.rightViewMode = .always
       }
}

extension UITextField {
    
    func validateWith(regex: TextFieldValidationRegex) -> Bool {
        let predicate = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        if !predicate.evaluate(with: self.text?.trimmingCharacters(in: .whitespaces)) {
            self.shake()
        }
        return predicate.evaluate(with: self.text?.trimmingCharacters(in: .whitespaces))
    }
}


enum TextFieldValidationRegex: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    case phoneNo = "^[0-9]{8,10}$"
    case pin = "^[0-9]{4}$"
    case name = "[a-zA-Z ]{2,}"
    case docNumber = "[A-Z0-9-]{4,}"
    case pan =  "[A-Z]{5}[0-9]{4}[A-Z]{1}"
    
    var errorMessage: String {
        switch self {
        case .email:
            return "Email is invalid"
        case .name:
            return "Name is invalid"
        case .phoneNo:
            return "Phone number is invalid"
        case .pin:
            return "Pin is invalid"
        case .docNumber:
            return "Document Number is Invalid"
        case .pan:
            return "Pan is Invalid"
        }
    }
}

extension UITextField {
    
    func setInputViewYearPicker(maxDate : Date? = nil, minDate : Date? = nil) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.dataSource = self//2
        datePicker.delegate = self
 
        self.inputView = datePicker //3

        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: nil, action: #selector(tapDone)) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9

    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    @objc func tapDone() {
        self.resignFirstResponder()
    }
}


extension UITextField: UIPickerViewDelegate, UIPickerViewDataSource {
    var yearsTillNow : [String] {
        var years = [String]()
        for i in (1947...2022).reversed() {
            years.append("\(i)")
        }
        return years
    }
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return yearsTillNow.count
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return yearsTillNow[row]
    }

    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.text = yearsTillNow[row]
    }
    
}

extension UITextField {
    func setInputViewDatePicker(target: Any, selector: Selector, maxDate : Date? = nil, minDate : Date? = nil) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        self.inputView = datePicker //3

        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9

    }
    
    func setInputViewTimePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        
        datePicker.datePickerMode = .time//2
        datePicker.preferredDatePickerStyle = .wheels
        self.inputView = datePicker //3

        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9

    }
}
