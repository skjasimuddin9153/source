//
//  OwnerVerificationVC.swift
//  Source
//
//  Created by Techwens on 30/06/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import UIKit

class OwnerVerificationVC: UIViewController {
    // MARK: Instance variables
    @IBOutlet weak var btnSend: AppSolidButton!
    @IBOutlet weak var camBtnView: UIView!
    @IBOutlet weak var docImageView: UIImageView!
    @IBOutlet weak var tfDocument: UITextField!
    @IBOutlet weak var tfDocNumber: UITextField!
    @IBOutlet weak var tfPAN: UITextField!
	lazy var viewModel = OwnerVerificationViewModel()
    var acticeDropDown: VerificationDetailsDropdown?
    var imagePicker = UIImagePickerController()
    var selectedDocument: DocTypes?
    // MARK: - View Life Cycle Methods
	override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.apiResponseDelegate = self
        hideKeyboardWhenTappedAround()
        viewModel.apiResponseDelegate = self
        tfDocument.setupSourceTextField(withPlaceholder: "Document Type")
        tfDocNumber.setupSourceTextField(withPlaceholder: "Input document number")
        tfPAN.setupSourceTextField(withPlaceholder: "PAN Card Number")
        tfPAN.addTarget(self, action: #selector(textUpdate), for: .editingChanged)
        tfPAN.delegate = self
        tfPAN.keyboardType = .alphabet
        tfDocument.delegate = self
        tfDocNumber.autocapitalizationType = .allCharacters
        tfPAN.autocapitalizationType = .allCharacters
        tfDocument.setRightViewButton(with: UIImage(named: "icDropdown")!)
        tfDocument.rightView?.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.tappedDocumentTypeDropDown()
        }
        camBtnView.setOnClickListener { [weak self] in
            guard let welf = self else {
                return
            }
            welf.showPickerAlert()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @objc func textUpdate(textfield: UITextField){

        if let pan = textfield.text{
            if pan.count >= 5 && pan.count <= 8 {
                textfield.keyboardType = .numberPad
                textfield.reloadInputViews()
            } else {
                textfield.keyboardType = .alphabet
                textfield.reloadInputViews()
            }
        }
    }
    
    func allFieldsValid() -> Bool {
        if selectedDocument == nil {
            showSnackbar(withMessage: "Select a Document Type")
            return false
        }
        if !tfDocNumber.validateWith(regex: .docNumber) {
            showSnackbar(withMessage: TextFieldValidationRegex.docNumber.errorMessage)
            return false
        }
        if !tfPAN.validateWith(regex: .pan) {
            showSnackbar(withMessage: TextFieldValidationRegex.pan.errorMessage)
            return false
        }
        guard let data = docImageView.image?.jpegData(compressionQuality: 0.0) else {
            showSnackbar(withMessage: "Add Document Image to continue")
            return false
        }
        if !isFileSizeAllowed(imageData: data) {
            showSnackbar(withMessage: "Image is too large")
            docImageView.shake()
            return false
        }
        return true
    }
    
    func tappedDocumentTypeDropDown() {
        print("Open drop down one")
        acticeDropDown = .documentType
        let dependency = PickerViewDependency(title: "Select Document Type", dataList:  viewModel.documents.map({$0.name}))
        guard let picker = PickerViewVC.loadFromXIB(withDependency: dependency) else { return }
        picker.delegate = self
        picker.modalPresentationStyle = .overFullScreen
        present(picker, animated: true)
    }
    @IBAction func tappedSaveBtn(_ sender: AppSolidButton) {
        if !allFieldsValid() {
           return
        }
        guard let docImage = docImageView.image?.jpegData(compressionQuality: 0.0) else {
            return
        }
        print(docImage)
        sender.showSpinner()
        view.isUserInteractionEnabled = false
        viewModel.registerStepTwo(with: docImage, prospect_id: viewModel.dependency!.prospect_id!, panNumber: tfPAN.text!, docType: selectedDocument!, docNumber: tfDocNumber.text!)
      
    }
    @IBAction func tappedBackBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func tappedLoginBtn(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    // MARK: Deinitialization
    deinit {
       debugPrint("\(self) deinitialized")
    }
}

// MARK: - Load from storyboard with dependency
extension OwnerVerificationVC {
    class func loadFromXIB(withDependency dependency: OwnerVerificationDependency? = nil) -> OwnerVerificationVC? {
        let storyboard = UIStoryboard(name: "OwnerVerification", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "OwnerVerificationVC") as? OwnerVerificationVC else {
            return nil
        }
        viewController.viewModel.dependency = dependency
        return viewController
    }
}

// MARK: - OwnerVerificationAPIResponseDelegate
extension OwnerVerificationVC: OwnerVerificationAPIResponseDelegate {
    func handleRegisterationStep2Response(message: String) {
        btnSend.hideSpinner()
        view.dismissLoader()
        showSnackbar(withMessage: message)
        view.isUserInteractionEnabled = true
        let depenedency = RegistrationDocsDependency(prospect_id: viewModel.dependency?.prospect_id)
        guard let vc = RegistrationDocsVC.loadFromXIB(withDependency: depenedency) else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleAPIError(_ error: Error) {
        btnSend.hideSpinner()
        view.dismissLoader()
        view.isUserInteractionEnabled = true
        showSnackbar(withMessage: error.localizedDescription)
    }
    
}

extension OwnerVerificationVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case tfDocument :
            tappedDocumentTypeDropDown()
            return false

        default:
            return true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case tfPAN :
            let maxLength = 10
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        default:
            return true
        }
       
    }
}

extension OwnerVerificationVC: SelectInfoFromPickerDelegate {
    func didInfoSelected(value: String, index: Int) {
        switch acticeDropDown {
        case .documentType:
            tfDocument.text = value
            selectedDocument = viewModel.documents[index]
        case .none:
            ()
        }
        acticeDropDown = nil
    }
    
    func noSelection() {
        acticeDropDown = nil
    }
}



extension OwnerVerificationVC {
    func openPicker(type: UIImagePickerController.SourceType) {
            imagePicker.sourceType = type
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = .currentContext
            imagePicker.delegate = self
            present(imagePicker, animated: true)
        
        
    }
    
    func showPickerAlert(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default , handler:{ [self] _ in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                openPicker(type: .camera)
            }
            else{
                showSnackbar(withMessage: "No Camera Available On This Device")
            }
            
        }))
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default , handler:{ [self] _ in
            openPicker(type: .photoLibrary)
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ _ in
            
        }))
        
       present(alert, animated: true, completion: nil)
    }
}

extension OwnerVerificationVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        docImageView.image = tempImage
        self.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
