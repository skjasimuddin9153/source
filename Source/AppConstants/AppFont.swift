//
//  AppFont.swift
//  Source
//
//  Created by Techwens on 12/06/22.
//

import UIKit

extension UIFont {

    @objc class func comfortaaRegular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Comfortaa-Regular", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func  comfortaaMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Comfortaa-Medium", size: size) ?? .italicSystemFont(ofSize: size)
    }
    @objc class func comfortaaBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Comfortaa-Bold", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func comfortaaLight(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Comfortaa-Light", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func comfortaaSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Comfortaa-SemiBold", size: size) ?? .systemFont(ofSize: size)
    }
    
    @objc class func poppinsRegular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Regular", size: size) ?? .italicSystemFont(ofSize: size)
    }

    @objc class func poppinsMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Medium", size: size) ?? .systemFont(ofSize: size)
    }

    @objc class func poppinsBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-Bold", size: size) ?? .italicSystemFont(ofSize: size)
    }
    @objc class func poppinsSemiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Poppins-SemiBold", size: size) ?? .italicSystemFont(ofSize: size)
    }

    @objc class func sfProTextRegular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Regular", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func  sfProTextLight(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Light", size: size) ?? .italicSystemFont(ofSize: size)
    }
    @objc class func sfProTextMedium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Medium", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func sfProTextSemibold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Semibold", size: size) ?? .systemFont(ofSize: size)
    }
    @objc class func sfProTextBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Bold", size: size) ?? .systemFont(ofSize: size)
    }
}
