//
//  PasswordSetupViewModel.swift
//  Source
//
//  Created by Techwens on 07/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol PasswordSetupAPIResponseDelegate: class {
    func handleRegisterResponse()
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class PasswordSetupViewModel {
    weak var apiResponseDelegate: PasswordSetupAPIResponseDelegate?
    lazy var localDataManager = PasswordSetupLocalDataManager()
    lazy var apiDataManager = PasswordSetupAPIDataManager()
    
    var dependency: PasswordSetupDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func register(prospect_id: String, pin: String, c_pin: String) {
        let parameters = [
            "prospect_id":prospect_id,
            "step":dependency?.userType == .hospital ? "5" : "6",
            "pin":pin,
            "confirm_pin":c_pin
        ]
        switch dependency?.userType {
        case.hospital:
            apiDataManager.hospitalregisterAPI(with: parameters, completion: { [weak self] response in
                guard let weakSelf = self else {
                    return
                }
                switch response {
                case .failure(let error):
                    weakSelf.apiResponseDelegate?.handleAPIError(error)
                case .success(let response):
                    weakSelf.apiResponseDelegate?.handleRegisterResponse()
                    weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }
                
            })
        default:
            apiDataManager.registerAPI(with: parameters, completion: { [weak self] response in
                guard let weakSelf = self else {
                    return
                }
                switch response {
                case .failure(let error):
                    weakSelf.apiResponseDelegate?.handleAPIError(error)
                case .success(let response):
                    weakSelf.apiResponseDelegate?.handleRegisterResponse()
                    weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
                }
                
            })
            
        }

    }
    

}
