//
//  ResetPinViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol ResetPinAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class ResetPinViewModel {
    weak var apiResponseDelegate: ResetPinAPIResponseDelegate?
    lazy var localDataManager = ResetPinLocalDataManager()
    lazy var apiDataManager = ResetPinAPIDataManager()
    
    var dependency: ResetPinDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func resetPin(type: FindType,withEmail email: String, pin: String, c_pin: String) {
        var parameter : [String: String] = [:]
        switch type {
        case .email:
            parameter["email"] = email
        case .phone:
            parameter["phone"] = email
        }
        parameter["pin"] = pin
        parameter["confirm_pin"] = c_pin
        apiDataManager.resetPinAPI(with: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)
            }
            
        })
    }
}
