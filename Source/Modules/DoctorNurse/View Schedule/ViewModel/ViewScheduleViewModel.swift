//
//  ViewScheduleViewModel.swift
//  Source
//
//  Created by Techwens on 07/09/22.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation
import UIKit

protocol ViewScheduleAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleMarkAsFavResponse(button: UIButton,message: String)
    func handleRemoveFavResponse(button: UIButton,message: String)
    func handleAPIMessage(message:String)
}

class ViewScheduleViewModel {
    weak var apiResponseDelegate: ViewScheduleAPIResponseDelegate?
    lazy var localDataManager = ViewScheduleLocalDataManager()
    lazy var apiDataManager = ViewScheduleAPIDataManager()
    
    var dependency: ViewScheduleDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func cancelBookingData(bookingId: Int) {
        let parameter =  ["booking_id": bookingId
                          ] as [String: AnyObject]
        apiDataManager.cancelBookingData(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)

            }
        }
    }
    
    func markAsFavourite(hospitalId: Int, button: UIButton) {
        let parameter =  ["user_id": hospitalId ,
                          ] as [String: AnyObject]
        apiDataManager.addToFavourite(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleMarkAsFavResponse(button: button, message: response.message)

            }
        }
    }
    
    func removeFromFavourite(hospitalId: Int, button: UIButton) {
        let parameter =  ["user_id": hospitalId ,
                          ] as [String: AnyObject]
        apiDataManager.removeFromFavourite(parameter:parameter) { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleRemoveFavResponse(button: button, message: response.message)

            }
        }
    }
}
