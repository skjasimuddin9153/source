//
//  MyOrderViewController.swift
//  Source
//
//  Created by Techwens Software on 07/09/23.
//

struct OrderProduct {
    
    let orderId:String
    let price:String
    let itemsCount:String
}

import UIKit

class MyOrderViewController: UIViewController {

    @IBOutlet var headerView: HeaderView!
    @IBOutlet var tableView: UITableView!
  
    @IBOutlet var tableheight: NSLayoutConstraint!
    @IBOutlet var productImage1: UIImageView!
    @IBOutlet var productFontImage: UIImageView!
    var orderProduct :[OrderProduct] = [
    OrderProduct(
    orderId: "Order #1234", price: "$175.41 ", itemsCount: "2 item"
    ),
    OrderProduct(
    orderId: "Order #1234", price: "$175.41 ", itemsCount: "2 item"
    ),
    OrderProduct(
    orderId: "Order #1234", price: "$175.41 ", itemsCount: "2 item"
    )
    ]

    override func viewDidLoad() {
     
        super.viewDidLoad()
        tableView.isUserInteractionEnabled = true
        headerView.navigationController = self.navigationController
        headerView.rightBox.isHidden = true
        headerView.title.text = "Order"
      
style()
        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func currentOrderBtnClicked(_ sender: Any) {
        guard let vc = CurrentOrderViewController.loadFromXIB() else {return}
        self.present(vc,animated:true)
    }
    
    private func style(){
        tableView.indicatorStyle = .white
        tableView.separatorStyle = .none
        productImage1.allCornersRadius = 20
        productImage1.borderColor = .white
        productImage1.borderWidth = 1
        productFontImage.borderColor = .white
        productFontImage.borderWidth = 1
        productFontImage.layer.cornerRadius = 20
        tableheight.constant = CGFloat(110 * orderProduct.count)
    }
}

extension MyOrderViewController {
    class func loadFromXIB() -> MyOrderViewController? {
        let storyboard = UIStoryboard(name: "MyOrder", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "MyOrderViewController") as? MyOrderViewController else {
            return nil
        }
    
        return viewController
    }
}

extension MyOrderViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        orderProduct.count
    }
    
  
     
 

    


    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  let cell :PastOrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PastOrderTableViewCell
        cell.onPastProductClicked = {
            guard let vc = PastOrderViewController.loadFromXIB() else {return}
            self.present(vc,animated:true)
        }
        cell.orderId.text = orderProduct[indexPath.row].orderId
        cell.price.text = orderProduct[indexPath.row].price
        cell.itemCount.text = orderProduct[indexPath.row].itemsCount
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard   let vc = ProductDetailsViewController.loadFromXIB() else {return}
        navigationController?.pushViewController(vc, animated: true)
    }
}
