//
//  MyCartTableViewCell.swift
//  Source
//
//  Created by Techwens Software on 05/09/23.
//

import UIKit

class MyCardTableViewCell: UITableViewCell {
    var onDeleteCard : (() -> Void)?
    var onCardClicked : (() -> Void)?
    @IBOutlet var cardBox: UIView!
    @IBOutlet var cardBtn: UIButton!
    @IBOutlet var cardLogo: UIImageView!
    @IBOutlet var cardNo: UILabel!
    @IBOutlet var cardSubLabel: UILabel!
    @IBOutlet var cardDeleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cardDeleteBtn.addTarget(self, action: #selector(onDeleteTapped), for: .touchUpInside)
        cardBtn.addTarget(self, action: #selector(onCardClickedtap), for: .touchUpInside)
        
        style()
    }

    
    private func style(){
        cardBox.borderColor = .black
        cardBox.borderWidth = 0.3
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     
        // Configure the view for the selected state
    }

    @objc private func onDeleteTapped (){
        onDeleteCard?()
    }
    @objc private func onCardClickedtap (){
        onCardClicked?()
    }
}
