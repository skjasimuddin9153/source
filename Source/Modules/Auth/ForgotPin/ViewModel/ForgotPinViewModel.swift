//
//  ForgotPinViewModel.swift
//  Source
//
//  Created by Techwens on 08/08/23.
//  Copyright © 2023 ___Techwens___. All rights reserved.
//

import Foundation

protocol ForgotPinAPIResponseDelegate: class {
    func handleAPIError(_ error: Error)
    func handleAPIMessage(message:String)
}

class ForgotPinViewModel {
    weak var apiResponseDelegate: ForgotPinAPIResponseDelegate?
    lazy var localDataManager = ForgotPinLocalDataManager()
    lazy var apiDataManager = ForgotPinAPIDataManager()
    
    var dependency: ForgotPinDependency?
    
    init() {
    }
    // Data fetch service methods goes here
    func forgotPassword(type: FindType,withEmail email: String) {
        var parameter : [String: String] = [:]
        switch type {
        case .email:
            parameter["email"] = email
        case .phone:
            parameter["phone"] = email
        }
        apiDataManager.forgotPasswordAPI(with: parameter, completion: { [weak self] response in
            guard let weakSelf = self else {
                return
            }
            switch response {
            case .failure(let error):
                weakSelf.apiResponseDelegate?.handleAPIError(error)
            case .success(let response):
                weakSelf.apiResponseDelegate?.handleAPIMessage(message: response.message)                
            }
            
        })
    }
    func validateUsername(_ username: String) -> Bool {
        // Validate the username based on your criteria
        // For example, require username to start with "@" and contain at least one letter and one digit
        // Additionally, no special characters like , and .
        let usernamePattern = "^@(?=.*[A-Za-z])(?=.*\\d)(?!.*[,.@  %'/*#^!`]).+$"
        let usernamePredicate = NSPredicate(format: "SELF MATCHES %@", usernamePattern)
        return usernamePredicate.evaluate(with: username)
    }



   
}
