//
//  CheckoutViewController.swift
//  Source
//
//  Created by Techwens Software on 05/09/23.
//

import UIKit

class CheckoutViewController: UIViewController {
    var isAddressSelected : String = ""
    var selectedDate: Date?
    var selectedTime: String?
    var timePicker: UIDatePicker?
    var datePicker = UIDatePicker()
    @IBOutlet var date: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var expressDeleveryBox: UIView!
    @IBOutlet var scheduleDeleveryBox: UIView!
    @IBOutlet var onrightThickIcon: UIImageView!
    
    @IBOutlet var scheduleDeleveryHeight: NSLayoutConstraint!
    @IBOutlet var scheduleThickIc: UIImageView!
    
    @IBOutlet var scheduleEditBox: UIView!
    @IBOutlet var conFirmBtn: BorderButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        style()
        // Do any additional setup after loading the view.
       logic()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onCalendarClicked(_ sender: Any) {
//         Create and present a date picker or calendar picker
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0, y: 15, width: self.view.frame.width, height: 216)
//        let alertController = UIAlertController(title: "Select Date", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
//        alertController.view.addSubview(datePicker)
//
//        let doneAction = UIAlertAction(title: "Done", style: .default) { (_) in
//            // Handle the selected date
//            self.selectedDate = datePicker.date
//            // Update your UI or perform any actions with the selected date
//        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//
//        alertController.addAction(doneAction)
//        alertController.addAction(cancelAction)
        
        scheduleEditBox.addSubview(datePicker)

//        present(alertController, animated: true, completion: nil)
        
//        print ("clander cli9cker")
    }
    
    
    @IBAction func onClockClicked(_ sender: Any) {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .time
        datePicker.addTarget(self, action: #selector(timePickerValueChanged(_:)), for: .valueChanged)
        datePicker.frame = CGRect(x: 0, y: 15, width: self.view.frame.width, height: 216)
//        let alertController = UIAlertController(title: "Select Date", message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
//        alertController.view.addSubview(datePicker)
//
//        let doneAction = UIAlertAction(title: "Done", style: .default) { (_) in
//            // Handle the selected date
//            self.selectedDate = datePicker.date
//            // Update your UI or perform any actions with the selected date
//        }
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//
//        alertController.addAction(doneAction)
//        alertController.addAction(cancelAction)
        
        scheduleEditBox.addSubview(datePicker)
    }
    
   

    @objc private func datePickerValueChanged(_ sender: UIDatePicker) {
        // You can optionally handle date changes in real-time
   self.selectedDate = sender.date
        sender.removeFromSuperview()
        
        let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "yyyy-MM-dd" // Adjust the date format as needed
           
           // Convert the selectedDate to a string
        let formattedDate = dateFormatter.string(from: selectedDate ?? Date())
           
           // Update the text of your UITextField
           date.text = formattedDate
        
    }

    @objc private func timePickerValueChanged(_ sender: UIDatePicker) {
        // You can optionally handle time changes in real-time
  
        let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "hh:mm a"
              let selectedTime = dateFormatter.string(from: sender.date)
              // Update your UI or perform any actions with the selected time
        self.selectedTime = selectedTime
              print("Selected Time: \(selectedTime)")
        sender.removeFromSuperview()
        time.text = self.selectedTime
    }

    private func logic(){
        

//        MARK:- add guester onSchedule box
        
        let expressBtnGesture = UITapGestureRecognizer(target: self, action: #selector(expressBtnClicked))
        expressDeleveryBox.addGestureRecognizer(expressBtnGesture)
        expressDeleveryBox.isUserInteractionEnabled = true
        
        let scheduleBoxGesture = UITapGestureRecognizer(target: self, action: #selector(scheduleBtnClick))
        scheduleDeleveryBox.addGestureRecognizer(scheduleBoxGesture)
        scheduleDeleveryBox.isUserInteractionEnabled = true
        
        
//        Confirm button
        
     
        
    }
    
    
    @objc private func expressBtnClicked(){
        if (isAddressSelected == "Express"){
            onrightThickIcon.isHidden = true
            conFirmBtn.isValid = false
            isAddressSelected = ""
        }else {
            onrightThickIcon.isHidden = false
            conFirmBtn.isValid = true
            isAddressSelected = "Express"
            scheduleDeleveryHeight.constant = 76
            UIView.animate(withDuration: 0.3) {
                  self.view.layoutIfNeeded()
              }
           
            scheduleEditBox.isHidden = true
           
        }
    }
    
    
        @objc private func scheduleBtnClick(){
            if isAddressSelected == "Schedule" {
                   isAddressSelected = ""
                conFirmBtn.isValid = false
                   // Reduce the height of scheduleDeleveryBox when Schedule is not selected
                scheduleDeleveryHeight.constant = 76
                UIView.animate(withDuration: 0.3) {
                      self.view.layoutIfNeeded()
                  }
               
                scheduleEditBox.isHidden = true
               } else {
                   // Expand the height of scheduleDeleveryBox when Schedule is selected
                   scheduleDeleveryHeight.constant = 141
                   UIView.animate(withDuration: 0.3) {
                         self.view.layoutIfNeeded()
                     }
                   isAddressSelected = "Schedule"
                   conFirmBtn.isValid = true
                   scheduleEditBox.isHidden = false
                   onrightThickIcon.isHidden = true
               }
            print(isAddressSelected,"isaddressSelected")
        }
       
    @IBAction func onConfirmClicked(_ sender: Any) {
        guard let vc = PaymentViewController.loadFromXIB() else {return}
                navigationController?.pushViewController(vc, animated: true)
    }
    

    private func style(){
        scheduleEditBox.isHidden = true
//        scheduleDeleveryBox.isHidden = true
       
//        scheduleThickIc.isHidden = true
        expressDeleveryBox.layer.borderWidth = 1
        expressDeleveryBox.layer.borderColor = UIColor.black.cgColor
        
        scheduleDeleveryBox.layer.borderWidth = 0.5
        scheduleDeleveryBox.layer.borderColor = UIColor.black.cgColor
        
        scheduleDeleveryHeight.constant = 76
    }
    
    
}

extension CheckoutViewController {
    
    class func loadFromXIB() -> CheckoutViewController? {
        let storyboard = UIStoryboard(name: "Checkout", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "CheckoutViewController") as? CheckoutViewController else {
            return nil
        }
        return viewController
}
    }
    
